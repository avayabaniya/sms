<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['id','name', 'department_head'];

    public function employee(){
        return $this->hasMany('App\Employee');
    }
}
