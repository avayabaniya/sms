<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{

 protected $fillable = [
        'cname', 'room_code', 'description', 'amount', 'room_id',
    ];
    public function studentadmission()
    {
        return $this->hasMany('App\StudentAdmission');
    }
    public function section()
    {
    	return $this->hasOne('App\Section');
    }
    public function invoice()
    {
        return $this->hasMany(Invoice::class);
    }
    public function subject()
    {
    	return $this->hasMany('App\Subject');
   }
   public function exam()
    {
        return $this->hasMany('App\Exam');

        // return $this->belongsTo('App\Exam');
   }
   public function classfee()
    {
        return $this->belongsTo('App\Room');
   }

   public function feetype()
    {
        return $this->hasMany('App\FeeType');
   }
    public function academic()
    {

        return $this->belongsTo('App\Academic');
   }
   public function mark()
    {
        return $this->hasMany('App\Mark');

   }
}
