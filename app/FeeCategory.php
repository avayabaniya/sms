<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeCategory extends Model
{
     protected $fillable = ['id', 'feecategory', 'receiptno', 'description'];
}

// public function feesubcategory()
// {
// 	return $this->belongsToMany('App\FeeSubCategory');
// }