<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeType extends Model
{
    protected $fillable = ['id', 'feetype','room_id','amount'];

    public function invoice(){
        return $this->hasMany('App\Invoice');
    }

    public function room(){
        return $this->belongsTo('App\Room');
    }
}
