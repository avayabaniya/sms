<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
   public function create()
   {
  	return $this->belongsTo('App\Create','exam_id');
   }

   public function subject()
   {
  	return $this->belongsTo('App\Subject');
   }

   public function room()
   {
  	return $this->belongsTo('App\Room');
   }
}
