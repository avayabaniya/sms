<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'sname', 'room_id', 'topic','description',
    ];
    public function room()
    {
    	return $this->belongsTo('App\Room');
    }

    public function exam()
    {
        return $this->hasMany('App\Exam');

   }
    public function mark()
    {
        return $this->hasMany('App\Mark');

   }

}
