<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{

    protected $table = 'employee_salary';

    protected $fillable = ['id','employee_id', 'salary','grade', 'allowance', 'micro_finance', 'local_allowance', 'festival_allowance', 'home_rent', 'mahangi_allowance', 'responsibility_allowance', 'manager_allowance', 'total', 'changed_salary'];

    public function employee(){
        return $this->belongsTo('App\Employee');
    }
}
