<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDetail extends Model
{
    protected $fillable = ['employee_id', 'dob', 'address', 'country', 'phone','mobile', 'resume','qualification','join_date','religion','image', 'mother_tongue','blood_group','email','description', 'facebook','twitter','instagram','linkedin' ];

    public function employee(){
        return $this->belongsTo('App\Employee', 'employee_id');
    }


}
