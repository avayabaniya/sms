<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeWaiver extends Model
{
     protected $fillable = ['id', 'feescategory', 'feessubcategory', 'excemptionordeduction'];
}
