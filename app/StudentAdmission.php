<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentAdmission extends Model
{

        protected $fillable = ['id', 'first_name', 'middle_name', 'last_name'];

    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function student()
    {
        return $this->belongsTo('App\Student');
    }
    public function invoice()
    {
        return $this->hasMany(StudentAdmission::class);
    }
}
