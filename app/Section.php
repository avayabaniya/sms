<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
        protected $fillable = [
        'sname', 'scode','room_id',

    ];

    public function invoice(){
        return $this->hasMany('App\Invoice');
        }

    public function student()
    {
        return $this->hasMany('App\Student');
    }

    public function studentadmission()
    {
        return $this->hasMany('App\StudentAdmission');
    }
    public function room()
    { 
    	return $this->belongsTo('App\Room');

    }
    public function academic()
    {
        return $this->hasMany('App\Academic');
    }
     public function mark()
    {
        return $this->hasMany('App\Mark');

   }
}
