<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeAllocate extends Model
{
     protected $fillable = ['id', 'feecategory', 'feesubcategory', 'feefor'];
}
