<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    protected $fillable = ['employee_id', 'month', 'salary_per_month'];

    public function salaryAddition(){
        return $this->hasOne('App\SalaryAddition');
    }

    public function salaryDeduction(){
        return $this->hasOne('App\SalaryDeduction');
    }

    public function employee(){
        return $this->belongsTo('App\Employee');
    }
}
