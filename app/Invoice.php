<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['id', 'student_admission_id', 'student_id', 'room_id', 'section_id','date','feetype_id'];

    public function feetype(){
        return $this->belongsTo('App\FeeType');
    }
    public function room(){
        return $this->belongsTo('App\Room');
    }

    public function section(){
        return $this->belongsTo('App\Section');
    }
     public function studentadmission(){
        return $this->belongsTo('App\StudentAdmission');
    }

     public function student(){
        return $this->belongsTo('App\Student');
    }

    public function setting(){
        return $this->belongsTo('App\Setting');
    }
}