<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryAddition extends Model
{
    protected $fillable = ['payroll_id', 'month', 'over_time_rate', 'over_time_hour_worked'];

    public function payroll(){
        return $this->belongsTo('App\Payroll');
    }
}
