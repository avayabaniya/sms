<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['employee_code', 'name', 'gender'];

    public function employeeDetails(){
        return $this->hasOne('App\EmployeeDetail');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function designation(){
        return $this->belongsTo('App\Designation');
    }

    public function payroll(){
        return $this->hasMany('App\Payroll');
    }

    public function academic(){
        return $this->hasmany('App\Academic');
    }

    public function employeeSalary(){
        return $this->hasOne('App\Salary');
    }
}
