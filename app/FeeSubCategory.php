<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeSubCategory extends Model
{
      protected $fillable = ['id', 'feecategory', 'feesubcategoryname', 'amount', 'feetype'];
}
