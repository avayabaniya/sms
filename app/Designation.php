<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = ['id','name', 'salary','grade', 'allowance', 'micro_finance', 'local_allowance', 'festival_allowance', 'home_rent', 'mahangi_allowance', 'responsibility_allowance', 'manager_allowance', 'total'];

    public function employee(){
        return $this->hasMany('App\Employee');
    }
}
