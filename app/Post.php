<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
   

    public function scopeSearch($query, $search)
    {

        return $query->where('title', 'LIKE', "%{$search}%")                                  
        
            ->paginate(15);
    }
}
