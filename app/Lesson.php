<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Notifiable;

class Lesson extends Model
{
    protected $fillable = [
        'batch', 'sname', 'scode', 'cname', 'topic',
    ];
}
