<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeSubCategoryFine extends Model
{
          protected $fillable = ['id', 'feecategory', 'feesubcategory', 'type', 'finetype'];

}
