<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Academic extends Model
{
     public function employee(){
        return $this->belongsTo('App\Employee');
    }
    public function room(){
        return $this->belongsTo('App\Room');
    }
    public function section(){
        return $this->belongsTo('App\Section');
    }
}
