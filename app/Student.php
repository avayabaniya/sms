<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function studentadmission()
    {
        return $this->hasMany('App\StudentAdmission');
    }

    public function guardian()
    {
        return $this->belongsToMany('App\Guardian');
    }

    public function room()
    {
        return $this->belongsTo('App\Room');
    }

      public function invoice()
    {
        return $this->hasMany('App\Invoice');
    }
    
} 