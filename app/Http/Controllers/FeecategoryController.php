<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeeCategory;
use Session;

class FeecategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['feecategory'] = FeeCategory::all();
        return view ('backend.feecategory.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
               'feecategory' => 'required',
               'receiptno' => 'required',
               'description' => 'required',
               ]);
        $feecategory = feecategory::create([
        'feecategory' => $request->feecategory,
        'receiptno' => $request->receiptno,
        'description' => $request->description,
      ]);
        return redirect()->back(); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $feecategory = feeCategory::find($id);
        return view('backend.feecategory.index')->with($feecategory);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $feecategory = feeCategory::find($id);
            $feecategory->feecategory = $request->input('feecategory');
            $feecategory->receiptno = $request->input('receiptno');
            $feecategory->description = $request->input('description');
            $feecategory->update();
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feecategory = FeeCategory::findOrFail($id);
        //dd($feecategory);
        $feecategory->delete();
        return redirect()->back();
    }

    // public function destroy(Feecategory $feecategory){
        
    //     $feecategory->delete();
       
    //      return redirect()->back();
    // }
}
