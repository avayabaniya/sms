<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Eventtype;
use Session;

class EventsController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['event'] = Event::all();
        $arr['eventtype'] = Eventtype::all();
        return view ('backend.events.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Event $event )
    {

        if ($request->checkbox == 1) 
        {
            $type = 'holiday';
            
        }else 
        {
            $type = $request->type;
            
        }

        $this->validate($request, [
               'name' => 'required|unique:events|regex:/(^([a-zA-Z]+))/',
               'description' => 'nullable',
               'start_date' => 'required|date|before_or_equal:end_date|after:'.today(),
               'end_date' => 'required|date|after_or_equal:start_date',
               'eventfor' => 'nullable',
           ]);
        
        $event->name = ucfirst(strtolower($request->name));
        $event->description = ucfirst(strtolower($request->description));
        $event->type = ucfirst(strtolower($type));
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
               
        $event->save();
        Session::flash('success', 'event Created');

        return redirect()->back();
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
     {
        $arr['event']= Event::all();
        return view('events.index')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        
        $arr['event']= $event;
        $arr['eventtype']= Eventtype::all();
        return view('backend.events.edit')->with($arr);
    
    }

     public function editEvent(Request $request, $id ){
        $event = Event::findOrFail($id);
        $eventtype = Eventtype::all();

        //echo "test";die;
        if ($request->isMethod('post'))
        {
       
 echo "string";die();
         

        $event->name = $request->name;
        $event->description = $request->description;
        $event->type = $request->type;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
               
        $event->save();

            //return redirect()->back()->with(compact('employee','departmentDetails', 'designationDetails', 'countries'));
            Session::flash('success', 'Event Successfully Updated');
        return redirect()->route('events.index');
            

        }

        //Session::flash('info', 'Employee Updated Successfully ');
        //return redirect()->route('edit.employee', ['id' => $employee->id])->with(compact('employee'));
        return view('backend.events.edit')->with(compact('event','eventtype'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Event $event, $id)
    {
        $this->validate($request, [
               'name' => 'required',
               'description' => 'nullable',
               'start_date' => 'required|before_or_equal:end_date',
               'end_date' => 'required|date|after_or_equal:start_date',
               'eventfor' => 'nullable',
           ]);

        $event=Event::where('id',$id)->first();
        $event->name = ucfirst(strtolower($request->name));
        $event->description = ucfirst(strtolower($request->description));
        $event->type = ucfirst(strtolower($request->type));
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
               
        $event->save();
        Session::flash('success', 'event Created');

        return redirect()->route('events.index');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();
        Session::flash('error', 'event detail Successfully Deleted');
        return redirect()->route('events.index');
    }
}
