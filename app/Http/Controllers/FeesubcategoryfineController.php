<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeeSubCategoryFine;
use App\FeeSubCategory;
use App\FeeCategory;
use Session;

class FeesubcategoryfineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $arr['feesubcategoryfine'] = FeeSubCategoryFine::all();
        // return view ('backend.feesubcategoryfine.index')->with($arr);
        $feecategoryfine = FeeCategory::get();
        $feesubcategoryfine = FeeSubCategory::get();
        $feeDetail = FeeSubCategoryFine::get();

        //$feeDetails = FeeSubCategoryFine::get();
        //dd($feeDetail);
        return view ('backend.feesubcategoryfine.index')->with(compact('feecategoryfine','feesubcategoryfine', 'feeDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
            //dd($request->all());
          
        if ($request->isMethod('post'))
        {

        $feesubcategoryfine = feeSubcategoryFine::create([
        'feecategory' => $request->feecategory,
        'feesubcategory' => $request->feesubcategoryname,
        //'type' => $request->type,
        'finetype' => $request->finetype,
        ]);
        return redirect()->back(); 

    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feesubcategoryfine = feeSubCategoryFine::find($id);
        return view('backend.feesubcategoryfine.index')->with($feesubcategoryfine); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feesubcategoryfine = feeSubCategoryFine::find($id);
            $feesubcategoryfine->feecategory = $request->input('feecategory');
            $feesubcategoryfine->feesubcategory = $request->input('feesubcategory');
            $feesubcategoryfine->type = $request->input('type');
            $feesubcategoryfine->finetype = $request->input('finetype');
            $feesubcategoryfine->update();
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feesubcategoryfine = FeeSubCategoryFine::findOrFail($id);

        //dd($feesubcategoryfine);
        $feesubcategoryfine->delete();
        return redirect()->back();
    }
}
