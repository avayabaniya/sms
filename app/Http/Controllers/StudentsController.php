<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Session;

class StudentsController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
    	$arr['student'] = Student::all();
    	return view ('backend.students.index')->with($arr);
    }

    public function store(Request $request,Student $student)
    {

            echo "string";die;
        
       $this->validate($request, [
               'category' => 'required',

           ]); 
        $student->category = $request->category;
        
        $student->save();
        Session::flash('success', 'student Successfully Created');

        return redirect()->route('studentcategory');
    
    }
    public function admission()
    {
    	$arr['admission'] = StudentAdmission::all();
    	return view ('backend.students.admission')->with($arr);
    }

    public function list()
    {
    	$arr['student'] = Student::all();
    	$arr['list'] = StudentList::all();
    	return view('backend.students.list')->with($arr);
    }

    public function show(StudentList $studentlist)
    {
    	$arr['list']= $studentlist;
        return view('backend.students.show')->with($arr);
    }

    public function edit(StudentList $studentlist)
    {
        
        $arr['list']= $studentlist;
        return view('backend.students.edit')->with($arr);
    
    }

    public function update(Request $request, StudentList $studentlist)
    {
        $this->validate($request, [
               'category' => 'required',
               

           ]);
        $category->title = $request->title;
        $category->save();
        Session::flash('success', 'category Successfully Updated');
        return redirect()->route('backend.categories.index');
    }

    public function destroy(Student $student)
    {
        $student->delete();
        Session::flash('error', 'student detail Successfully Deleted');
        return redirect()->route('students.index');
    }
}
