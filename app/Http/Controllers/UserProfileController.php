<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserProfile;
use App\Country;
use App\Setting;
use Session;
use Image;
use Illuminate\Support\Facades\Input;

class UserProfileController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

     public function edit(Request $request, $id )
     {
        $user = User::findOrFail($id);
        $user = User::all();
       	$countries = Country::all();
    	$setting = Setting::all();
        $userprofile = UserProfile::all();
        return view('auth.adminprofile')->with(compact('user','userprofile','countries','setting'));
    }

    public function update(Request $request, User $user, $id)

    {   
        $this->validate($request,[
        'name' => 'required|max:50',
        'email' => 'required|email',
        'address' => 'required',
        'mobile' => 'required',
        'about' => 'required',
            ],[
                'about.required'=>'Message is Required'

      ]);
        	$user=User::where('id',$id)->first();        
       
        if ($request->hasFile('image'))
        {
               $image_tmp = Input::file('image');
               if ($image_tmp->isValid()) 
               {
                   //echo "test";die;
                   $extension = $image_tmp->getClientOriginalExtension();
                   $filename = rand(111,99999).'.'.$extension;
                   $large_image_path = 'public/uploads/avatars/'.$filename;
                    $currentfilename= $user->userprofile->image;

                     if(file_exists('public/uploads/avatars/'.$currentfilename))
                    {
                       
                        unlink('public/uploads/avatars/'.$currentfilename);

                    }

                   //Resize Images
                   Image::make($image_tmp)->save($large_image_path);

                   $user->userprofile->image = $filename;
                }
                else
                {

                        //Store image name in products table
                        $user->userprofile->image = $currentfilename;
                }
        }

            $user->name =  ucwords(strtolower($request->name));
            $user->email = strtolower($request->email);
            $user->userprofile->mobile = $request->mobile;        
            $user->userprofile->about =  ucwords(strtolower($request->about));        
            $user->userprofile->address =  ucwords(strtolower($request->address));        
        	$user->save();
        	$user->userprofile->save();
         Session::flash('success', 'Info Updated Successfully ');
        return redirect()->back();

    }

}
