<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Room;
use App\Lesson;
use App\Subject;
use App\Academic;
class LessonsController extends Controller
{
    
    public function index()
    {   
            $arr['rooms'] = Room::All();
            $arr['lessons'] = Lesson::All();
            $arr['subjects'] = Subject::All();
            $arr['academics'] = Academic::All();
        return view('backend.lessons.index')->with(compact('arr'));
    }

   

    
    public function store(Request $request, Lesson $lesson)
    {
       $this->validate($request,[
        'batch' => 'required',
        'sname' => 'required',          
        'scode'=> 'required',
        'cname'=> 'required',
        'topic'=> 'required', 

            ]);

            $topic[] = $request->input('topic');
            $contact = json_encode($topic);

            $lessons = Lesson::create([
               'batch' => $request->batch,
               'sname' => $request->sname,
               'scode'=> $request->scode,
               'cname'=> $request->cname,
               'topic'=> $contact,
           ]);

          Session::flash('success', 'Lesson Added Successfully ');
        return redirect()->back();

    }

 public function edit( Request $request, $id)
 {


     $les = Lesson::find($id);

              $arr['rooms'] = Room::All();
            $arr['lessons'] = Lesson::find($id);
            $arr['subjects'] = Subject::All();
            $arr['academics'] = Academic::All();

    // echo  $arr['lessons'][2]->batch ; die();

        return view('backend.lessons.edit')->with(compact('arr','les'));



 }


    
     public function update(Request $request, $id)
    {  
        $lessons = Lesson::find($id);

   // print_r( $request->all()); die();

           $topic[] = $request->input('topic');
            $contact = json_encode($topic);


            $lessons->batch = $request->batch;
            $lessons->sname = $request->sname;
            $lessons->scode = $request->scode;
            $lessons->cname = $request->cname;
            $lessons->topic = $contact;



        $lessons->save();
         Session::flash('success', 'lesson Updated Successfully ');
        
        return redirect(Route('lesson'));

    }



    public function destroy($id = null)
    {
        $les = Lesson::findOrFail($id);

        $les->delete();
        Session::flash('error', 'lesson  Successfully Deleted');
        return redirect()->route('lesson');
    }
 
}
