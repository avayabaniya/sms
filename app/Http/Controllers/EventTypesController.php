<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Eventtype;
use Session;
use Illuminate\Support\Facades\DB;

class EventTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['eventtype'] = Eventtype::all();
        return view ('backend.eventtypes.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Eventtype $eventtype )
    {
        $this->validate($request, [
               'title' => 'required',
               
           ]);
        
        $eventtype->title = $request->title;   
        $eventtype->save();
        Session::flash('success', 'event Type Added');

        return redirect()->back();
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Eventtype $eventtype)
     {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function editEvent(Request $request, $id = null) {

        //dd($request->all());
        //dd($request->department_head);

        //Department::where('id',$id)->update(['department_id' => $request->department_head]);


        if ($request->isMethod('post')){
            DB::table('eventtypes')->where('id', $id)->update(['title' => $request->title]);

            return redirect(route('eventtypes.index'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Eventtype $event)
    {
        
        $this->validate($request, [
               'title' => 'required',
               
           ]);
        
        $event->title = $request->title;   
        $event->save();
        Session::flash('success', 'event Type Added');

        return redirect()->back();
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Eventtype $eventtype)
    {
        $eventtype->delete();
        Session::flash('error', 'event Successfully Deleted');
        return redirect()->route('eventtypes.index');
    }
}
