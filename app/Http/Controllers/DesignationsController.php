<?php

namespace App\Http\Controllers;

use App\Department;
use App\Designation;
use App\Employee;
use App\Salary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DesignationsController extends Controller
{
    public function viewDesignation() {
        $designations = Designation::orderBy('id', 'desc')->get();

        //echo "<pre>"; print_r($departments); die;
        return view('backend.designation.index')->with(compact('designations'));
        //return view('backend.department.index');
    }

    public function addDesignation(Request $request)
    {
        $this->validate($request, [
            'designation' => 'required|max:20',
            'salary' => 'required|numeric',
        ]);


        Designation::create([
            'name' => ucwords(strtolower($request->designation)),
            'salary' => $request->salary,
            'total' => $request->salary
        ]);

        Session::flash('success', 'Designation Successfully Added');
        return redirect(route('view.designation'));
    }

    public function editDesignation(Request $request, $id = null) {

        //dd($request->all());
        //dd($request->department_head);

        //Department::where('id',$id)->update(['department_id' => $request->department_head]);

        $salary = $request->salary;
        if (empty($request->salary)){
            $salary = $request->current_salary;
        }

        if ($request->isMethod('post')){
            $total = $salary + $request->grade + $request->allowance + $request->micro_finance + $request->local_allowance + $request->festival_allowance + $request->home_rent + $request->mahangi_allowance + $request->responsibility_allowance + $request->manager_allowance;

            $newSalary = Designation::where('id', $id)->first();

            $newSalary->salary = $salary;
            $newSalary->grade = $request->grade;
            $newSalary->allowance = $request->allowance;
            $newSalary->micro_finance = $request->micro_finance;
            $newSalary->local_allowance = $request->local_allowance;
            $newSalary->festival_allowance = $request->festival_allowance;
            $newSalary->home_rent = $request->home_rent;
            $newSalary->mahangi_allowance = $request->mahangi_allowance;
            $newSalary->responsibility_allowance = $request->responsibility_allowance;
            $newSalary->manager_allowance = $request->manager_allowance;
            $newSalary->total = $total;
            $newSalary->save();
            //($newSalary);

            $employees = Employee::where('designation_id', $id)->get();
            foreach ($employees as $employee){
                $changedSalary = Salary::where('employee_id', $employee->id)->first();
                if (!empty($changedSalary))
                {
                    if ($changedSalary->changed_salary == 0){

                        $changedSalary->salary = $salary;
                        $changedSalary->grade = $request->grade;
                        $changedSalary->allowance = $request->allowance;
                        $changedSalary->micro_finance = $request->micro_finance;
                        $changedSalary->local_allowance = $request->local_allowance;
                        $changedSalary->festival_allowance = $request->festival_allowance;
                        $changedSalary->home_rent = $request->home_rent;
                        $changedSalary->mahangi_allowance = $request->mahangi_allowance;
                        $changedSalary->responsibility_allowance = $request->responsibility_allowance;
                        $changedSalary->manager_allowance = $request->manager_allowance;
                        $changedSalary->total = $total;
                        $changedSalary->save();
                    }
                }

            }

            Session::flash('success', 'Designation Successfully Updated');
            return redirect(route('view.designation'));
        }

    }

    public function deleteDesignation($id = null) {

        Employee::where('designation_id', $id)->update(['designation_id' => 0]);

        Designation::findOrFail($id)->delete();
        Session::flash('error', 'Designation Successfully Deleted');
        return redirect()->back();
    }

    public function viewSelectedDesignation($id = null)
    {
        $designationDetail = Designation::with('employee')->findOrFail($id);
        //dd($designationDetail);
        return view('backend.designation.listing')->with(compact('designationDetail'));
    }
}
