<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DepartmentsController extends Controller
{
    public function viewDepartment() {
        $departments = Department::orderBy('id', 'desc')->get();

        $departmentHeads = Employee::all();

        //echo "<pre>"; print_r($departments); die;
        return view('backend.department.index')->with(compact('departments', 'departmentHeads'));
    }

    public function addDepartment(Request $request)
    {
        $this->validate($request, [
            'department' => 'required|max:20',
            ]);

        Department::create([
            'name' => ucwords(strtolower($request->department)),
            'department_head' => null
        ]);
        Session::flash('success', 'Department Successfully Added');
        return redirect(route('view.department'));
    }

    public function editHeadDepartment(Request $request, $id = null) {

        //dd($request->all());
        //dd($request->department_head);

        //Department::where('id',$id)->update(['department_id' => $request->department_head]);

        DB::table('departments')->where('id', $id)->update(['department_head' => $request->department_head]);

        Session::flash('success', 'Department Head Successfully Updated');
        return redirect(route('view.department'));

    }

    public function deleteDepartment($id = null) {

        Employee::where('department_id', $id)->update(['department_id' => 0]);

        Department::findOrFail($id)->delete();

        Session::flash('error', 'Department Successfully Deleted');

        return redirect()->back();
    }

    public function viewSelectedDepartment($id = null)
    {
        $departmentDetail = Department::with('employee')->findOrFail($id);
        //dd($departmentDetails);
        return view('backend.department.listing')->with(compact('departmentDetail'));
    }
}
