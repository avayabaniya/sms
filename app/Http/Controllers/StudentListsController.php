<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\StudentList;
use App\StudentAdmission;
use Session;

class StudentListsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['student'] = Student::all();
        $arr['list'] = StudentList::all();
        return view('backend.students.list')->with($arr)->sortBy('name');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr['student'] = Student::all(); 
        return view('student.admission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,StudentList $studentlist)
    {
        
        $studentlist->academic_year = $request->academic_year;
        $studentlist->register_number = $request->register_number;
        $studentlist->joining_date = $request->joining_date;
        $studentlist->course = $request->course;
        $studentlist->batch = $request->batch;
        $studentlist->first_name = $request->first_name;
        $studentlist->middle_name = $request->middle_name;
        $studentlist->last_name = $request->last_name;
        $studentlist->dob = $request->dob;
        $studentlist->gender = $request->gender;
        $studentlist->blood_group = $request->blood_group;
        $studentlist->birth_place = $request->birth_place;
        $studentlist->nationality = $request->nationality;
        $studentlist->rollno = $request->rollno;
        $studentlist->mother_tongue = $request->mother_tongue;
        $studentlist->category = $request->category;
        $studentlist->religion = $request->religion;        
        $studentadmission->permanent_address = $request->permanent_address;        
        $studentadmission->present_address = $request->present_address;        
        $studentadmission->city = $request->city;        
        $studentadmission->province = $request->province;        
        $studentadmission->country = $request->country;        
        $studentadmission->phone_no = $request->phone_no;        
        $studentadmission->mobile_no = $request->mobile_no;        
        $studentadmission->email = $request->email;        
            $studentadmission->pname =$request->pname;
            $studentadmission->relation  =$request->relation;
            $studentadmission->education  =$request->education;
            $studentadmission->occupation  =$request->occupation;
            $studentadmission->income  =$request->income;
            $studentadmission->address  =$request->address;
            $studentadmission->pcity  =$request->pcity;
            $studentadmission->pcountry  =$request->pcountry;
            $studentadmission->pstate  =$request->pstate;
            $studentadmission->pphone  =$request->pphone;
            $studentadmission->pmobile  =$request->pmobile;
            $studentadmission->pemail  =$request->pemail;       
        $studentlist->save();
        Session::flash('success', 'student Successfully Created');

        return redirect()->route('studentlists.index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show(StudentList $studentlist)
    {
        $arr['list']= $studentlist;
        return view('backend.students.show')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentList $studentlist)
    {
         $arr['studentlist']= $studentlist;
        return view('backend.students.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentList $studentlist)
    {
        
       $studentlist->register_number = $request->register_number;
        $studentlist->joining_date = $request->joining_date;
        $studentlist->course = $request->course;
        $studentlist->batch = $request->batch;
        $studentlist->first_name = $request->first_name;
        $studentlist->middle_name = $request->middle_name;
        $studentlist->last_name = $request->last_name;
        $studentlist->dob = $request->dob;
        $studentlist->gender = $request->gender;
        $studentlist->blood_group = $request->blood_group;
        $studentlist->birth_place = $request->birth_place;
        $studentlist->nationality = $request->nationality;
        $studentlist->rollno = $request->rollno;
        $studentlist->mother_tongue = $request->mother_tongue;
        $studentlist->category = $request->category;
        $studentlist->religion = $request->religion;        
        $studentlist->save();
        Session::flash('success', 'student Successfully Created');

        return redirect()->route('studentlists.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
