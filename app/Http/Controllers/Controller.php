<?php

namespace App\Http\Controllers;

use App\Department;
use App\Designation;
use App\Employee;
use App\Event;
use App\Room;
use App\Setting;
use App\Student;
use App\StudentAdmission;
use App\Invoice;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function departments(){
        $departments = Department::get();
        return $departments;
    }

    public static function designations(){
        $designations = Designation::get();
        return $designations;
    }

    public static function settings(){
    	$settings = Setting::get();
    	return $settings;
    }

    public static function studentadmissions(){
    	$studentadmissions = StudentAdmission::get();
    	return $studentadmissions;
    }

    public static function rooms(){
        $rooms = Room::get();
        return $rooms;
    }

    public static function invoices(){
        $invoices = Invoice::get();
        return $invoices;
    }

    public static function studentCount(){
        $studentCount = StudentAdmission::count();
        return $studentCount;
    }

    public static function employeeCount(){
        $employeeCount = Employee::count();
        return $employeeCount;
    }

    public static function classCount(){
        $classCount = Room::count();
        return $classCount;
    }

    public static function departmentCount(){
        $departmentCount = Department::count();
        return $departmentCount;
    }

    public static function designationCount(){
        $designationCount = Designation::count();
        return $designationCount;
    }

    public static function eventCount(){
        $eventCount = Event::count();
        return $eventCount;
    }

    
}
