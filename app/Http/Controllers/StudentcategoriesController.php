<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Session;
use Illuminate\Support\Facades\DB;


class StudentcategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['student'] = Student::all();
        return view ('backend.studentcategories.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Student $student)
    {
        
       $this->validate($request, [
               'category' => 'required|regex:/(^([a-zA-Z]+))/|unique:students|max:20',
                'percent' => 'required|numeric|min:0|max:100',
            ],[
                'category.required' => ' Category field is required.'
           ]); 

        $student->category = ucwords(strtolower($request->category));
        $student->percent = $request->percent;

        $student->save();
        Session::flash('success', 'Student Category Successfully Created');

        return redirect()->route('studentcategories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit(Request $request, $id = null) {

        //dd($request->all());
        //dd($request->department_head);

        //Department::where('id',$id)->update(['department_id' => $request->department_head]);


        if ($request->isMethod('post')){
            DB::table('students')->where('id', $id)->update(['percent' => $request->percent,
                'category' => $request->category]);
        Session::flash('success', 'Student Category Successfully Updated');
            return redirect(route('studentcategories.index'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id = null)
    {
        Student::findOrFail($id)->delete();
        Session::flash('success', 'Student Category Deleted');
        return redirect()->back();
    }
}