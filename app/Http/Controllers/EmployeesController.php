<?php

namespace App\Http\Controllers;

use App\Country;
use App\Department;
use App\Designation;
use App\Employee;
use App\EmployeeDetail;
use App\Payroll;
use App\Salary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EmployeesController extends Controller
{
    public function addEmployee(Request $request)
    {

        $departmentCount = Department::count();
        $designationCount = Designation::count();

        if ($departmentCount == 0){
            Session::flash('error', 'Please create a department before adding employee');
            return redirect(route('add.department'));
        }

        if ($designationCount == 0){
            Session::flash('error', 'Please create a designation before adding employee');
            return redirect(route('add.designation'));
        }

        $departmentDetails = Department::get();
        $designationDetails = Designation::get();
        $countries = Country::get();
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'first_name' => 'required|max:20|alpha',
                'last_name' => 'required|max:20|alpha',
                'bdate' => 'required',
                'gender' => 'required',
                'department' => 'required',
                'designation' => 'required',
                'employee_type' => 'required',
                'country' => 'required',
                'qualification' => 'required',
                'phone' => 'nullable|numeric|digits_between:7,10',
                'mobile' => 'required|numeric|digits_between:10,15',
                'address' => 'required',
                'jdate' => 'required',
                'blood_group' => 'required',
                'email' => 'required|email|unique:employee_details',
                'turl' => 'nullable|url',
                'insurl' => 'nullable|url',
                'furl' => 'nullable|url',
                'inurl' => 'nullable|url',
            ]);


            //Image upload
            $imageName = "";
            if ($request->hasFile('image')){
                $image = $request->file('image');
                $ext = $image->getClientOriginalExtension();
                $imageName = str_random().'.'.$ext;
                $path = public_path('backend\images\employee');

                //echo $path; die;

                $image->move($path, $imageName);
            }else{
                if ($request->gender === 'male' ){
                    $imageName = 'male_avatar.png';
                }elseif ($request->gender === 'female'){
                    $imageName = 'female_avatar.png';
                }else{
                    $imageName = 'male_avatar.png';
                }
            }
            //Resume upload
            if ($request->hasFile('resume')){
                $resume = $request->file('resume');
                $ext = $resume->getClientOriginalExtension();
                $resumeName = str_random().'.'.$ext;
                $resumePath = public_path('backend\documents');

                //echo $path; die;
                $resume->move($resumePath, $resumeName);
            }else{
                $resumeName = "";
            }

            $employee = new Employee;
            $employee->employee_code = rand(10000, 99999);
            $employee->name = ucwords(strtolower($request->first_name ." ". $request->last_name)) ;
            $employee->department_id = $request->department;
            $employee->designation_id = $request->designation;
            $employee->gender = ucwords(strtolower($request->gender));
            $employee->employee_type = $request->employee_type;
            $employee->save();

            EmployeeDetail::create([
               'employee_id' => $employee->id,
                'dob' => $request->bdate,
                 'email' => $request->email,
                 'image' => $imageName,
                 'address' => ucwords(strtolower($request->address)),
                'country' => $request->country,
                'qualification' => $request->qualification,
                'resume' => $resumeName,
                'phone' => $request->phone,
                'mobile' => $request->mobile,
                'join_date' => $request->jdate,
                'religion' => ucwords(strtolower($request->religion)),
                'mother_tongue' => ucwords(strtolower($request->mother_tongue)),
                'blood_group' => $request->blood_group,
                'description' => $request->description,
                'facebook' => $request->furl,
                'twitter' => $request->turl,
                'instagram' => $request->insurl,
                'linkedin' => $request->inurl,
            ]);

            $designationSalary = Designation::where('id', $request->designation)->first();
            if (!empty($designationSalary)){
                Salary::create([
                    'employee_id' => $employee->id,
                    'salary' => $designationSalary->salary,
                    'grade' => $designationSalary->grade,
                    'allowance' => $designationSalary->allowance,
                    'micro_finance' => $designationSalary->micro_finance,
                    'local_allowance' => $designationSalary->local_allowance,
                    'festival_allowance' => $designationSalary->festival_allowance,
                    'home_rent' => $designationSalary->home_rent,
                    'mahangi_allowance' => $designationSalary->mahangi_allowance,
                    'responsibility_allowance' => $designationSalary->responsibility_allowance,
                    'manager_allowance' => $designationSalary->manager_allowance,
                    'total' => $designationSalary->total,
                    'changed_salary' => 0,
                ]);
            }

            Session::flash('success', 'Employee Added Successfully ');
            return redirect(route('view.employee'))->with(compact('departmentDetails', 'designationDetails', 'countries'));
            //return redirect()->back()->with(compact('departmentDetails', 'designationDetails', 'countries'));

        }
        return view('backend.employee.add')->with(compact('designationDetails', 'departmentDetails', 'countries'));
    }

    public function viewEmployee(){

        $employeeDetails = Employee::with( 'employeeDetails', 'designation', 'employeeSalary')->orderBy('updated_at', 'desc')->get();
        //dd($employeeDetails[0]->employeeSalary['total']);
        return view('backend.employee.index')->with(compact('employeeDetails'));
    }

    public function editEmployee(Request $request, $id = null){

        $employee = Employee::with('employeeDetails')->findOrFail($id);
        $departmentDetails = Department::get();
        $designationDetails = Designation::get();
        $countries = Country::get();
        //echo "test";die;
        if ($request->isMethod('post'))
        {

            $this->validate($request, [
                'name' => 'required|max:20|regex:/(^([a-zA-Z\s]+))/',
                'bdate' => 'required',
                'gender' => 'required',
                'department' => 'required',
                'designation' => 'required',
                'employee_type' => 'required',
                'country' => 'required',
                'qualification' => 'required',
                'phone' => 'nullable|numeric|digits_between:7,10',
                'mobile' => 'required|numeric|digits_between:10,15',
                'address' => 'required',
                'jdate' => 'required',
                'blood_group' => 'required',
                'email' => 'required|email',
                'turl' => 'nullable|url',
                'insurl' => 'nullable|url',
                'furl' => 'nullable|url',
                'inurl' => 'nullable|url',
            ]);

            if ($request->hasFile('image')){
                $image = $request->file('image');
                $ext = $image->getClientOriginalExtension();
                $imageName = str_random().'.'.$ext;
                $path = public_path('backend\images\employee');

                //echo $path; die;
                $image->move($path, $imageName);
            }else{
                $imageName = $request->current_image;
            }

            if ($request->hasFile('resume')){
                $resume = $request->file('resume');
                $ext = $resume->getClientOriginalExtension();
                $resumeName = str_random().'.'.$ext;
                $resumePath = public_path('backend\documents');

                //echo $path; die;
                $resume->move($resumePath, $resumeName);
            }else{
                $resumeName = $request->current_resume;
            }

            if (empty($request->country)) {
                $request->country = "Nepal";
            }

            $employee->name = ucwords(strtolower($request->name));
            $employee->gender = ucwords(strtolower($request->gender));
            $employee->employee_type = $request->employee_type;
            $employee->department_id = $request->department;
            $employee->designation_id = $request->designation;

            $employee->employeeDetails->dob = $request->bdate;
            $employee->employeeDetails->address = ucwords(strtolower($request->address));
            $employee->employeeDetails->country = $request->country;
            $employee->employeeDetails->phone = $request->phone;
            $employee->employeeDetails->mobile = $request->mobile;
            $employee->employeeDetails->join_date = $request->jdate;
            $employee->employeeDetails->religion = ucwords(strtolower($request->religion));
            $employee->employeeDetails->mother_tongue = ucwords(strtolower($request->mother_tongue));
            $employee->employeeDetails->blood_group = $request->blood_group;
            $employee->employeeDetails->description = $request->description;
            $employee->employeeDetails->image = $imageName;
            $employee->employeeDetails->qualification = $request->qualification;
            $employee->employeeDetails->resume = $resumeName;
            $employee->employeeDetails->facebook = $request->furl;
            $employee->employeeDetails->twitter = $request->turl;
            $employee->employeeDetails->instagram = $request->insurl;
            $employee->employeeDetails->linkedin = $request->inurl;

            $employee->employeeDetails->save();
            $employee->save();


            $salaryCheck = Salary::where('employee_id', $id)->count();
            if ($salaryCheck != 0 )
            {
                $designationSalary = Designation::where('id', $request->designation)->first();
                if (!empty($designationSalary)){
                    Salary::create([
                        'employee_id' => $employee->id,
                        'salary' => $designationSalary->salary,
                        'grade' => $designationSalary->grade,
                        'allowance' => $designationSalary->allowance,
                        'micro_finance' => $designationSalary->micro_finance,
                        'local_allowance' => $designationSalary->local_allowance,
                        'festival_allowance' => $designationSalary->festival_allowance,
                        'home_rent' => $designationSalary->home_rent,
                        'mahangi_allowance' => $designationSalary->mahangi_allowance,
                        'responsibility_allowance' => $designationSalary->responsibility_allowance,
                        'manager_allowance' => $designationSalary->manager_allowance,
                        'total' => $designationSalary->total,
                        'changed_salary' => 0,
                    ]);
                }
            }

            //return redirect()->back()->with(compact('employee','departmentDetails', 'designationDetails', 'countries'));
            Session::flash('success', 'Employee Successfully Updated');
            return redirect()->route('view.employee');

        }

        //Session::flash('info', 'Employee Updated Successfully ');
        //return redirect()->route('edit.employee', ['id' => $employee->id])->with(compact('employee'));
        return view('backend.employee.edit')->with(compact('employee','departmentDetails','designationDetails','countries' ));

    }

    public function deleteEmployee($id = null){

        $emp = Employee::findOrFail($id);

        $payrolls = Payroll::where('employee_id', $id)->get();
        foreach ($payrolls as $payroll){
            $payroll->salaryAddition()->delete();
            $payroll->salaryDeduction()->delete();
            $payroll->delete();
        }


        //die(print_r($cat->categories()));
        $emp->employeeDetails()->delete();
        Employee::findOrFail($id)->delete();

        Session::flash('error', 'Employee Successfully Deleted');

        return redirect()->back();


    }

}
