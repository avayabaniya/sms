<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Create;
use App\Term;

class CreateExamsController extends Controller
{
    public function index(){
    	$arr['create'] = Create::All();
         
   return view('backend.exams.create')->with(compact('arr'));
 }
 public function store(Request $request, Create $create )
    {  
        // echo "hi"; die();
 $this->validate($request,[
         'exam_name'    => 'required|regex:/(^([a-zA-Z]+))/|unique:creates',
         'start'    => 'required',
         'end'    => 'required',
     ],
     [

         'exam_name.required' => ' Exam name is required.',
        'exam_name.regex' => ' Exam name should be in alphabet form.',


         
         
         ]);

            $create->exam_name = ucwords(strtolower($request->exam_name));
            $create->start= $request->start;
            $create->end= $request->end;
           
        $create->save();
         Session::flash('success', 'Exam Create Added Successfully ');
        return redirect()->back();

    }

    public function update(Request $request, $id)
    {  
        // echo "string"; die();
       
      $create = Create::findOrFail($id);
       $this->validate($request,[
         'exam_name'    => 'required',
         'start'    => 'required',
         'end'    => 'required',
         ]);

         // dd($request->all());
            $create->exam_name = $request->exam_name;
            $create->start= $request->start;
            $create->end= $request->end;
        $create->save();
         Session::flash('success', 'Create Exam updated Successfully ');
        return redirect()->back();
    }
public function destroy($id = null)
    {
        $create = Create::findOrFail($id);

        $create->delete();
        Session::flash('error', ' Exam  Successfully Deleted');
        return redirect()->route('create');
    }

}
