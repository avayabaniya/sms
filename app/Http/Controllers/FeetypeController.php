<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeeType;
use App\Room;
use Session;
use DB;

class FeetypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feetype = FeeType::latest()->get();
        $room = Room::all();
        return view ('backend.feetype.index')->with(compact('feetype','room'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = FeeType::where('feetype', ucwords($request->feetype))->where('room_id', $request->room_id)->count();
        if ($check > 0){
            Session::flash('error', 'Same feetype already exists');
            return redirect()->back();
        }
        //dd($request->all());
        $this->validate($request, [

            'feetype' => 'required',

        ]);
        if(empty($request->onetime_fee) ){
            $request->onetime_fee = 0;   
        }
        $feetype = new FeeType;
        $feetype->room_id = $request->room_id;
        $feetype->feetype = $request->feetype;
        $feetype->type = $request->type;
        if ($request->type ==='Onetime' ) {
        $feetype->amount = $request->amount;
          
        }
        elseif ($request->type ==='Monthly') {
        $feetype->amount = $request->amount * 12;
            
        }
        
        Session::flash('success', 'FeeType Added Successfully ');
        $feetype->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feetype = FeeType::find($id);
        return view('backend.feetype.index')->with($feetype);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feetype = FeeType::find($id);
        $feetype->room_id = $request->room_id;
        $feetype->feetype = $request->feetype;
        $feetype->amount = $request->amount;

        $feetype->update();

        Session::flash('success', 'FeeType updated Successfully ');
        return redirect()->back();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feetype = FeeType::findOrFail($id);
        //dd($feetype);
        $feetype->delete();

        Session::flash('error', 'FeeType  Successfully Deleted');
        return redirect()->back();
    }
}