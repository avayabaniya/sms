<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use App\Create;
use App\Exam;
use App\Room;
use App\Section;
use App\Subject;
use App\StudentAdmission;
class ExamsController extends Controller
{

    public function index(){
           $arr['creates'] = Create::All();
    	   $arr['rooms'] = Room::with('section')->get();
            $arr['subjects'] = Subject::All();
            $arr['exams'] = Exam::with('room','subject','create')->orderBy('id','DESC')->get();

             // dd( $arr['exams']);
           
   return view('backend.exams.index')->with(compact('arr'));
 }
 public function store(Request $request, Exam $exam )
    {  
 $this->validate($request,[
        'exam_id' => 'required',
        'room_id'	=> 'required',			
		'subject_id'=> 'required',
        'full_mark'=> 'required|integer',
        'pass_mark'=> 'required|integer',
		      'date'=> 'required',	
        'time_from'=> 'required',
        'time_to'=> 'required',
        ],
        [
            'exam_id.required' => ' Exam name is  required.',
            'room_id.required' => ' Class name is  required.',
            'Subject_id.required' => ' Subject name is  required.',
            'full_mark.required' => ' Full Mark  is  required.',
            'full_mark.integer' => ' Full Mark  in numeric form.',            
            'pass_mark.required' => ' Pass Mark  is  required.', 
            'pass_mark.required' => ' Pass Mark  in  numeric form.',   
            'date.required' => 'Exam date is  required.',
            'time_from.required' => ' Exam start date  is  required.',
            'time_to.required' => ' Exam end date is  required.',


         ]);

            //dd($request->all());

            $exam->exam_id= $request->exam_id;
            $exam->room_id = $request->room_id;
            $exam->subject_id = $request->subject_id;
            $exam->date = $request->date;
            $exam->full_mark = $request->full_mark;
            $exam->pass_mark = $request->pass_mark;
            $exam->time_from = $request->time_from;
            $exam->time_to = $request->time_to;

        $exam->save();
         Session::flash('success', 'Exam Schedule Added Successfully ');
         return redirect()->route('viewexam');

    }

 public function viewexam(){
           $arr['creates'] = Create::All();
           $arr['rooms'] = Room::with('section')->get();
            $arr['subjects'] = Subject::All();
            $arr['exams'] = Exam::with('room','subject','create')->orderBy('id','DESC')->get();

             // dd( $arr['exams']);
           
   return view('backend.exams.show')->with(compact('arr'));
 }
 public function viewreport(){
           $arr['creates'] = Create::All();
           $arr['rooms'] = Room::with('section')->get();
            $arr['subjects'] = Subject::All();
          $arr['student_admissions'] = StudentAdmission::All();
            $arr['exams'] = Exam::with('room','subject','create')->get();

             // dd( $arr['exams']);
           
   return view('backend.exams.exam_report')->with(compact('arr'));
 }
 public function edit($id = null)
    {
        $exam = Exam::findOrFail($id);

          $arr['creates'] = Create::All();
           $arr['rooms'] = Room::with('section')->get();
            $arr['subjects'] = Subject::All();
            $arr['exams'] = Exam::with('room','subject','create')->get();

             // dd( $arr['exams']);
           
   return view('backend.exams.edit')->with(compact('arr','exam'));
       
    }

    public function update(Request $request, $id)
    {  
 $exam = Exam::findOrFail($id);
 
 $this->validate($request,[
        'exam_id' => 'required',
        'room_id' => 'required',      
    'subject_id'=> 'required',
        'full_mark'=> 'required|integer',
        'pass_mark'=> 'required|integer',
          'date'=> 'required',  
        'time_from'=> 'required',
        'time_to'=> 'required',
       
         ]);

            $exam->exam_id= $request->exam_id;
            $exam->room_id = $request->room_id;
            $exam->subject_id = $request->subject_id;
            $exam->date = $request->date;
            $exam->full_mark = $request->full_mark;
            $exam->pass_mark = $request->pass_mark;
            $exam->time_from = $request->time_from;
            $exam->time_to = $request->time_to;

        $exam->save();
         Session::flash('success', 'Exam Schedule updated Successfully ');

        return redirect()->route('viewexam');
        

    }
 public function individual(Request $request)
    {
//dd($request)->all();

       $arr['rooms'] = Room::All();
         $arr['creates'] = Create::All();
          $arr['student_admissions'] = StudentAdmission::All();
           $chooseyear = $request->academic_year;
              $chooseclass = $request->cname;
              $choosecreate = $request->exam_name;
          
          if ($request->isMethod('post')){

   $exams = Exam::with('room')
                ->where('exam_id', $request->exam_name)
                ->where('room_id', $request->cname)
                ->get();

               // dd($exam);
            $search = 1;

          

             return view('backend.exams.individual')->with(compact('arr','exams','search','chooseyear','chooseclass','choosecreate'));
        }

        $search = 0;

          return view('backend.exams.individual')->with(compact('arr','search','student_admissions'));
 
        
    }
 



    public function destroy($id = null)
    {
        $exam = Exam::findOrFail($id);
        $exam->delete();
        Session::flash('error', 'Exam Schedule  Successfully Deleted');
        return redirect()->route('viewexam');
    }




    public function getSelectedDate(Request $request){

        $exam = Create::where('id', $request->current_exam)->first();

        $dates = ['startdate' => $exam->start, 'enddate' =>$exam->end];
        return $dates;
    }

    public function getSelectedSubject(Request $request)
    {

        //return "testttt";

        $subjects = DB::table('subjects')->where('room_id',$request->current_class)->first();
        $subject = $subjects->topic;

        return $subject;
    }
 

}
