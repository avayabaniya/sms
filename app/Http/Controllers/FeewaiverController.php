<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeeSubCategory;
use App\FeeCategory;
use App\FeeWaiver;
use Session;

class FeewaiverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feescategory = FeeCategory::get();
        $feessubcategory = FeeSubCategory::get();
        $feeDetail = FeeWaiver::get();

        //$feeDetails = FeeSubCategoryFine::get();
        //dd($feeDetail);
        return view ('backend.feewaiver.index')->with(compact('feescategory','feessubcategory', 'feeDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->all());
        
         if ($request->isMethod('post'))
        {

        $feewaiver = FeeWaiver::create([
        'feescategory' => $request->feescategory,
        'feessubcategory' => $request->feessubcategory,
        'excemptionordeduction' => $request->excemptionordeduction,
        ]);
        return redirect()->back(); 

    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $feewaives = FeeWaiver::find($id);
        return view('backend.feewaives.index')->with($feewaives); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         $feewaiver = FeeWaiver::find($id);
            $feewaiver->feescategory = $request->input('feescategory');
            $feewaiver->feessubcategory = $request->input('feessubcategory');
            $feewaiver->excemptionordeduction = $request->input('excemptionordeduction');
            $feewaiver->update();
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feewaiver = FeeWaiver::findOrFail($id);
        //dd($feesubcategoryfine);
        $feewaiver->delete();
        return redirect()->back();
    }
}
