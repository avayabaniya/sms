<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Room;
use Session;
class RoomsController extends Controller
{
    public function index(){

    $arr['rooms'] = Room::latest()->get();
   return view('backend.classes.index')->with($arr);
 }

 public function store(Request $request, Room $room )
    {  
       
 $this->validate($request,[
        'cname' => 'required|alpha|unique:rooms|max:10',
        'room_code' => 'nullable|integer|unique:rooms', ],
        [
        'cname.required' => ' Class  name is required.',
        'cname.unique' => ' Class  name is already taken.',

        'cname.alpha' => ' Class  name in alphabet form.',  
        'room_code.integer' => ' Class name in numeric form.',         
         ]);



            $room->cname =ucwords($request->cname);
            if ($request->cname=='one'||$request->cname=='One') {
                $room->room_code = '1';
            }
            elseif ($request->cname=='two'||$request->cname=='Two') {
                $room->room_code = '2';
            }
            elseif ($request->cname=='three'||$request->cname=='Three') {
                $room->room_code = '3';
            }
            elseif ($request->cname=='four'||$request->cname=='Four') {
                $room->room_code = '4';
            }
            elseif ($request->cname=='five'||$request->cname=='Five') {
                $room->room_code = '5';
            }
            elseif ($request->cname=='six'||$request->cname=='Six') {
                $room->room_code = '6';
            }
            elseif ($request->cname=='seven'||$request->cname=='Seven') {
                $room->room_code = '7';
            }
            elseif ($request->cname=='eight'||$request->cname=='Eight') {
                $room->room_code = '8';
            }
            elseif ($request->cname=='nine'||$request->cname=='Nine') {
                $room->room_code = '9';
            }
            elseif ($request->cname=='ten'||$request->cname=='Ten') {
                $room->room_code = '10';
            }
            elseif ($request->cname=='eleven'||$request->cname=='Eleven') {
                $room->room_code = '11';
            }
            elseif ($request->cname=='twelve'||$request->cname=='Twelve') {
                $room->room_code = '12';
            }
            elseif ($request->cname=='lkg'||$request->cname=='Lkg') {
                $room->room_code = '13';
            }
            elseif ($request->cname=='ukg'||$request->cname=='Ukg') {
                $room->room_code = '14';
            }
            elseif ($request->cname=='nursery'||$request->cname=='Nursery') {
                $room->room_code = '15';
            }
            
            else {
               $room->room_code =  rand(16,25);
                
            }
          
        $room->save();
         Session::flash('success', 'Class Added Successfully ');
        return redirect()->back();

    }

    public function update(Request $request, $id)
    {  
        // echo "string"; die();
       
            $room = Room::findOrFail($id);

         $this->validate($request,[
        'cname' => 'required',
        'room_code' => 'required',          
         ]);

            $room->cname = $request->cname;
            $room->room_code = $request->room_code;

        $room->save();
         Session::flash('success', 'Class updated Successfully ');
        return redirect()->back();


    }
     public function destroy($id = null)
    {
        $rom = Room::findOrFail($id);

        $rom->delete();
        Session::flash('error', 'class  Successfully Deleted');
        return redirect()->route('classes');
    }
 

 
}