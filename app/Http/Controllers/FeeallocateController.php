<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeeSubCategory;
use App\FeeCategory;
use App\FeeAllocate;
use Session;


class FeeallocateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feecategory = FeeCategory::get();
        $feesubcategory = FeeSubCategory::get();
        $feeDetail = FeeAllocate::get();

        return view('backend.feeallocation.index', compact('feecategory','feesubcategory', 'feeDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //dd($request->all());
        
         if ($request->isMethod('post'))
        {

        $feeallocate = FeeAllocate::create([
        'feecategory' => $request->feecategory,
        'feesubcategory' => $request->feesubcategory,
        'feefor' => $request->feefor,
        ]);
        return redirect()->back(); 

    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feeallocate = FeeAllocate::find($id);
        return view('backend.feeallocate.index')->with($feeallocate); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           
            $feeallocate = FeeAllocate::find($id);
            $feeallocate->feecategory = $request->input('feecategory');
            $feeallocate->feesubcategory = $request->input('feesubcategory');
            $feeallocate->feefor = $request->input('feefor');
            $feeallocate->update();
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feeallocate = FeeAllocate::findOrFail($id);
        //dd($feesubcategoryfine);
        $feeallocate->delete();
        return redirect()->back();
    }
}
