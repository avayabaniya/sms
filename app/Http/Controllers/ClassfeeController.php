<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassFee;
use App\room;
use App\FeeType;
use Session;

class ClassfeeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classfee = ClassFee::all();
        $room = Room::get();
        $feetype = FeeType::all();
        return view ('backend.classfee.index')->with(compact('classfee', 'room', 'feetype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'room' => 'required',
            'amount' => 'required', 
        ]);

             $classfee = new ClassFee;
             $classfee->room = $request->room;
             $classfee->amount = $request->amount;
             Session::flash('success', 'ClassFee Added Successfully ');
             $classfee->save();
             return redirect()->back(); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classfee = ClassFee::find($id);
        $room = Room::get();
        $amount = amount::get();

        return redirect()->back(); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
            $classfee = ClassFee::find($id);
            $classfee->room = $request->input('room');
            $classfee->amount = $request->input('amount');
            $classfee->update();
            Session::flash('success', 'classfee update Successfully ');
            return redirect()->back(); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classfee = ClassFee::findOrFail($id);
        //dd($classfee);
        $classfee->delete();
        Session::flash('error', 'classfee  Successfully Deleted');
        return redirect()->back();
    }
}
