<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\support\Facades\Input;
use Image;
use App\Setting;
use App\Country;
use App\Province;
use Session;
class SettingsController extends Controller
{
  
    public function index(){
    $arr['provinces'] = Province::all();
    $arr['countries'] = Country::all();
   $arr['setting'] = Setting::first();
   return view('backend.settings.index')->with($arr);
 }
 

 public function update(Request $request, Setting $setting)

    {   

// dd($request);
        
        $this->validate($request,[

        'name' => 'required',
        'province' => 'required',
        'email' => 'required',
        'number' => 'required',
        'state' => 'required',
        'city' => 'required',
        'pcode' => 'required',
        'pnumber' => 'required',
        'cnumber' => 'required',
        'icode' => 'required',
        'fax' => 'required',
        'facebook' => 'required|url',
        'twitter' => 'required|url',
        'youtube' => 'required|url',
        'instagram' => 'required|url',
      ]);
    	



    		$setting = Setting::firstorFail($setting->id);

     if (!empty($request->image)){

                $image_tmp = Input::file('image');

                if ($image_tmp->isValid()) {
                    
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $image_path = 'public/backend/images/setting/'.$filename;
                   
                    Image::make($image_tmp)->save($image_path);
                                   
                    $setting->image = $filename;
                }

            }
            else
            {
                $setting->image = $request->current_image;
            }


            if (!empty($request->image1)){
       


                $img_tmp = Input::file('image1');

                if ($img_tmp->isValid()) {
                    
                    $extension = $img_tmp->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $image_path = 'public/backend/images/setting/'.$filename;
                   
                    Image::make($img_tmp)->save($image_path);
                                   
                    $setting->image1 = $filename;
                }

            }
            else
            {
                $setting->image = $request->current_image;
            }
        
            $setting->name =ucwords(strtolower( $request->name));
            $setting->province =ucwords( strtolower($request->province));
            $setting->email = strtolower($request->email);
            $setting->number = $request->number;
            $setting->state = ucwords($request->state);
            $setting->city = ucwords($request->city);
            $setting->pcode = $request->pcode;
            $setting->country = $request->country;
            $setting->icode = $request->icode;
            $setting->fax = $request->fax;
            $setting->pnumber = $request->pnumber;
            $setting->cnumber = $request->cnumber;
            $setting->facebook = $request->facebook;
            $setting->twitter = $request->twitter;
            $setting->youtube = $request->youtube;
            $setting->instagram = $request->instagram;
        
        $setting->save();
         Session::flash('success', 'Info Updated Successfully ');
        return redirect()->back();

    }
}
