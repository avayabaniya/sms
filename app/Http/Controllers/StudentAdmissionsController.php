<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentAdmission;
use App\Setting;
use App\Student;
use App\Room;
use App\Section;
use App\Country;
use App\Province;
use App\Nationality;
use Image;
use Illuminate\Support\Facades\Input;

use Session;

class StudentAdmissionsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $arr['admission'] = StudentAdmission::all();
        $arr['setting'] = Setting::all();
        $arr['student'] = Student::all();
        $arr['room'] = Room::all();
        $arr['section'] = Section::all();
        return view ('backend.studentcategories.list')->with($arr);

    }

    public function idcard_index()
    {
        $arr['admission'] = StudentAdmission::all();
        $arr['setting'] = Setting::all();
        $arr['student'] = Student::all();
        $arr['room'] = Room::all();
        $arr['section'] = Section::all();
        return view ('welcome')->with($arr);
    }

    public function guardian_index()
    {
        //echo "<pre>"; print_r($studentadmission); die();
    
        $arr['admission'] = StudentAdmission::all();
        return view ('backend.guardians.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classCount = Room::count();
        $sectionCount = Section::count();
        $student_idCount = Student::count();

           if( $classCount == 0)
           {
                Session::flash('success', 'No class record Found please add to continue ');
                return redirect( route('classes'));
            }
            
            if( $student_idCount == 0)
            {
                Session::flash('success', 'No Student discount record Found please add to continue ');
                return redirect( route('studentcategory'));
            }
       
        $arr['student'] = Student::all();
        $arr['admission'] = StudentAdmission::all();
        $arr['room'] = Room::all();
        $arr['setting'] = Setting::all();
        $arr['section'] = Section::all();
        $arr['countries'] = Country::all();
        $arr['provinces'] = Province::all();
        $arr['nationalities'] = Nationality::all();
        return view ('backend.studentadmissions.index')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StudentAdmission $studentadmission)
    {
         //"<pre>";print_r($request->all());die;
        
        //echo "<pre>";print_r($request->all());die;

        $this->validate($request, [
            'academic_year' => 'required|digits:4|integer|min:2000|max:'.(date('Y')+1),
            'register_number' => 'required|unique:student_admissions',
            'joining_date' => 'required|date|after:dob',
            'room_id' => 'required',
            'section_id' => 'nullable',
            'first_name' => 'required|alpha|max:20',
            'middle_name' => 'nullable|alpha|max:20',
            'last_name' => 'required|alpha|max:20',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif',
            'dob' => 'required|date|before:joining_date',
            'gender' => 'required',
            'blood_group' => 'nullable',
            'birth_place' => 'required',
            'nationality' => 'required',
            'mother_tongue' => 'required',
            'student_id' => 'nullable',
            'religion' => 'required',
            'permanent_street' => 'nullable',
            'permanent_municipality' => 'required',
            'permanent_ward_number' => 'nullable|integer|min:0',
            'province' => 'required',
            'country' => 'required',
            'temporary_street' => 'nullable|string',
            'temporary_municipality' => 'required|alpha',
            'temporary_ward_number' => 'nullable|integer|min:0',
            'temporary_country' => 'required',
            'temporary_province' => 'required',
            'phone_no' => 'nullable|numeric|digits_between:9,14|unique:student_admissions',
            'mobile_no' => 'nullable|numeric|digits_between:10,15|unique:student_admissions',
            'email' => 'nullable|email|unique:student_admissions',
            'pname' => 'required|regex:/(^([a-zA-Z]+))/',
            'relation' => 'required|string',
            'education' => 'required',
            'occupation' => 'required',
            'income' => 'required|numeric|digits_between:1,12',
            'pprovince' => 'nullable',
            'pcountry' => 'nullable',
            'parent_street' => 'nullable|string',
            'parent_municipality' => 'nullable|alpha',
            'parent_ward_number' => 'nullable|integer',
            'pphone' => 'nullable|numeric|digits_between:9,14|min:9|unique:student_admissions',
            'pmobile' => 'required|numeric|digits_between:10,15|unique:student_admissions',
            'pemail' => 'nullable|email|unique:student_admissions',

            ],[
                'pname.required' => 'Parent\'s Name is Required',
		'room_id.required' => 'Class Field is Required',
                'relation.required' => 'Parent\'s Relation is Required',
                'education.required' => 'Parent\'s Education is Required',
                'occupation.required' => 'Parent\'s Occupation is Required',
                'income.required' => 'Parent\'s  Income is Required',
                'pphone.min' => 'Parent\'s Phone Number Must be atleast 9 digits',
                'pmobile.min' => 'Parent\'s Mobile Number Must be atleast 10 digits',
                'pmobile.required' => 'Parent\'s Mobile Number is Required'

            ]);

        if ($request->hasFile('image'))
        {
            $image_tmp = Input::file('image');
            if ($image_tmp->isValid()) 
            {
                   
                   $extension = $image_tmp->getClientOriginalExtension();
                   $filename = rand(111,99999).'.'.$extension;
                   $large_image_path = 'public/backend/images/student/'.$filename;
                  

                   //Resize Images
                   Image::make($image_tmp)->save($large_image_path);
                   

                   //Store image name in abouts table
                   $studentadmission->image = $filename;
            }
        }
        else
        {
            
            if ($request->gender === "Male") {
            $studentadmission->image = 'male.png';
                
            }
            elseif ($request->gender === "Female") {
            $studentadmission->image = 'female.png';
            }
            else
            $studentadmission->image = 'other.png';

        }

         if ($request->checkbox == 1) 
        {
            $parent_ward_number = $request->permanent_ward_number;
            $parent_street = $request->permanent_street;
            $parent_municipality = $request->permanent_municipality;
            $pcountry = $request->country;
            $pprovince = $request->province;
        }
        elseif ($request->checkbox == 2) 
        {
            $parent_ward_number = $request->temporary_ward_number;
            $parent_street = $request->temporary_street;
            $parent_municipality = $request->temporary_municipality;
            $pcountry = $request->temporary_country;
            $pprovince = $request->temporary_province;
        }
        else 
        {
            $parent_ward_number = $request->parent_ward_number;
            $parent_street = $request->parent_street;
            $parent_municipality = $request->parent_municipality;
            $pcountry = $request->pcountry;
            $pprovince = $request->pprovince;
        }

        $studentadmission->academic_year = $request->academic_year;
        $studentadmission->register_number = $request->register_number;
        $studentadmission->admission_number = $request->admission_number;
        $studentadmission->joining_date = $request->joining_date;
        $studentadmission->room_id = $request->room_id;
        $studentadmission->section_id = $request->section_id;
        $studentadmission->first_name = ucfirst(strtolower($request->first_name));
        $studentadmission->middle_name = ucfirst(strtolower($request->middle_name));
        $studentadmission->last_name = ucfirst(strtolower($request->last_name));
        $studentadmission->dob = $request->dob;
        $studentadmission->gender = $request->gender;
        $studentadmission->blood_group = $request->blood_group;
        $studentadmission->birth_place = ucfirst(strtolower($request->birth_place));
        $studentadmission->nationality = $request->nationality;
        $studentadmission->mother_tongue = ucfirst(strtolower($request->mother_tongue));
        $studentadmission->student_id = $request->student_id;
        $studentadmission->religion = ucfirst(strtolower($request->religion));        
               
        $studentadmission->province = $request->province;        
        $studentadmission->country = $request->country;        
        $studentadmission->phone_no = $request->phone_no;        
        $studentadmission->mobile_no = $request->mobile_no;        
        $studentadmission->email = strtolower($request->email);        
        $studentadmission->pname =ucwords($request->pname);
        $studentadmission->relation  =ucfirst(strtolower($request->relation));
        $studentadmission->education  =ucfirst(strtolower($request->education));
        $studentadmission->occupation  =ucfirst(strtolower($request->occupation));
        $studentadmission->income  =$request->income;
        $studentadmission->parent_municipality  =ucfirst(strtolower($parent_municipality));
        $studentadmission->parent_street = ucfirst(strtolower($parent_street));
        $studentadmission->parent_ward_number = $parent_ward_number;
        $studentadmission->pprovince  =$pprovince;
        $studentadmission->pcountry  =$pcountry;
        $studentadmission->pphone  =$request->pphone;

        $studentadmission->permanent_municipality  =ucfirst(strtolower($request->permanent_municipality));
        $studentadmission->permanent_street  =ucfirst(strtolower($request->permanent_street));
        $studentadmission->permanent_ward_number  =$request->permanent_ward_number;

        $studentadmission->temporary_municipality  =ucfirst(strtolower($request->temporary_municipality));
        $studentadmission->temporary_street  =ucfirst(strtolower($request->temporary_street));
        $studentadmission->temporary_ward_number  =$request->temporary_ward_number;
        $studentadmission->temporary_country  =$request->temporary_country;
        $studentadmission->temporary_province  =$request->temporary_province;

        $studentadmission->pmobile  =$request->pmobile;
        $studentadmission->pemail  =strtolower($request->pemail);  

        $studentadmission->save();
        Session::flash('success', 'student Successfully Added');

        return redirect()->route('studentadmissions.index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $arr['admission'] = StudentAdmission::all();
        return view ('backend.guardians.index')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(StudentAdmission $studentadmission)
    {
        $arr['student'] = Student::all();
        $arr['room'] = Room::all();
        $arr['section'] = Section::all();
        $arr['countries'] = Country::all();
        $arr['provinces'] = Province::all();
        $arr['nationalities'] = Nationality::all();
        $arr['studentadmission']= $studentadmission;
        return view('backend.studentadmissions.edit')->with($arr);
    
    }
    public function editGuardian(StudentAdmission $studentadmission, $id)
    {
        $arr['student'] = Student::all();
        $arr['countries'] = Country::all();
        $arr['provinces'] = Province::all();
        $arr['nationalities'] = Nationality::all();
         $arr['studentadmission']= StudentAdmission::findOrFail($id);
        return view('backend.studentadmissions.editguardian')->with($arr);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentAdmission $studentadmission)
    { 
             $this->validate($request, [
            'academic_year' => 'required|digits:4|integer|min:2000|max:'.(date('Y')+1),
            'register_number' => 'required',
            'joining_date' => 'required|date|after:dob',
            'room_id' => 'required',
            'section_id' => 'nullable',
            'first_name' => 'required|alpha|max:20',
            'middle_name' => 'nullable|alpha|max:20',
            'last_name' => 'required|alpha|max:20',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif',
            'dob' => 'required|date|before:joining_date',
            'gender' => 'required',
            'blood_group' => 'nullable',
            'birth_place' => 'required',
            'nationality' => 'required',
            'mother_tongue' => 'required',
            'student_id' => 'nullable',
            'religion' => 'required',
            'permanent_street' => 'nullable',
            'permanent_municipality' => 'required',
            'permanent_ward_number' => 'nullable|numeric|min:1',
            'province' => 'required',
            'country' => 'required',
            'temporary_street' => 'nullable|string',
            'temporary_municipality' => 'required|string',
            'temporary_ward_number' => 'nullable|integer|min:1',
            'temporary_country' => 'required',
            'temporary_province' => 'required',
            'phone_no' => 'nullable|min:9',
            'rollno' => 'nullable|numeric|min:1',
            'mobile_no' => 'nullable|digits_between:10,15',
            'email' => 'nullable|email',
            ],[
                'rollno.min' => 'Roll Number must be atleast 1',
                'permanent_ward_number.min' => 'Ward Number Starts from 1',
                'temporary_ward_number.min' => 'Ward Number Starts from 1',

            ]);

        if ($request->hasFile('image'))
        {
               $image_tmp = Input::file('image');
               if ($image_tmp->isValid()) 
               {
                   //echo "test";die;
                   $extension = $image_tmp->getClientOriginalExtension();
                   $filename = rand(111,99999).'.'.$extension;
                   $large_image_path = 'public/backend/images/student/'.$filename;
                    $currentfilename= $studentadmission->image;

                    
                   //Resize Images
                   Image::make($image_tmp)->save($large_image_path);

                   $studentadmission->image = $filename;
                }
                else
                {
                        //Store image name in products table
                        $studentadmission->image = $currentfilename;
                }
        }

        $studentadmission->academic_year = $request->academic_year;
        $studentadmission->register_number = $request->register_number;
        $studentadmission->admission_number = $request->admission_number;
        $studentadmission->joining_date = $request->joining_date;
        $studentadmission->room_id = $request->room_id;
        $studentadmission->rollno = 'not assiged';
        $studentadmission->section_id = $request->section_id;
        $studentadmission->rollno = $request->rollno;
        $studentadmission->first_name = ucfirst(strtolower($request->first_name));
        $studentadmission->middle_name = ucfirst(strtolower($request->middle_name));
        $studentadmission->last_name = ucfirst(strtolower($request->last_name));
        $studentadmission->dob = $request->dob;
        $studentadmission->gender = $request->gender;
        $studentadmission->blood_group = $request->blood_group;
        $studentadmission->birth_place = ucfirst(strtolower($request->birth_place));
        $studentadmission->nationality = $request->nationality;
        $studentadmission->mother_tongue = ucfirst(strtolower($request->mother_tongue));
        $studentadmission->student_id = $request->student_id;
        $studentadmission->religion = ucfirst(strtolower($request->religion)); 
        $studentadmission->permanent_municipality  =ucfirst(strtolower($request->permanent_municipality));
        $studentadmission->permanent_street  =ucfirst(strtolower($request->permanent_street));
        $studentadmission->permanent_ward_number  =$request->permanent_ward_number;

        $studentadmission->temporary_municipality  =ucfirst(strtolower($request->temporary_municipality));
        $studentadmission->temporary_street  =ucfirst(strtolower($request->temporary_street));
        $studentadmission->temporary_ward_number  =$request->temporary_ward_number;
        $studentadmission->temporary_country  =$request->temporary_country;
        $studentadmission->temporary_province  =$request->temporary_province;       
               
        $studentadmission->province = $request->province;        
        $studentadmission->country = $request->country;        
        $studentadmission->phone_no = $request->phone_no;        
        $studentadmission->mobile_no = $request->mobile_no;        
        $studentadmission->email = strtolower($request->email);
        $studentadmission->pname  = ucfirst(strtolower($request->pname));
            $studentadmission->relation  =ucfirst(strtolower($request->relation));
            $studentadmission->education  =ucfirst(strtolower($request->education));
            $studentadmission->occupation  =ucfirst(strtolower($request->occupation));
            $studentadmission->income  =$request->income;
            $studentadmission->parent_municipality  =ucfirst(strtolower($request->parent_municipality));
            $studentadmission->parent_street  =ucfirst(strtolower($request->parent_street));
            $studentadmission->parent_ward_number  =ucfirst(strtolower($request->parent_ward_number));
            $studentadmission->pcountry  =$request->pcountry;
            $studentadmission->pprovince  =$request->pprovince;
            $studentadmission->pphone  =$request->pphone;
            $studentadmission->pmobile  =$request->pmobile;
            $studentadmission->pemail  =strtolower($request->pemail); 

                   /*echo "<pre>"; print_r($request->all()); die();*/
            $studentadmission->save();

           /* if ($studentadmission->pname != $request->pname || $studentadmission->relation != $request->relation || $studentadmission->education != $request->education || $studentadmission->occupation != $request->occupation|| $studentadmission->income != $request->income|| $studentadmission->address != $request->address|| $studentadmission->pcity != $request->pcity|| $studentadmission->pcountry != $request->pcountry|| $studentadmission->pprovince != $request->pprovince|| $studentadmission->pphone != $request->pphone|| $studentadmission->pmobile != $request->pmobile|| $studentadmission->pemail != $request->pemail)  
                {*/
                     
                    if ($request->check == 1) {
                        $this->validate($request, [
            'pname' => 'required',
            'relation' => 'required|alpha',
            'education' => 'required',
            'occupation' => 'required',
            'income' => 'required',
            'pprovince' => 'nullable',
            'pcountry' => 'nullable',
            'parent_street' => 'nullable|alpha',
            'parent_municipality' => 'nullable|alpha',
            'parent_ward_number' => 'nullable|integer',
            'pphone' => 'nullable|min:9',
            'pmobile' => 'required|min:10|max:15',
            'pemail' => 'nullable|email',

            ]);
            $studentadmission->pname  = ucfirst(strtolower($request->pname));
            $studentadmission->relation  =ucfirst(strtolower($request->relation));
            $studentadmission->education  =ucfirst(strtolower($request->education));
            $studentadmission->occupation  =ucfirst(strtolower($request->occupation));
            $studentadmission->income  =$request->income;
            $studentadmission->parent_municipality  =ucfirst(strtolower($request->parent_municipality));
            $studentadmission->parent_street  =ucfirst(strtolower($request->parent_street));
            $studentadmission->parent_ward_number  =ucfirst(strtolower($request->parent_ward_number));
            $studentadmission->pcountry  =$request->pcountry;
            $studentadmission->pprovince  =$request->pprovince;
            $studentadmission->pphone  =$request->pphone;
            $studentadmission->pmobile  =$request->pmobile;
            $studentadmission->pemail  =strtolower($request->pemail);
            $studentadmission->save();

            return redirect()->route('guardian_index');
            
       }
            Session::flash('success', 'Update Successful');

            return redirect()->route('studentadmissions.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentAdmission $studentadmission)
    {
        $studentadmission->delete();
        Session::flash('success', 'Student Detail Deleted');
        return redirect()->route('studentadmissions.index');
    }
}
