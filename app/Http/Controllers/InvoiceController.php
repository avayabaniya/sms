<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\FeeType;
use App\Room;
Use App\Section;
Use App\Student;
Use App\StudentAdmission;
use App\Setting;
use Illuminate\Routing\Controller;
use Session;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $invoice = FeeType::all();
        //$feeDetails = Invoice::all();
        $feeDetails = Invoice::with('studentadmission', 'room', 'section', 'student')->get();
        $rooms = Room::all();
        $sections = Section::all();
        $student = StudentAdmission::all();
        $setting = Setting::all();
        $invoices = Invoice::all();
        $studentdiscountcategory = Student::all();
        
        // dd($feeDetails);
        return view ('backend.invoice.index',compact('invoice','feeDetails','room','sections', 'student', 'studentdiscountcategory'));
    }

    public function store(Request $request)
    {

        // if($request->feetype->onetime_fee)

        //dd($request->all());
        /* $this->validate($request, [
                'room' => 'required',
                'section' => 'required',
                'student' => 'required',
                'sdc' => 'required',
                'feetype' => 'required',
                'amount' => 'required',
                'dueamount' => 'required',
                'date' => 'required',
                ]);*/
        //   $invoice   = Invoice::create([
        //   'room'     => 1,
        //   'section'  => $request->section,
        //   'student'  => $request->student,
        //   'sdc'      => $request->sdc,
        //   'feetype'  => $request->feetype,
        //   'amount'   => $request->amount,
        //   // 'dueamount'=> $request->dueamount,
        //   'date'     => $request->date,
        // ]);


        $invoice = new Invoice;
        $invoice->room_id = $request->room_id;
        $invoice->section_id = $request->section_id;
        $invoice->student_id = $request->student_id;
        $invoice->student_admission_id = $request->student_admission_id;
        $invoice->feetype = $request->feetype;
        $invoice->date = $request->date;

        Session::flash('success', 'Invoice Added Successfully ');
        $invoice->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editinvoice($id)
    {

        //dd('test');
        $invoice = Invoice::with('student')->where('student_admission_id', $id)->latest()->first();
        // dd($invoice);
        $room = Room::all();
        $sections = Section::all();
        $studentadmission = StudentAdmission::where('id', $id)->first();
        $studentdiscountcategory = Student::all();
        $feetype = FeeType::all();
        $setting = Setting::all();

        // dd($feetype);
       
        return view('backend.invoice.show')->with(compact('invoice', 'room', 'sections', 'setting', 'feetype', 'studentadmission', 'studentdiscountcategory'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $invoice = Invoice::find($id);
        $room = Room::all();
        $sections = Section::all();
        $studentadmission = StudentAdmission::all();
        $studentdiscountcategory = Student::all();
        // $feetype = FeeType::get();
        return view('backend.invoice.edit')->with(compact('invoice', 'room', 'feeDetails', 'sections', 'studentadmission', 'studentdiscountcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $invoice = Invoice::find($id);
        $invoice->room_id = $request->input('room_id');
        $invoice->section_id = $request->input('section_id');
        $invoice->student_id = $request->input('student_id');
        $invoice->student_admission_id = $request->input('student_admission_id');
        // $invoice->feetype = $request->input('feetype');
        // $invoice->amount = $request->input('amount');
        $invoice->date = $request->input('date');
        $invoice->update();
        Session::flash('success', 'Invoice update Successfully ');
        // return redirect()->back();
        // return view('backend.invoice.index');
        return redirect()->route('invoice.index');
    }

    public function getSelectedSection(Request $request)
    {
        $sections = Section::where('room_id',$request->current_class)->get();

        //$section = $sections->sname;
        //echo "<pre>"; print_r($section);die;

        return $sections;
    }


     public function getSelectedStudentadmission(Request $request)
    {
        $studentadmission = StudentAdmission::where('section_id',$request->current_section)->get();
        // $section = $studentadmission->first_name;
        //echo "<pre>"; print_r($section);die;
        return $studentadmission;
    }

     public function getSelectedfeetype(Request $request)
    {
        $feetype = FeeType::where('feetype',$request->current_feetype)->get();
        // $feetype = $feetypes->feetype;
        //echo "<pre>"; print_r($section);die;
        return $feetype;
    }

    public function viewdetails(Request $request){
        
        $student = StudentAdmission::find($request->id);
        $room = Room::all();
        $sections = Section::all();
        $studentadmission = StudentAdmission::all();
        return view('backend.invoice.search', compact('student, room, sections, studentadmission'));
    }

    

    public function storeinvoice(Request $request, Invoice $invoice)
    {       if ($request->total_amount == $request->paid_amount) 
        {
                    $invoice->status = '3';
        }
        elseif ($request->paid_amount != '' && $request->paid_amount > 0 && $request->total_amount > $request->paid_amount) 
        {
                    $invoice->status = '2';
        }
        elseif ($request->paid_amount== '') 
        {
                    $invoice->status = '1';
        }
        if ($request->total_amount < $request->paid_amount) {
            Session::flash('error', 'Amount to be paid cannot be greater than total amount');

            return redirect()->back();
        }
            $invoice->paid_amount = $request->paid_amount;
            $invoice->total_amount = $request->total_amount;
            $invoice->partial = $request->partial;
            $invoice->student_id = $request->student_id;
            $invoice->room_id = $request->room_id;
            $invoice->section_id = $request->section_id;
            $invoice->student_admission_id = $request->student_admission_id;
            $invoice->save();
            Session::flash('success', 'Payment Succesful');
            return redirect()->route('invoice.index');

    }


    
    public function destroy($id)
    {
        $invoice = Invoice::findOrFail($id);
        //dd($invoice);
        $invoice->delete();
        Session::flash('error', 'Invoice  Successfully Deleted');
        return redirect()->back();
    }
}