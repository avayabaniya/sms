<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeeSubCategory;
use App\FeeCategory;
use Session;

class FeesubcategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feesubcategory = FeeCategory::get();
        $feeDetail = FeeSubCategory::get();
        //dd($feesubcategory);
        return view ('backend.feesubcategory.index')->with(compact('feesubcategory','feeDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

   
        // dd($request->all());
        if ($request->isMethod('post'))
        {

        $feesubcategory = FeeSubCategory::create([
        'feecategory' => $request->feecategory,
        'feesubcategoryname' => $request->feesubcategoryname,
        'amount' => $request->amount,
        'feetype' => $request->feetype,
      ]);
        return redirect()->back(); 
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $feesubcategory = feeSubCategory::find($id);
        return view('backend.feesubcategory.index')->with($feesubcategory);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $feesubcategory = feeSubCategory::find($id);
            $feesubcategory->feecategory = $request->input('feecategory');
            $feesubcategory->feesubcategoryname = $request->input('feesubcategoryname');
            $feesubcategory->amount = $request->input('amount');
            $feesubcategory->feetype = $request->input('feetype');
            $feesubcategory->update();
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feesubcategory = FeeSubCategory::findOrFail($id);

        //dd($feesubcategory);
        $feesubcategory->delete();
        return redirect()->back();
    }

    
}
