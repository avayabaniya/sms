<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentAdmission;
use App\Guardian;
use Session;

class GuardiansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['admission'] = StudentAdmission::all();
        return view ('backend.guardians.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentAdmission $admission)
    {
        $arr['studentadmission']= StudentAdmission::all();
        return view('backend.guardians.edit')->with($arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentAdmission $admission)
    {

        $admission->pname = $request->pname;
        $admission->relation = $request->relation;
        $admission->education = $request->education;
        $admission->occupation = $request->occupation;
        $admission->income = $request->income;
        $admission->address = $request->address;
        $admission->pcity = $request->pcity;
        $admission->pcountry = $request->pcountry;
        $admission->pprovince = $request->pprovince;
        $admission->pphone = $request->pphone;
        $admission->pmobile = $request->mobile;
        $admission->pemail = $request->pemail;       
        $admission->save();
        Session::flash('success', 'guardian Successfully Created');

        return redirect()->route('guardians.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
