<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StudentAdmission;
use App\Room;
use App\Section;
use App\Academic;
use App\Employee;
use Session;

class AcademicsController extends Controller
{
	
    public function index(){
        $employeeCount = Employee::count();

        if( $employeeCount == 0){
             Session::flash('success', 'No employees record Found please add to continue ');
             return redirect( route('add.employee'));
        }
            $arr['student_admissions'] = StudentAdmission::latest()->get();
     		$arr['rooms'] = Room::latest()->get();
   			$arr['sections'] = Section::latest()->get();
   			$arr['academics'] = Academic::latest()->get();
            $arr['employees'] = Employee::latest()->get();
   return view('backend.academic.index')->with(compact('arr'));
 
}
 public function store(Request $request, Academic $academic )
    {  
        
    $this->validate($request, [
        'room_id' => 'required',
        'section_id' => 'required',         
        'batch'=> 'required',       
        'employee_id'=> 'required|unique:academics',],
        [

            'employee_id.required' => '  Employee name field is required.',
            'section_id.required' => '  Section\ name field is required.',
            'room_id.required' => '  Class name field is required.',
            'batch.required' => '  Batch  field is required.',


        
            ]);

            $academic->room_id = $request->room_id;
            $academic->section_id = $request->section_id;
            $academic->batch = $request->batch;
            $academic->employee_id = $request->employee_id;

        $academic->save();
         Session::flash('success', 'Class Teacher allocation Added Successfully ');
        return redirect()->back();

    }

public function edit($id=null){
    //echo "<pre>"; print_r($id); die();

        $academic = Academic::findOrFail($id);

            $arr['student_admissions'] = StudentAdmission::all();
            $arr['rooms'] = Room::all();
            $arr['sections'] = Section::all();
            $arr['academic'] = Academic::findOrFail($id);
            $arr['employees'] = Employee::all();

   return view('backend.academic.edit')->with(compact('arr','academic'));
 
}

public function update(Request $request, $id )
    {  
        //dd($request);

        $academic = Academic::findOrFail($id);


        $this->validate($request, [
        'room_id' => 'required',
        'section_id' => 'required',         
        'batch'=> 'required',       
        'employee_id'=> 'required|unique:academics',],
        [

            'employee_id.required' => '  Employee name field is required.',
            'employee_id.unique' => '  Employee name already taken.',
            'section_id.required' => '  Section name field is required.',
            'room_id.required' => '  Class name field is required.',
            'batch.required' => '  Batch  field is required.',

            ]);

            $academic->room_id = $request->room_id;
            $academic->section_id = $request->section_id;
            $academic->batch = $request->batch;
            $academic->employee_id = $request->employee_id;

        $academic->save();
         Session::flash('success', 'Class Teacher allocation Updated Successfully ');

        return redirect()->route('academic');

    }


     public function destroy($id = null)
    {   
        $aca = Academic::findOrFail($id);

        $aca->delete();
        Session::flash('error', 'Class Teacher Successfully Deleted');
         return redirect()->back();
    }
}
