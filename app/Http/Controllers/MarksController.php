<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Create;
use App\Room;
use App\Subject;
use App\Section;
use App\Exam;
use App\Post;
use App\StudentAdmission;
use Session;
use Response;
use App\Mark;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MarksController extends Controller
{

   
public function markadd(){
            $arr['student_admissions'] = StudentAdmission::All();
            $arr['creates'] = Create::All();
    	      $arr['rooms'] = Room::with('section')->get();
            $arr['subjects'] = Subject::All();
            $arr['exams'] = Exam::with('room','subject','create')->get();


   // dd($arr['exams']->all());

   return view('backend.marks.create')->with(compact('arr'));

 }



  public function subjectmarkadd(Request $request)
  {
  $rooms = DB::table('rooms')
            ->join('student_admissions', 'rooms.id', '=', 'student_admissions.room_id')
            ->join('exams', 'rooms.id', '=', 'exams.room_id')
            ->where('student_admissions.academic_year', $request->academic_year)
              ->where('exams.exam_id', $request->exam_name)
              ->where('student_admissions.room_id', $request->cname)
              ->where('student_admissions.section_id', $request->sname)
              ->where('exams.subject_id', $request->subject_id)

            ->get();

 //dd($rooms);

   return view('backend.marks.mark_add')->with(compact('arr','rooms'));
 }
public function subjectmarksave(Request $request)
  { 


 $this->validate($request,[
        'student_id' => 'required|unique:marks',
      ],

        [
        'student_id.unique' => ' Student is already taken.',
         ]);

        $mark = new Mark();
        $mark->student_id = $request->input('student_id');
        $mark->section_id = $request->input('section_id');
        $mark->room_id = $request->input('room_id');
        $mark->exam_id = $request->input('exam_id');
        $mark->subject_id = $request->input('subject_id');
        $mark->mark = $request->input('mark');

        $mark->save ();
        
return redirect()->route('markadd');

}


 public function classmarkview(Request $request)
  {
         $arr['rooms'] = Room::All();
         $arr['sections'] = Section::All();

          $arr['student_admissions'] = StudentAdmission::All();

           $chooseyear = $request->academic_year;
              $chooseclass = $request->cname;
              $choosesection = $request->sname;

          if ($request->isMethod('post')){
            
                $student_admissions = StudentAdmission::with('room', 'section')
                ->where('academic_year', $request->academic_year)
                ->where('section_id', $request->sname)
                ->where('room_id', $request->cname)
                ->get();

// dd($student_admissions);
                        
            $search = 1;

          
       //dd($StudentAdmission);

             return view('backend.marks.index')->with(compact('arr','rooms','search','student_admissions','chooseyear','chooseclass','choosesection'));
        }

        $search = 0;

          return view('backend.marks.index')->with(compact('arr','search','student_admissions'));
       }
  
 public function markview(Request $request)
 {

$student = StudentAdmission::find($request->id);
          $arr['creates'] = Create::All();
         $arr['rooms'] = Room::with('section')->get();
            $arr['subjects'] = Subject::All();
            $arr['student_admissions'] = StudentAdmission::All();
            $arr['exams'] = Exam::All();
            $marks = Mark::All();

   return view('backend.marks.view')->with(compact('arr','student','marks'));
 }

 

}
