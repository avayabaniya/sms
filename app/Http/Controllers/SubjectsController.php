<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Section;
use App\Employee;
use App\Room;

use Session;

class SubjectsController extends Controller
{
   public function index(){

    $arr['rooms'] = Room::All();
     $arr['subjects'] = Subject::latest()->get();
   return view('backend.subjects.index')->with(compact('arr'));
 }

 public function store(Request $request, Subject $subject )
    { 

        $this->validate($request,[
          'room_id' => 'required|unique:subjects',
          'topic' => 'required|unique:subjects',

        ],
        [
          'topic.required' => ' Subject field is required.',
          'topic.required' => ' Subject field is required.',
          
          'room_id.required' => ' Select The class room.',
          'room_id.unique' => 'Subject for this class already listed.',
        ]);



           // echo "<pre>"; print_r($request->all()); die();
           
            $topic[] = $request->input('topic');
            $subject_name = json_encode($topic);

       // echo "<pre>"; print_r($subject_name); die();
            
               $subjects = Subject::create([
               'room_id'=> $request->room_id,
               
               'topic'=> ucwords($subject_name),
             

           ]);
         Session::flash('success', 'Subjects Added Successfully ');
        return redirect()->back();

    }
public function edit( Request $request, $id)
 {


     $sub = Subject::find($id);

              $arr['rooms'] = Room::All();
            $arr['subjects'] = Subject::find($id);
            

        return view('backend.subjects.edit')->with(compact('arr','sub'));

 }

  public function update(Request $request, $id)
    {  
        $subjects = Subject::find($id);

   // print_r( $request->all()); die();

           $topic[] = $request->input('topic');
            $subject = json_encode($topic);

            //dd($subject);

            $subjects->room_id = $request->room_id;
            $subjects->topic = $subject;




        $subjects->save();
         Session::flash('success', 'Subject Updated Successfully ');
        
        return redirect(Route('subject'));

    }
    
     public function destroy($id = null)
    {
        $sub = Subject::findOrFail($id);

        $sub->delete();
        Session::flash('error', 'Subject  Successfully Deleted');
        return redirect()->route('subject');
    }
}
