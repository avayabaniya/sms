<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

use App\Term;
use App\Room;
use App\Lesson;
use App\Section;
use App\Subject;
use App\Academic;
class TermsController extends Controller
{
     public function index(){
     	$arr['rooms'] = Room::All();
            $arr['subjects'] = Subject::All();
            $arr['academics'] = Academic::All();
            $arr['sections'] = Section::All();
            $arr['terms'] = term::All();
   return view('backend.exams.set')->with(compact('arr'));
 }

 public function store(Request $request, Term $term )
    {  
        // echo "hi"; die();
 $this->validate($request,[
        'term_name' => 'required',
         'sdate'    => 'required|date',
         'edate'      => 'required|date|after:sdate',	

         ]);




            $term->term_name= $request->term_name;
            $term->sdate = $request->sdate;
            $term->edate = $request->edate;


        $term->save();
         Session::flash('success', 'Term Added Successfully ');
        return redirect()->back();

    }


     public function update(Request $request, $id)
    {  
        // echo "string"; die();
       
            $term = Term::findOrFail($id);

         $this->validate($request,[
        'term_name' => 'required',
         'sdate'    => 'required|date',
         'edate'      => 'required|date|after:sdate',   

         ]);

         // dd($request->all());

             $term->term_name= $request->term_name;
            $term->sdate = $request->sdate;
            $term->edate = $request->edate;

        $term->save();
         Session::flash('success', 'Term updated Successfully ');
        return redirect()->back();


    }



public function destroy($id = null)
    {
        $term = Term::findOrFail($id);

        $term->delete();
        Session::flash('error', 'Term  Successfully Deleted');
        return redirect()->route('term');
    }
}
