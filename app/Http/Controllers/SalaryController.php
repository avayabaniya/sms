<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Salary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SalaryController extends Controller
{
    public function addSalary(Request $request, $code = null)
    {
        $this->validate($request,['salary' => 'required|between:0,9999999.99',
                                    'grade' => 'required|between:0,999999.99',
                                    'allowance' => 'required|between:0,999999.99',
                                    'micro_finance' => 'required|between:0,999999.99',
                                    'local_allowance' => 'required|between:0,999999.99',
                                    'festival_allowance' => 'required|between:0,999999.99',
                                    'home_rent' => 'required|between:0,999999.99',
                                    'mahangi_allowance' => 'required|between:0,999999.99',
                                    'mahangi_allowance' => 'required|between:0,999999.99',
                                    'mahangi_allowance' => 'required|between:0,999999.99',
                                   ]);

        if ($request->isMethod('post'))
        {
            $employee = Employee::where('employee_code', $code)->first();

            $salary = Salary::where('employee_id', $employee->id)->first();

            $salary->employee_id = $employee->id;
            $salary->salary = $request->salary;
            $salary->grade = $request->grade;
            $salary->allowance = $request->allowance;
            $salary->micro_finance = $request->micro_finance;
            $salary->local_allowance = $request->local_allowance;
            $salary->festival_allowance = $request->festival_allowance;
            $salary->home_rent = $request->home_rent;
            $salary->mahangi_allowance = $request->mahangi_allowance;
            $salary->responsibility_allowance = $request->responsibility_allowance;
            $salary->manager_allowance = $request->manager_allowance;
            $salary->total = $request->salary + $request->grade + $request->allowance + $request->micro_finance + $request->local_allowance + $request->festival_allowance + $request->home_rent + $request->mahangi_allowance + $request->responsibility_allowance + $request->manager_allowance;
            $salary->changed_salary = 1;
            $salary->save();

            Session::flash('success', 'Employee Salary Successfully changed');
        }
        return redirect()->back();
    }

}
