<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Payroll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PayrollsController extends Controller
{
    public function payrollEmployee($code = null ){

        //dd($id);
        /*$payroll = Payroll::with('salaryDeduction', 'salaryAddition','employee')->where('employee_id', $id)->first();
        if (empty($payroll)){
            $p = new Payroll;
            $p->employee_id = $id;
            $p->month = 'current month';
            $p->salary_per_month = 0.0;
            $p->save();
            $payroll = Payroll::with('salaryDeduction', 'salaryAddition','employee')->where('employee_id', $id)->first();
        }*/

        $employee = Employee::with('designation')->where('employee_code', $code)->firstOrFail();
        if (empty($employee->designation)){
            echo "Payroll for this particular employee not available";die;
        }
        //dd($payroll);
        return view('backend.employee.payroll.view')->with(compact('employee'));
    }

    public function addPayroll(Request $request, $id){

        $employee = Employee::with('designation', 'EmployeeSalary')->find($id);
        $salaryTax = (13/100) * $employee->designation->total;

        if ($request->isMethod('post')){

            $this->validate($request, ['payroll_month' => 'required']);

            $payrollCount = Payroll::where('employee_id', $id)->where('month_id', $request->payroll_month)->count();
            if ($payrollCount > 0){
                $payroll = Payroll::with('salaryAddition', 'salaryDeduction')->where('employee_id', $id)->where('month_id', $request->payroll_month)->first();


                $payroll->salary_per_month = $request->total_payroll;

                $payroll->salaryAddition->over_time_rate = $request->ot_money_hr;
                $payroll->salaryAddition->over_time_hour_worked = $request->ot_hr;

                $payroll->salaryDeduction->pf_fund = $request->pf_fund;
                $payroll->salaryDeduction->retirement_fund = $request->retirement_fund;
                $payroll->salaryDeduction->salary_tax = $request->salary_tax;
                $payroll->salaryDeduction->security_tax = $request->security_tax;

                $payroll->salaryAddition->save();
                $payroll->salaryDeduction->save();
                $payroll->save();

                Session::flash('success', 'Employee Payroll Successfully Updated');
                return redirect(route('view.employee'));

            }else{

                $payroll = new Payroll;
                $payroll->employee_id = $id;
                $payroll->month_id = $request->payroll_month;
                $payroll->salary_per_month = $request->total_payroll;
                $payroll->save();

                DB::table('salary_additions')->insert([
                    'payroll_id' => $payroll->id,
                    'over_time_rate' => $request->ot_money_hr,
                    'over_time_hour_worked' => $request->ot_hr
                ]);

                DB::table('salary_deductions')->insert([
                    'payroll_id' => $payroll->id,
                    'pf_fund' => $request->pf_fund,
                    'retirement_fund' => $request->retirement_fund,
                    'salary_tax' => $request->salary_tax,
                    'security_tax' => $request->security_tax
                ]);

                Session::flash('success', 'New Employee Payroll Successfully Added');
                return redirect(route('view.employee'));
            }


        }
        return view('backend.employee.payroll.add_payroll')->with(compact('employee', 'salaryTax'));
    }

    public function viewDepartmentPayroll(Request $request){

        $departments = Department::all();
        //dd($employees[0]);

        if ($request->isMethod('post')){

            $this->validate($request, ['payroll_month' => 'required',
                                        'department' => 'required']);

            $selectedDepartment = $request->department;
            $selectedMonth = $request->payroll_month;

            /*$employees = Employee::with('payroll','department', 'designation', 'employeeDetails')
                ->whereHas('payroll', function($q) use ($request) {$q->where('month_id', $request->payroll_month);})
                ->where('department_id', $request->department)->get();

            dd($employees);*/

            $payrolls = Payroll::with('employee', 'salaryAddition', 'salaryDeduction')
                ->where('month_id', $request->payroll_month)
                ->whereHas('employee', function($q) use ($request) {$q->where('department_id', $request->department);})
                ->get();

            //dd($payrolls);

            $search = 1;
            return view('backend.employee.payroll.view_payroll')->with(compact( 'departments', 'search', 'selectedDepartment','selectedMonth', 'payrolls' ));

        }

        $search = 0;
        return view('backend.employee.payroll.view_payroll')->with(compact( 'departments', 'search'));
    }

    public function viewIndividualPayroll(Request $request)
    {
        $employees = Employee::all();
        
        if ($request->isMethod('post')){

            /*$payrolls = Payroll::with('employee', 'salaryDeduction', 'salaryAddition')
                ->whereHas('employee', function ($q) use ($request) {$q->where('id', $request->employee);})
                ->get();*/
            $months = DB::table('months')->get();
            $payrolls = Payroll::with('employee', 'salaryDeduction', 'salaryAddition')
                ->where('employee_id', $request->employee)
                ->orderBy('month_id')
                ->get();
            $selectedEmployee = $request->employee;

            //dd($payrolls);

            $search = 1;
            return view('backend.employee.payroll.view_individual_payroll')->with(compact('employees', 'search', 'payrolls', 'selectedEmployee', 'months'));

        }

        $search = 0;
        return view('backend.employee.payroll.view_individual_payroll')->with(compact('employees', 'search'));
    }

    public function viewAnnualPayroll(Request $request){

        $departments = Department::all();

        if ($request->isMethod('post')){

            $this->validate($request, ['payroll_year' => 'required',
                'department' => 'required']);

            $months = DB::table('months')->get();
            $selectedDepartment = $request->department;
            $selectedYear = $request->payroll_year;

            $payrolls = Payroll::with('employee', 'salaryDeduction', 'salaryAddition')
                ->whereHas('employee', function ($q) use ($request) {$q->where('department_id', $request->department);})
                ->orderBy('month_id')
                ->get();

            //check for selected year
            $totalBasicSalary = 0;
            $totalFinalSalary = 0;
            $finalPayroll = [];
            foreach ($payrolls as $payroll ){

                $date = explode("-",$payroll->month_id);
                if ($date[1] == $request->payroll_year){
                    $finalPayroll[] = $payroll;
                    $totalBasicSalary = $payroll->employee->designation->salary + $totalBasicSalary;
                    $totalFinalSalary = $payroll->salary_per_month + $totalFinalSalary;
                }
            }
            Session::forget('error');
            if (sizeof($finalPayroll) == 0){
                Session::flash('error', 'No data available');
                $search = 0;
                return view('backend.employee.payroll.view_annual_payroll')->with(compact('departments', 'search'));
            }


            $search = 1;
            return view('backend.employee.payroll.view_annual_payroll')->with(compact('departments', 'search', 'finalPayroll', 'months','selectedDepartment', 'selectedYear', 'totalFinalSalary', 'totalBasicSalary'));

        }
        $search = 0;
        return view('backend.employee.payroll.view_annual_payroll')->with(compact('departments', 'search'));
    }

    public function sameAsPreviousPayroll($id = null)
    {
        if (Carbon::now()->month != 1) {
            $previousMonth = Carbon::now()->month - 1;
            $currentYear = Carbon::now()->year;

            $currentDate = $previousMonth . "-" . $currentYear;

            $oldPayroll = Payroll::with('salaryAddition', 'salaryDeduction')->where('employee_id', $id)->where('month_id', $currentDate)->first();
            //dd($oldPayroll);

            //Check for payroll of previous month
            $countPayroll = Payroll::where('employee_id', $id)->where('month_id', $currentDate)->count();
            if ($countPayroll == 0) {
                Session::flash('error', 'Payroll of previous month not available');
                return redirect()->back();
            }

            //check for payroll of current month
            $countCurrentPayroll = Payroll::where('employee_id',$id)->where('month_id', Carbon::now()->month . "-" . Carbon::now()->year)->count();
            if ($countCurrentPayroll > 0) {
                Session::flash('error', 'Payroll of this employee for this month already exists');
                return redirect()->back();
            }


            Session::forget('error');

            $payroll = new Payroll;
            $payroll->employee_id = $id;
            $payroll->month_id = Carbon::now()->month . "-" . Carbon::now()->year;
            $payroll->salary_per_month = $oldPayroll->salary_per_month;
            $payroll->save();

            DB::table('salary_additions')->insert([
                'payroll_id' => $payroll->id,
                'over_time_rate' => $oldPayroll->salaryAddition->over_time_rate,
                'over_time_hour_worked' => $oldPayroll->salaryAddition->over_time_hour_worked
            ]);

            DB::table('salary_deductions')->insert([
                'payroll_id' => $payroll->id,
                'pf_fund' => $oldPayroll->salaryDeduction->pf_fund,
                'retirement_fund' => $oldPayroll->salaryDeduction->retirement_fund,
                'salary_tax' => $oldPayroll->salaryDeduction->salary_tax,
                'security_tax' => $oldPayroll->salaryDeduction->security_tax
            ]);

            Session::flash('success', 'Payroll of previous month successfully added');
            return redirect()->back();


        }else{

            Session::flash('error', 'This is the first month of the year');
            return redirect()->back();

        }
    }

}
