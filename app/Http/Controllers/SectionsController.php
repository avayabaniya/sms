<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use Session;
use App\Room;
class SectionsController extends Controller
{
   public function index(){
    $arr['room'] = Room::all();

   // echo "<pre>"; print_r( $arr); die();

    $arr['sections'] = Section::latest()->get();
   return view('backend.sections.index')->with($arr);
 }


 public function store(Request $request, Section $section )
    {
        $check = Section::where('sname', ucwords($request->sname))->where('room_id', $request->room_id)->count();
        if ($check > 0){
            Session::flash('error', 'Same section already exists');
            return redirect()->back();
        }
       
 $this->validate($request,[
         
        'sname' => 'required',
        'room_id' => 'required'
    ],
        [
        'sname.required' => ' Section  name is required.',
        'room_id.required' => ' Class name is required.',  
         ]);

            $section->sname =ucwords($request->sname);
            $section->room_id = $request->room_id;

        $section->save();
         Session::flash('success', 'Section Added Successfully ');
        return redirect()->back();

    }

     public function update(Request $request, $id)
    {  
        $this->validate($request,[
        'sname' => 'required',
        'scode' => 'required',  
        'room_id' => 'required',  
         'description'=> 'required', 
         ]);
            $section = Section::findOrFail($id); 

            $section->sname = $request->sname;
            $section->room_id = $request->room_id;

        $section->save();
         Session::flash('success', 'Section Updated Successfully ');
        return redirect()->back();

    }
      public function destroy($id = null)
    {
        $sec = Section::findOrFail($id);

        $sec->delete();
        Session::flash('error', 'Section  Successfully Deleted');
        return redirect()->route('sections');
    }
 
}