<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    public function room()
    {
    	return $this->belongsTo('App\Room');
    }
    public function section()
    {
    	return $this->belongsTo('App\Section');
    }
    public function subject()
    {
    	return $this->belongsTo('App\Subject');
    }
}
