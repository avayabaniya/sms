<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Create extends Model
{
    public function exam()
    {
    	return $this->hasOne('App\Exam','exam_id');
    }
}
