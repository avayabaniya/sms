<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassFee extends Model
{
   protected $fillable = ['id', 'class_id'];

   public function room(){
        return $this->belongsTo('App\ClassFee');
    }
    public $timestamps = false;
}
