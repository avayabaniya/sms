<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryDeduction extends Model
{
    protected $fillable = ['payroll_id', 'pf_fund', 'retirement_fund', 'salary_tax', 'security_tax'];

    public function payroll(){
        return $this->belongsTo('App\Payroll');
    }
}
