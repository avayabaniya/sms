<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
        	'name' => 'Admin',
        	'email' => 'admin@admin.com',
        	'password' => bcrypt('password'),
        ]);
        App\UserProfile::create([
        	'user_id' =>$user->id,
        	'avatar' => 'public/uploads/avatars/avatar.png',
        	'about' => 'description',
        	'mobile'=> '9842137489',
        	'address' => 'Kathmandu',

        ]);
    }
}
