<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateStudentAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_admissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('admission_number');
            $table->string('academic_year');
            $table->string('register_number');
            $table->string('joining_date');
            $table->integer('room_id')->nullable();
            $table->integer('section_id')->nullable();
            $table->string('image')->nullable();
            $table->string('rollno')->nullable();
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('dob');
            $table->string('gender');
            $table->string('blood_group')->nullable();
            $table->string('birth_place');
            $table->string('nationality');
            $table->string('mother_tongue');
            $table->integer('student_id')->nullable();
            $table->string('religion');
            $table->string('permanent_municipality');
            $table->string('permanent_street')->nullable();
            $table->string('permanent_ward_number')->nullable();
            $table->string('province');
            $table->string('country');
            $table->string('temporary_country');
            $table->string('temporary_province');
            $table->string('temporary_municipality');
            $table->string('temporary_street')->nullable();
            $table->string('temporary_ward_number')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('email')->nullable();
            $table->string('pname');
            $table->string('relation');
            $table->string('education');
            $table->string('occupation');
            $table->double('income',15, 2);
            $table->string('parent_municipality')->nullable();
            $table->string('parent_ward_number')->nullable();
            $table->string('parent_street')->nullable();
            $table->string('pcountry')->nullable();
            $table->string('pprovince')->nullable();
            $table->string('pphone')->nullable();
            $table->string('pmobile');
            $table->string('pemail')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_admissions');
    }
}