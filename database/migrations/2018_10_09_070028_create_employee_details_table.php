<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->integer('department_id')->nullable();
            $table->integer('designation_id')->nullable();
            $table->string('dob');
            $table->string('address');
            $table->string('country');
            $table->string('phone')->nullable();
            $table->string('mobile');
            $table->string('email');
            $table->string('join_date');
            $table->string('religion');
            $table->string('mother_tongue');
            $table->string('blood_group');
            $table->string('image')->nullable();
            $table->string('resume')->nullable();
            $table->string('qualification');
            $table->text('description')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_details');
    }
}
