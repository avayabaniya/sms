<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeSubCategoryFinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_sub_category_fines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('feecategory');
            $table->string('feessubCategory');
            $table->string('type');
            $table->string('finetype');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_sub_category_fines');
    }
}
