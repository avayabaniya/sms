<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->string('academic_year');
            $table->string('register_number');
            $table->string('joining_date');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('image')->nullable();
            $table->string('dob');
            $table->string('address');
            $table->string('country');
            $table->string('phone_no')->nullable();
            $table->string('mobile_no');
            $table->string('email');
            $table->string('mother_tongue');
            $table->string('category');
            $table->string('religion');
            $table->string('blood_group');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_details');
    }
}
