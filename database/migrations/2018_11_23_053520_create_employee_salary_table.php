<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->unsigned();
            $table->double('salary',15,2)->nullable();
            $table->double('grade',15,2)->nullable();
            $table->double('allowance', 15, 2)->nullable();
            $table->double('micro_finance',15, 2)->nullable();
            $table->double('local_allowance',15,2)->nullable();
            $table->double('festival_allowance',15,2)->nullable();
            $table->double('home_rent',15,2)->nullable();
            $table->double('mahangi_allowance',15,2)->nullable();
            $table->double('responsibility_allowance',15,2)->nullable();
            $table->double('manager_allowance',15,2)->nullable();
            $table->double('total',15,2)->nullable();
            $table->integer('changed_salary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salary');
    }
}
