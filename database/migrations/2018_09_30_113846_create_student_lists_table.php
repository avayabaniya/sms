<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('admission_no');
            $table->string('admission_date');
            $table->string('register_number');
            $table->string('joining_date');
            $table->string('course');
            $table->string('batch');
            $table->string('rollno');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('dob');
            $table->string('gender');
            $table->string('blood_group');
            $table->string('birth_place');
            $table->string('nationality');
            $table->string('mother_tongue');
            $table->string('category');
            $table->string('religion');
            $table->string('caste');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_lists');
    }
}
