@extends('backend.includes.app')
@section('content')

  <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Profile</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active">Profile</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>

                    <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            @foreach($user as $user)

                            <div class="card-body">
                                <center class="m-t-30"><img src="{{asset('public/uploads/avatars/'.$user->userprofile->image)}}" class="img-circle" width="150" />
                                
                                    <h4 class="card-title m-t-10"></h4>
                                    <h6 class="card-subtitle">{{$user->name}}</h6>
                                    <div class="row text-center justify-content-md-center">
                                        @foreach($setting as $setting)
                                            <div class="col-4"><a href="javascript:void(0)" class="link"><font class="font-medium">{{$setting->name}}</font></a></div>
                                           @endforeach
                                    </div>
                                </center>
                            </div>
                            <div>
                                <hr> 
                            </div>
                            <div class="card-body"> <small class="text-muted">Email address </small>
                                <h6>{{$user->email}}</h6> 
                                <small class="text-muted p-t-30 db">Phone</small>
                                <h6>{{$user->userprofile->mobile}}</h6> 
                                <small class="text-muted p-t-30 db">Address</small>
                                <h6> {{$user->userprofile->address }}</h6>
                                <small class="text-muted p-t-30 db">About</small>
                                <h6>{{$user->userprofile->about}} </h6>
                                <br/>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!--second tab-->
                                    <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material" method="post" action="{{ route('userprofile.update',$user->id)}}" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                @method('PUT')
                
                                            <div class="form-group">
                                                <label class="col-md-12">Full Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Admin Admin" class="form-control form-control-line" maxlength="30" name="name" value="{{$user->name}}">
                                                    	<a href="" style="color: red;">{{ $errors -> first('name') }}</a>
                                                </div>
                                            </div>

                                           <div class="form-group">
                                                <label for="image" class="col-md-12">Image</label>
                                                    <input type="file" class="form-control form-control-line" name="image" id="image" onchange="return fileValidation()" placeholder="Select">
                                                    <a href="" style="color: red;">{{ $errors -> first('image') }}</a>
                                           </div>

                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" name="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email" value="{{$user->email}}">
                                                    <a href="" style="color: red;">{{ $errors -> first('email') }}</a>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-12">Phone No</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="mobile" placeholder="123 456 7890" maxlength="15" class="form-control form-control-line" value="{{$user->userprofile->mobile}}">
                                                    <a href="" style="color: red;">{{ $errors -> first('mobile') }}</a>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Address</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="address" placeholder="sankhamul,Kathmandu" maxlength="60" class="form-control form-control-line" value="{{$user->userprofile->address}}">
                                                    
                                                    <a href="" style="color: red;">{{ $errors -> first('address') }}</a>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-12">Message</label>
                                                <div class="col-md-12">
                                                    <textarea rows="5" maxlength="300" name="about" class="form-control form-control-line">{{$user->userprofile->about}}</textarea>
                                                    <a href="" style="color: red;">{{ $errors -> first('about') }}</a>
                                                </div>
                                            </div>

                                            

                                            @endforeach
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>

                @endsection