@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Edit Event</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item">Event</li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.row -->
   <!-- /.container-fluid -->
<div class="content">

<form enctype="multipart/form-data" method="post" action="{{route('updateevent',['id' => $event->id])}}">
<input type="hidden" name="_token" value="{{csrf_token()}}">
@method('PUT')
                       
        <div class="row text-center">
            <div class="col-md-6 col-md-offset-3">
                <div class="card">
                    <div class="card-body"> 
                               <p>Event Type</p>
                        
                            <div class="form-group">
                            	<div class="row">
                                	<label name="name" class="col-md-4 text-center" for="name">Event Name</label>
                               		<div class="col-md-5">
                                		<input type="text" class="form-control" name="name" required="" value="{{ $event->name }}">
                            		</div>
								</div>
							</div>


                             <div class="form-group">
                             	<div class="row">
                                <label name="holiday" class="col-md-4 text-center" for="holiday">Holiday</label>
                                <div class="col-md-5">
                                <input type="checkbox" class="form-control" name="holiday">
                            </div>
                             </div>

                             <div class="form-group">
                             	<div class="row">
                                <label  class="col-md-4 text-center" for="type">Event Type</label>
                                <div class="col-md-5">
                                <select  name="type" class="form-control custom-select">
                                   @foreach($eventtype as $et)
										<option value="{{ $et->title }}" 
										@if($et->title == $event->type)
										selected
										@endif
										> {{ $et->title }}</option>
									@endforeach
								</select>
                                </div>
                             </div>
                         </div>

                            <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 text-center">Description</label>
                                        <br>
                                        <div class="col-md-12">
                                            <textarea id="summernote" class="form-control" name="description" maxlength="200">{!! $event->description !!}</textarea>

                                        </div>
                                    </div>
                            </div>

                             <div class="form-group ">
                             	<div class="row">
	                                <label class="col-md-4 text-center" for="start_date">Start Date</label>
	                                <div class="col-md-5">
	                               		<input type="text" name="start_date" class="form-control mydatepicker" value="{{$event->start_date  }}">
	                             	</div>
	                         	</div>
	                         </div>

                             <div class="form-group ">
                             	<div class="row">
                               		<label class="col-md-4 text-center" for="end_date">End Date</label>
                                	<div class="col-md-5">
                               			<input type="text" name="end_date" class="form-control mydatepicker" value="{{$event->end_date  }}">
                             		</div>
                         		</div>
                         	</div>

                        </div>

                            <div class="form-actions text-center">
                                 <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>    
                            </div>
                   		</form>
                    </div>
                            
                </div>


           	</div>
  </div>                                    
                                
 @endsection