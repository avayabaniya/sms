@extends('backend.includes.app')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Event</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item ">Event</li>
                                <li class="breadcrumb-item active">Add</li>
                            </ol>
                        </div>
                    </div>
                </div>


                <div class="row">
                      <div class="col-lg-4 col-xlg-4 col-md-4">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Add Event</p>
                                  <form class="form-horizontal" method="post" action="{{ route('event.store')}}" enctype="multipart/form-data" >
                                   <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <div class="form-group">
                                        <div class="row">
                                            <label name="name" class="col-md-4" for="name">Event Name<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <input type="text" value="{{ old('name') }}" class="form-control" name="name">
                                                <a href="" style="color: red;">{{ $errors -> first('name') }}</a>
                                            </div>
                                         </div>
                                    </div>

                                    <div class="form-group col-sm-6">
                                          Holiday
                                          <input type="checkbox" id="trigger" name="checkbox" value="1"> 
                                    </div>

                                    <div class="form-group" id="hidden_fields">
                                        <div class="row">
                                            <label  for="type" class="col-md-4">Event Type</label>
                                            <div class="col-md-8">
                                                <input type="text" value="{{ old('type') }}" class="form-control" name="type">
                                                <a href="" style="color: red;">{{ $errors -> first('type') }}</a>
                                            </div>
                                        </div> 
                                    </div>

                                     <div class="form-group">
                                        <div class="row">
                                            <label for="start_date" class="col-md-4">Start Date<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                              <input type="text" class="form-control mydatepicker" name="start_date" value="{{ old('start_date') }}">
                                              <a href="" style="color: red;">{{ $errors -> first('start_date') }}</a>
                                            </div>
                                        </div>
                                      </div>

                                      <div class="form-group">
                                           <div class="row">
                                              <label for="end_date" class="col-md-4">End Date<span style="color: red;">*</span></label>
                                              <div class="col-md-8">
                                                <input type="text" class="form-control mydatepicker" name="end_date" value="{{ old('end_date') }}">
                                                <a href="" style="color: red;">{{ $errors -> first('end_date') }}</a>
                                              </div>
                                            </div>
                                      </div>
                              
                                      <div class="form-group">
                                          <div class="row">
                                              <label class="col-md-4">Description</label>
                                                  <br>
                                              <div class="col-md-12">
                                                  <textarea id="summernote" class="form-control" name="description" z-index="0" maxlength="200">{!! old('description') !!}</textarea>
                                                  <a href="" style="color: red;">{{ $errors -> first('description') }}</a>
                                              </div>
                                          </div>
                                      </div>
                         

                                      <div class="form-actions text-center">
                                         <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
                                      </div>
                                  </form>
                            </div>
                        </div>

                      </div>
              
                      <div class="col-lg-8 col-md-8">
                            <div class="card">
                                  <div class="card-body">
                                                  
                                      <table class="table table-hover table-striped table-bordered">
                                          <thead>
                                              <tr>
                                                  <th>SN</th>
                                                  <th>Event Name</th>
                                                  <th>Start Date</th>
                                                  <th>End Date</th>
                                                  <th>Manage</th>
                                                                  
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @if(count($event))
                                                   @foreach($event as $e)
                                              <tr>
                                                                   
                                                  <td>{{ $loop->index + 1 }}</td>
                                                  <td>{{ $e->name }}</td>
                                                  <td>{{ $e->start_date }}</td>
                                                  <td>{{ $e->end_date }}</td>
                                                  <td>
                                                      <a href="{{ route('edit.event',['id' => $e->id])}}" class="ti-pencil"></a>

                                                      <a href="#" class="ti-eye ti-eye-info" data-toggle="modal" data-target=".bs-example-modal-lg{{ $e->id }}" class="model_img img-responsive" ></a>

                                                      <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
                                                        <form method="post" id="del" class="delete" action="{{ route('events.destroy',$e->id)}}">
                                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                          @method('DELETE')
                                                        </form>
                                                  </td>                   
                                              </tr>

                                              <div class="modal fade bs-example-modal-lg{{ $e->id }}" tabindex="-1" role="dialog"         aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-lg">
                                                  <div class="modal-content">
                                                 
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myLargeModalLabel">Event Detail</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                       <p style="text-align: center;"> Event Name : &nbsp {{$e->name}}</p>
                                                       <p style="text-align: center;"> Event Type: &nbsp {{$e->type}}</p>
                                                       <p style="text-align: center;"> Description: &nbsp {{$e->description}}</p>
                                                       <p style="text-align: center;"> Start Date: &nbsp {{$e->start_date}}</p>
                                                       <p style="text-align: center;"> End Date: &nbsp {{$e->end_date}}</p>
                                                       <p style="text-align: center;"> Description: &nbsp {{$e->description}}</p>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <a href="{{ route('events.edit',$e->id)}}" class="btn btn-info btn-xs btn-xs btn-xs">Edit</a>
                                                        <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                    @endforeach 
                                                    @else
                                                    <tr colspan="3">
                                                    <td>No Events found</td>
                                                    </tr>
                                                    @endif
                                                  </div>
                                              
                                                </div>
                                              </div>
                                                              
                                          </tbody>  
                                      </table>
                                  </div>
                            </div>
                      </div>
                </div>
@endsection