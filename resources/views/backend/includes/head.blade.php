<?php
use \App\Http\Controllers\Controller;
$settings = Controller::settings();
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
                    
    <!-- Favicon icon -->
    @foreach($settings as $setting)
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('public/backend/images/setting/'.$setting->image)}}">
   <title> {{$setting->name}}</title>
    @endforeach

    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="{{asset('public/backend/assets/node_modules/morrisjs/morris.css')}}" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{asset('public/backend/assets/node_modules/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- Morris CSS -->
    <link href="{{asset('public/backend/assets/node_modules/morrisjs/morris.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('public/backend/dist/css/style.min.css')}}" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="{{asset('public/backend/dist/css/pages/dashboard1.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <link href="{{ asset('public/backend/dist/css/pages/file-upload.css')}}" rel="stylesheet">
    <link href=" {{ asset('public/backend/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/backend/dist/css/style.min.css')}}" rel="stylesheet">
    <link href="{{ asset('public/backend/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('public/backend/assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/backend/assets/node_modules/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="nepali.datepicker.v2.2.min.css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

<style type="text/css">
    .toast-top-right {
top: 160px;
margin-right:30px;

}
</style>

    <style>
        .error{
            color: red;
        }
    </style>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
