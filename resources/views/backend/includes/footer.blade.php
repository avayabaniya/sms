<footer class="footer">
    © 2018 Developed by OnlineZeal
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset('public/backend/assets/node_modules/jquery/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="{{asset('public/backend/assets/node_modules/popper/popper.min.js')}}"></script>
<script src="{{asset('public/backend/assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('public/backend/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('public/backend/dist/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('public/backend/dist/js/sidebarmenu.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('public/backend/dist/js/custom.min.js')}}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--morris JavaScript -->
<script src="{{asset('public/backend/assets/node_modules/raphael/raphael-min.js')}}"></script>
<script src="{{asset('public/backend/assets/node_modules/morrisjs/morris.min.js')}}"></script>
<script src="{{asset('public/backend/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<!-- Popup message jquery -->
<script src="{{asset('public/backend/assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
<!-- Chart JS -->
<script src="{{asset('public/backend/dist/js/dashboard1.js')}}"></script>

<script src="{{ asset('public/backend/dist/js/pages/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('public/backend/dist/js/pages/mask.js') }}"></script>

<!-- Date Picker Plugin JavaScript -->
<script src="{{ asset('public/backend/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    // Date Picker
    jQuery('.mydatepicker').datepicker();
</script>

<!-- This is data table -->
<script src="{{asset('public/backend/assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/backend/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/backend/assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('public/backend/assets/node_modules/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>

<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!-- end - This is for export functionality only -->
<script  src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script> 

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
          height:250
        });
    });
  </script>

<script >
  @if(Session::has('success'))

toastr.options.positionClass = 'toast-top-right';
toastr.success('{{ Session::get('success') }}')


@endif
@if(Session::has('error'))

toastr.options.positionClass = 'toast-top-right';
toastr.error('{{ Session::get('error') }}')

@endif
</script>
<script>
    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>

<script type="text/javascript">

jQuery('#date-range').datepicker({
        startDate:new Date(),
        
        toggleActive: true

    });

jQuery('#date-range1').datepicker({
        startDate:new Date(),
        toggleActive: true
    });
</script>
<script type="text/javascript">
     $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });
</script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>
<script type="text/javascript">

    $("#datepickeracademic").datepicker({
        
    startDate:new Date(),
    endDate:"+1y",
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
</script>

<script type="text/javascript">
    $(".jd_datepicker").datepicker({

        endDate:"y" + 1
    });
</script>

<script type="text/javascript">

    $(".dob_datepicker").datepicker({

        endDate:"y"
    });
</script>


<script type="text/javascript">
    $("#datepicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
</script>
<script type="text/javascript">
    $("#monthpicker").datepicker({
        format: "mm-yyyy",
        viewMode: "months",
        minViewMode: "months"
    });
</script>
<script type="text/javascript">
    
    $(function() {
  
  var checkbox = $("#trigger");
  var hidden = $("#hidden_fields"); 
  
  hidden.show();
  
  checkbox.change(function() {
    
    if (checkbox.is(':checked')) {
      
      hidden.hide();
      
    } else {
      
      hidden.show();
     
    }
  });
});
</script>

<script type="text/javascript">
    
    $(function() {
  
  var checkbox = $("#trigger1");
  var hidden = $("#hidden_fields"); 
  
  hidden.show();
  
  checkbox.change(function() {
    
    if (checkbox.is(':checked')) {
      
      hidden.hide();
      
    } else {
      
      hidden.show();
     
    }
  });
});
</script>
<script type="text/javascript">
    
    $(function() {
  
  var checkbox = $("#trigger2");
  var hidden = $("#hidden_fields"); 
  
  
  checkbox.change(function() {
    
    if (checkbox.is(':checked')) {
      
      hidden.show();
      
    } else {
      
      hidden.hide();
     
    }
  });
});
</script>

<script>
    $(document).ready(function(){
        $('#addRow').click(function(){
            $("#extraPerson").slideDown();
         });
    })
</script>
<script>
    $('.payrollCalculation').keyup(function () {


        var total = $('#total').val();
        var  total_payroll = $('#total_payroll').val();
        var id = $('#emp_payroll_id').val();
        //Deduction
        var pf_fund = $('#pf_fund').val();
        var r_fund = $('#retirement_fund').val();
        var security_tax = $('#security_tax').val();
        var salary_tax = $('#salary_tax').val();
        //addition
        var ot_money_hr = $('#ot_money_hr').val();
        var ot_hr = $('#ot_hr').val();

        $('#ot').val(ot_money_hr * ot_hr);

        $('#total_payroll').val(total - pf_fund - r_fund - security_tax - salary_tax + (ot_money_hr * ot_hr));


        if (isNaN($('#total_payroll').val())){
            $('#update_payroll').prop('disabled', true);
            $("#empty_payroll").text('Invalid value entered').css('color', 'red');
        }else {
            $('#update_payroll').prop('disabled', false);
            $("#empty_payroll").text('').css('color', 'green');
        }

    });
</script>
<script>

    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
        // For select 2
        $(".select2").select2();
        $('.selectpicker').selectpicker();
        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }
        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();
        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });
        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });
        // For multiselect
        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });
        $('#public-methods').multiSelect();
        $('#select-all').click(function() {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });
        $('#refresh').on('click', function() {
            $('#public-methods').multiSelect('refresh');
            return false;
        });
        $('#add-option').on('click', function() {
            $('#public-methods').multiSelect('addOption', {
                value: 42,
                text: 'test 42',
                index: 0
            });
            return false;
        });
        $(".ajax").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    });
    </script>

    <script type="text/javascript">

       $('.select-exam-date').change(function () {
           var current_exam = $(this).val();
           $('#examDate').val(null);
           $.ajax({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               type:'post',
               url: '/sms/exam/schedule/get-selected-date',
               data:{current_exam:current_exam},
               success:function (resp) {
                   var startDate = resp.startdate;
                   var endDate = resp.enddate;
                   console.log(resp);
                   $('.mydatepicker1').datepicker('destroy');
                   $('.mydatepicker1').datepicker({
                       format: "mm/dd/yyyy",
                       autoclose: true,
                       startDate : new Date(startDate),
                       endDate : new Date(endDate)
                   });

               }, error:function () {
                   alert("Error");
               }
           });
       });
</script>
<script>
 $('.select-class-subject').change(function () {
     var current_class = $(this).val();
     $("#select_class_subject").find('option').remove().end().append('<option value=null>--Choose Subject--</option>').val('whatever');
     $.ajax({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         type:'post',
         url: 'schedule/get-selected-subject',
         data:{current_class:current_class},
         success:function (resp) {
            console.log(resp);
             var sub = $.parseJSON(resp);
             //alert(sub[0][0]);
             console.log(sub);

             /*var i;
             var j;
             for (i = 0; i < sub.length; i++) {
                 for (j = 0; j < sub[i].length; i++) {
                     alert(sub[i][j]);
                 }
                 }*/

             for (j = 0; j < sub[0].length; j++) {
                 var o = new Option(sub[0][j], sub[0][j]);
                 $(o).html(sub[0][j]);
                 $("#select_class_subject").append(o);
             }

             /*var o = new Option(resp, current_class);
             $(o).html(resp);
             $("#select_class_subject").append(o);*/
         }, error:function () {
            console.log(resp);
             alert("error");
             console.log(resp);
         }
     });
 });

</script>

<script>
 $('.select_subject').change(function () {
     var current_class = $(this).val();
     $(".select_class").find('option').remove().end().append('<option value=null>--Choose Subject--</option>').val('whatever');
     $.ajax({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         type:'post',
         url: 'schedule/get-selected-subject',
         data:{current_class:current_class},
         success:function (resp) {
            console.log(resp);
             var sub = $.parseJSON(resp);
             //alert(sub[0][0]);
             console.log(sub);

             /*var i;
             var j;
             for (i = 0; i < sub.length; i++) {
                 for (j = 0; j < sub[i].length; i++) {
                     alert(sub[i][j]);
                 }
                 }*/

             for (j = 0; j < sub[0].length; j++) {
                 var o = new Option(sub[0][j], sub[0][j]);
                 $(o).html(sub[0][j]);
                 $(".select_class").append(o);
             }

             /*var o = new Option(resp, current_class);
             $(o).html(resp);
             $("#select_class_subject").append(o);*/
         }, error:function () {
            console.log(resp);
             alert("error");
             console.log(resp);
         }
     });
 });

</script>

<!-- Mark Save  -->
<script>

        $("#formbuttonId").click(function () {
            var name = $("#firstName").val();
            var token = $(this).data("token");
            $.ajax(
                    {
                        url: "{!!URL::to('url/to/submit/')!!}/",
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            name: name,
                            _token: '{!! csrf_token() !!}'
                        },
                        success: function (response) {
                            alert('ok success');
                            {{--window.location.href = "{{ route('subjectmarksave') }}";--}}
                        toastr.success('Enquiry Closed Successfully.', 'Success Alert', {timeOut: 5000});


                        }
                    });


            // console.log("It failed");
        });
    </script>

{{-- <script>
    $('.invoiceCalculation').keyup(function () {
        
        var total = $('#total').val();
        var  total_invoice = $('#total_invoice').val();
        var id = $('#invoice_id').val();
       
        $('#total_invoice').val(total);

    });
</script> --}}


{{--administration settings validation--}}
<script>
    function institutionDetailValidation() {

        $('#update_institution_details').prop('disabled', false);
        $("#empty_field").text("").css("color","green");

        var name = $('#name').val();
        var address = $('#address').val();
        var email = $('#email').val();
        var number = $('#number').val();
        var state = $('#state').val();
        var city = $('#city').val();
        var pcode = $('#pcode').val();
        var country = $('#country').val();
        var pnumber = $('#pnumber').val();
        var cnumber = $('#cnumber').val();
        var icode = $('#icode').val();
        var fax = $('#fax').val();
        var facebook = $('#facebook').val();
        var twitter = $('#twitter').val();
        var youtube = $('#youtube').val();
        var instagram = $('#instagram').val();

        if (name.length === 0 || address.length === 0 || email.length === 0 || number.length === 0 || state.length === 0
            || city.length === 0 || pcode.length === 0 || country.length === 0 || pnumber.length === 0 || cnumber.length === 0
            || icode.length === 0 || fax.length === 0 || facebook.length === 0 || twitter.length === 0 || youtube.length === 0
            || instagram.length === 0)
        {
            $('#update_institution_details').prop('disabled', true);
            $("#empty_field").text("Please fill up all required fields").css("color","red");


        }else
        {
            $('#update_institution_details').prop('disabled', false);

        }
    }

   /* $().ready(function () {
        $('#institutionForm').validate({
            rules:{
                name:{
                    required:true,
                    minlength:2
                }
            },
            messages:{
                name:{
                    required:"Please enter your Name",
                    minlength:"Your Name must be atleast 2 characters long"
                }
            }
        });
    });*/

</script>
 <!-- Image validation -->
<script type="text/javascript">
function fileValidation(){
    var fileInput = document.getElementById('file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }
    else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}

</script>

<script src="{{asset('public/backend/dist/js/pages/jquery.PrintArea.js')}}" type="text/JavaScript"></script>

<script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>




<script>
 $('.class').change(function () {
     var current_class = $(this).val();
      $(".section").find('option').remove().end().append('<option value=null>--Choose Section--</option>').val('whatever');
     //alert(current_class);
     
     $.ajax({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         type:'post',
         url: '/sms/invoice/get-selected-section',
         data:{current_class:current_class},
         success:function (resp) {
            //alert(resp);
            console.log(resp);

            $.each(resp, function( index, value ) {
              //alert( value.sname );
              var o = new Option(value.sname, value.sname);
              //$(o).html(value.sname);
                 $(".section").append('<option value='+ value.id +'>'+ value.sname +'</option>');
             
            });

         }, error:function () {
            alert('No Section Available');

         }
     });
 });
</script>

<script>
 $('.section').change(function () {
        // alert("test");
     var current_class = $('.class').val();
     var current_section = $(this).val();
      $(".student_admission").find('option').remove().end().append('<option value=null>--Choose students--</option>').val('whatever');

     // alert(current_section);
     
     $.ajax({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         type:'post',
         url: '/SMS/invoice/get-selected-studentadmission',
         data:{current_class:current_class,
          current_section:current_section},

         success:function (resp) {
            // alert(resp);
            console.log(resp);

            $.each(resp, function( index, value ) {
              //alert( value.sname );
              var o = new Option(value.first_name, value.first_name);
              //$(o).html(value.sname);
            $(".student_admission").append("<option value='"+ value.id +"'>"+ value.first_name + value.middle_name + value.last_name + "</option>");

             
            });

         }, error:function () {
            alert('No Student Available');

         }
     });
 });
</script>


<script type="text/javascript">
  
$('#pass_mark').keyup(function(){
  if (parseFloat($(this).val()) > parseFloat($('#full_mark').val())){
    alert("pass Mark less than "+$('#full_mark').val());

   
($(this).val(''));



  }
});
</script>


{{--resume validation--}}
<script type="text/javascript">
    function fileResumeValidation(){
        var fileInput = document.getElementById('resume_file');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.pdf|\.docx)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Please upload file having extensions .pdf/.docx only.');
            fileInput.value = '';
            return false;
        }else{
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
    }
</script>

<script type="text/javascript">
 var itemID =0;


   function addElement(parentId, elementTag, elementId, html) { // Adds an element to the document
   var p = document.getElementById(parentId);
   var newElement = document.createElement(elementTag);
   newElement.setAttribute('class', 'controls');
   newElement.setAttribute('id',elementId);
   newElement.innerHTML = html;
   p.appendChild(newElement);
   }

   function removeElement(elementId) {
   // Removes an element from the document
   var element = document.getElementById(elementId);
   element.parentNode.removeChild(element);
   }

   function addItem_to(){

   itemID++; // increment fileId to get a unique ID for the new element

   var html =
   "<div class='input-group mb-3' >"+
   "   <div class='input-group-prepend controls'>"+
   "    <span class='input-group-text' id='basic-addon1'></span>"+
   "    </div>"+
   "    <input type='text' name='topic[]' id='topic' class='form-control' placeholder='Enter subject' aria-label='name' aria-describedby='basic-addon1'>"+
    "<a onclick=javascript:removeElement('ph_desc_div"+itemID+"');" +
   " return false;'><button class='btn btn-danger'> - </button></a> "
   "</div>";

   addElement('parent_ph', 'div', 'ph_desc_div'+itemID, html);


   }
   </script>

<script type="text/javascript">

       $('.feetype').change(function () {
           var current_feetype = $(this).val();
           // alert("test");
           $('#feetype').val(null);
           $.ajax({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               type:'post',
               url: '/SMS/invoice/get-selected-feetype',
               data:{current_feetype:current_annual},
               success:function (resp) {
                   var startDate = resp.startdate;
                   var endDate = resp.enddate;
                   console.log(resp);
                   $('.mydatepicker1').datepicker('destroy');
                   $('.mydatepicker1').datepicker({
                       format: "mm/dd/yyyy",
                       autoclose: true,
                       startDate : new Date(startDate),
                       endDate : new Date(endDate)
                   });

                    }, error:function () {
                   alert("Error");
               }
           });
       });
</script>

<script>
  $('.paid_amount').keyup(function () {
        
         var total = $('.total').val();
         var  paid_amount = $('.paid_amount').val();
         if(parseFloat(total) > parseFloat(paid_amount))
         {
          var partial = total - paid_amount;
         }
         $('.partial').val(partial);
});

</script>


{{--Change salary calculation--}}
<script>
    function changeSalaryTotal() {

        var salary = parseFloat($('#salary').val());
        var grade = parseFloat($('#grade').val());
        var allowance = parseFloat($('#allowance').val());
        var micro_finance = parseFloat($('#micro_finance').val());
        var local_allowance = parseFloat($('#local_allowance').val());
        var festival_allowance = parseFloat($('#festival_allowance').val());
        var home_rent = parseFloat($('#home_rent').val());
        var mahangi_allowance = parseFloat($('#mahangi_allowance').val());
        var responsibility_allowance = parseFloat($('#responsibility_allowance').val());
        var manager_allowance = parseFloat($('#manager_allowance').val());

        var total = salary + grade + allowance + micro_finance + local_allowance + festival_allowance + home_rent + mahangi_allowance + responsibility_allowance + manager_allowance;

        $("#salary_total").text('').css('color', 'green');
        $("#salary_total").text(total.toFixed(2)).css('color', 'green');




    }

</script>

{{--Change Salary validation--}}
<script>
    $(document).ready(function () {
       $('#salary_form').validate({
            rules: {
                salary:{
                    required:true,
                    number:true,
                    min:0
                },
                grade:{
                    required:true,
                    number:true,
                    min:0
                },
                allowance: {
                    required:true,
                    number:true,
                    min:0
                },
                micro_finance: {
                    required:true,
                    number:true,
                    min:0
                },
                local_allowance: {
                    required:true,
                    number:true,
                    min:0
                },
                festival_allowance: {
                    required:true,
                    number:true,
                    min:0
                },
                home_rent: {
                    required:true,
                    number:true,
                    min:0
                },
                mahangi_allowance: {
                    required:true,
                    number:true,
                    min:0
                },
                responsibility_allowance: {
                    required:true,
                    number:true
                },
                manager_allowance: {
                    required:true,
                    number:true,
                    min:0
                }
            }
       });
    });
</script>



</body>
</html>

