<?php
use \App\Http\Controllers\Controller;
$departments = Controller::departments();
$designations = Controller::designations();
?>
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Balance</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{route('home')}}"><i class="icon-speedometer"></i><span class="hide-menu">Dashboard</span></a>
                </li>

                   <!--  settings -->
                    <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-cog"></i><span class="hide-menu">Settings</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('settings') }}">Institution Details</a></li>
                       
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="     false"><i class="ti-user"></i><span class="hide-menu">Student</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/studentcategories')}}"> Student Discount   Category</a></li>
                        <li><a href="{{ route('studentadmissions.create')}}"> Student Admission</a></li>
                        <li><a href="{{ route('studentadmissions.index')}}"> Student List</a></li>
                        <li><a href="{{ url('/guardians') }}"> Parent list</a></li>
                        {{--<li><a href="{{ route('idcard_index')}}"> ID Card</a></li> --}}
                        
                    </ul>

                </li>
                {{--Academic--}}
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-university" ></i><span class="hide-menu">Academic</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('classes')}}">Class</a></li>
                        <li><a href="{{ route('sections')}}">Section</a></li>
                        <li><a href="{{ route('academic') }}">Class Teacher Allocation</a></li>
                        <li><a href="{{ route('subject') }}">Subjects</a></li>
                        <!-- <li><a href="{{ route('lesson') }}">Lesson Planning</a></li> -->
                    </ul>
                </li>
                 {{--Exam--}} 
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-pencil-square-o" ></i><span class="hide-menu">Exam</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('create') }}">Exam Create</a></li>
                        <li><a href="{{ route('exam') }}">Exam Schedule</a></li>
                        <li><a href="{{ route('viewexam') }}">View Exam Schedule </a></li>
                        <li><a href="{{ route('individual')}}">Individual Schedule </a></li>
                    </ul>
                </li>
                 {{--Mark--}} 
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i  class="fa fa-graduation-cap" ></i><span class="hide-menu">Mark</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('classmarkview') }}">Mark </a></li>

                    </ul>
                </li>
                {{--Employee--}} 
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-user"></i><span class="hide-menu">Employees</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('view.employee') }}">View Employee</a></li>
                        <li><a href="{{ route('add.employee') }}">Add Employee</a></li>

                        {{--Department--}}
                        <li><a href="{{ route('view.department') }}">Add Department</a></li>

                        {{--Designation--}}
                        <li><a href="{{ route('view.designation') }}">Add Designation</a></li>

                    </ul>
                </li>

                {{--Payroll--}}
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu"> Payroll</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('view.department.payroll') }}">Payroll Record</a></li>
                        <li><a href="{{ route('view.individual.payroll') }}">Individual Payroll</a></li>
                        <li><a href="{{ route('view.annual.payroll') }}">Annual Payroll</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-building-o" aria-hidden="true"></i><span class="hide-menu"> Departments list</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @foreach($departments as $department)
                            <li><a href="{{ route('view.selected.department', ['id' => $department->id]) }}">{{ $department->name }}</a></li>
                        @endforeach
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-id-card-o" aria-hidden="true"></i><span class="hide-menu"> Designation list</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @foreach($designations as $designation)
                            <li><a href="{{ route('view.selected.designation', ['id' => $designation->id]) }}">{{ $designation->name }}</a></li>
                        @endforeach
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-star"></i><span class="hide-menu">Event</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <!-- <li><a href="{{url('/eventtypes')}}">Event Types</a></li> -->
                                <li><a href="{{ route('add.event') }}">Add Events</a></li>
                            </ul>
                           
                        </li>
                        {{--  <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-star"></i><span class="hide-menu">Finance</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('feecategory.index')}}">Fee Category</a></li>
                                <li><a href="{{route('feesubcategory.index')}}">Fee Sub Category</a></li>
                                <li><a href="{{route('feesubcategoryfine.index')}}">Fee Sub Category Fine</a></li>
                                <li><a href="{{route('feewaiver.index')}}">Fee Waiver</a></li>
                                <li><a href="{{route('feeallocate.index')}}">Fee Allocation</a></li>
                                <li><a href="{{route('feecollection.index')}}">Fee Collation</a></li>
                            </ul>
                        </li> --}}

                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class=" ti-briefcase"></i><span class="hide-menu">Account</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('feetype.index')}}">Fee Type</a></li>
                                {{-- <li><a href="{{route('classfee.index')}}">Class Fee</a></li> --}}
                                <li><a href="{{route('invoice.index')}}">Invoice</a></li>
                              
                                
                            </ul>
                        </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>