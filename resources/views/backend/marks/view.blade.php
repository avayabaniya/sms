@extends('backend.includes.app')
@section('content')
 
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Mark Details View</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active">Mark Details view</li>
                            </ol>
                            <a href="{{ route('classmarkview')}}">
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i>View All Mark Details</button></a>
                            
                        </div>
                    </div>
                </div>
                           

         <div class="col-lg-12 col-xlg-3 col-md-5">
                        <div class="card">
                                 

                            <div class="card-body">
                                <center class="m-t-30"> 
                                     <img  src="{{asset('public/backend/images/student/'.$student->image)}}" class="img-circle" width="150" height="150">
                                    <h4 class="card-title m-t-10">
                                            {{$student->first_name.' '.$student->middle_name.' '.$student->last_name}}
                                      </h4>
                                </center>
                            
                           <div class="row">
                            <div class="col-4"> 
                               <label class="label-conreol"> Reg No :{{$student->register_number}}</label>
                            </div>
                             <div class="col-4"> 
                               <label class="label-conreol">Roll No :{{$student->rollno}}</label>
                            </div>
                             <div class="col-4"> 
                               <label class="label-conreol">Class :{{$student->room->cname}}</label>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-4"> 
                               <label class="label-conreol"> Section :{{$student->section->sname}}</label>
                            </div>
                             <div class="col-4"> 
                               <label class="label-conreol">Date Of Birth :{{$student->dob}}</label>
                            </div>
                             <div class="col-4"> 
                               <label class="label-conreol">Gender :{{$student->gender}}</label>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-4"> 
                               <label class="label-conreol"> Blood Group :{{$student->blood_group}}</label>
                            </div>
                             <div class="col-4"> 
                               <label class="label-conreol">Phone Number :{{$student->phone_no}}</label>
                            </div>
                             <div class="col-4"> 
                               <label class="label-conreol">Parent Name :{{$student->pname}}</label>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-4"> 
                               <label class="label-conreol"> Parent Phone :{{$student->pphone}}</label>
                            </div>
                             <div class="col-4"> 
                               <label class="label-conreol">Temporary Municipality :{{$student->temporary_municipality}}</label>
                            </div>
                             <div class="col-4"> 
                               <label class="label-conreol"> Temporary Street :{{$student->temporary_street}}</label>
                            </div>
                            </div>
                            <hr>  
                        <div class="row-mark">        
                            <div class="col-md-5 align-self-center">
                                <h4 class="text-themecolor">Mark Informations</h4>
                            </div>
                          
                           <div class="col-md-5 align-self-center">
                                <h6 class="text-themecolor">test</h6>
                            </div>
                            <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr><th>SN</th>
                                            <th>Subject</th>
                                            <th>Full Mark</th>
                                            <th>Pass Mark</th>
                                            <th>Practicle Mark</th>                                          
                                            <th>obtain Mark</th>
                                            <th>Total Mark</th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                      @foreach($marks as $mark )
                                        <tr>
                                            <td>{{$loop->index +1}}</td>
                                            <td>{{$mark->id}}</td>
                                            <td>{{$mark->id}}</td>

                                            <td>{{$mark->id}}</td>
                                            <td>{{$mark->id}}</td>
                                            <td>{{$mark->id}}</td>
                                            <td>{{$mark->id}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                </div>

                         </div> 

                        </div>


                    </div>

 @endsection
