@extends('backend.includes.app')

@section('content')
 
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Mark </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active">Subject Mark Add</li>
                            </ol>
                            
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                        <form class="form-horizontal" method="post" action="{{ route('subjectmarkadd')}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                         <div class="row">
                            <div class="col-2">  
                                <label class="control-label" for="academic_year">Academic Year:</label>
                                <select  name="academic_year" class="form-control custom-select">
                                <option value="">--Choose Year--</option>
                                 @foreach($arr['student_admissions'] as $student)                                 
                                 <option value="{{$student->academic_year}}"  @if(old('student') == $student->id) selected @endif  >{{($student->academic_year)}}</option>
                                 @endforeach
                                  </select>
                                 <a href="" style="color: red;">{{ $errors -> first('academic_year') }}</a>  
                             </div>
                             <div class="col-2">  
                                <label class="control-label" for="exam_name">Exam:</label>
                                <select  name="exam_name" class="form-control custom-select">
                                 <option value="">--Choose Exam--</option>
                                 @foreach($arr['creates'] as $create)
                                 <option value="{{$create->id}}"  @if(old('create') == $create->id) selected @endif  >{{($create->exam_name)}}</option>
                                 @endforeach
                                  </select>
                                 <a href="" style="color: red;">{{ $errors -> first('exam_name') }}</a>  
                             </div>
                              <div class="col-2 form-group">
                                    <label class="control-label" for="cname">Class</label>
                                    <select  name="cname" id="class" class=" class form-control custom-select select_subject" >
                                        <option value="null">--Choose Class--</option>
                                        @foreach($arr['rooms'] as $room)
                                            <option value="{{$room->id}}" @if($room->id == old('cname') ) selected @endif >{{ucfirst($room->cname)}}</option>
                                        @endforeach
                                    </select>
                                    <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>
                                </div>
                             <div class="col-2 form-group">  
                                <label class="control-label" for="section">Section:</label>
                                <select  name="sname" id="section" class="section form-control custom-select">
                                 
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('sname') }}</a>  
                            </div>
                             <div class="col-2 form-group">
                                    <label class="control-label" for="subject_id">Subject</label>
                                    <select  name="subject_id" class="form-control custom-select select_class">
                                    </select>
                                    <a href="" style="color: red;">{{ $errors -> first('subject_id') }}</a>
                            </div>
                              <div class="col-md-2"> <br>
                           <button type="submit"  class="btn btn-info"><i class="fa fa-plus-circle"></i> Mark</button>
                            </div>
                            
                            </div>
                        </form>
                            </div>
                        </div>
                    </div>
                </div>




 @endsection
