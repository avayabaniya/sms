@extends('backend.includes.app')

@section('content')
 
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Mark </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active">Mark</li>
                            </ol>
                            
                            
                        </div>
                    </div>
                </div>
    @if($search == 0)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        <form class="form-material m-t-10" method="post" enctype="multipart/form-data" action="{{ route('classmarkview')}}">
                            @csrf

                            <div class="row">
                                <div class="col-2">  
                             <a href="{{ route('markadd')}}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add Mark</a>
                                 </div>
                                 <div class="col-3"> 
                                <label class="control-label" for="academic_year">Academic Year:</label>
                                <select  name="academic_year" class="form-control custom-select">
                                 <option value="">--Choose Year--</option>
                                 @foreach($arr['student_admissions'] as $student)                                 
                                 <option value="{{$student->academic_year}}"  @if(old('student') == $student->id) selected @endif  >{{($student->academic_year)}}</option>
                                 @endforeach
                             </select>
                                  <a href="" style="color: red;">{{ $errors -> first('academic_year') }}</a>  
                                
                                 </div>

                                 <div class="col-3">  
                                <label class="control-label" for="cname">Class:</label>
                                <select  name="cname" class="form-control custom-select class" id="class">
                                 <option value="">--Choose Class--</option>
                                 @foreach($arr['rooms'] as $room)
                                 <option value="{{$room->id}}"  @if(old('room') == $room->id) selected @endif  >{{ucfirst(trans($room->cname))}}</option>
                                 @endforeach
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>  
                                 </div>

                                 <div class="col-3">  
                                <label class="control-label" for="section">Section:</label>
                                <select  name="sname" class="form-control custom-select section" id="section">
                                 
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('sname') }}</a>  
                                 </div>

                                  <div class="col-sm-1">
                                    <br>
                                    <br>
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                            </div>
                            </form>
                                </div>
                            </div>
                        </div>
                     </div>
    @endif

    @if($search == 1)
         <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        <form class="form-material m-t-10" method="post" enctype="multipart/form-data" action="{{ route('classmarkview')}}">
                            @csrf

                            <div class="row">
                                <div class="col-2">  
                             <a href="{{ route('markadd')}}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add Mark</a>
                                 </div>
                                 <div class="col-3"> 
                                <label class="control-label" for="academic_year">Academic Year:</label>
                                <select  name="academic_year" class="form-control custom-select">
                                 <option value="">--Choose Year--</option>
                                 @foreach($arr['student_admissions'] as $student)                                 
                                 <option value="{{$student->academic_year}}"  @if($chooseyear == $student->academic_year) selected @endif  >{{($student->academic_year)}}</option>
                                 @endforeach
                             </select>
                                  <a href="" style="color: red;">{{ $errors -> first('academic_year') }}</a>  
                                
                                 </div>

                                 <div class="col-3">  
                                <label class="control-label" for="cname">Class:</label>
                                <select  name="cname" class="form-control custom-select class" id="class">
                                 <option value="">--Choose Class--</option>
                                 @foreach($arr['rooms'] as $room)
                                 <option value="{{$room->id}}"  @if($chooseclass == $room->id) selected @endif  >{{ucfirst(trans($room->cname))}}</option>
                                 @endforeach
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>  
                                 </div>

                                 <div class="col-3">  
                                <label class="control-label" for="section">Section:</label>
                                <select  name="sname" class="form-control custom-select section" id="section">
                                 
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('sname') }}</a>  
                                 </div>

                                  <div class="col-sm-1">
                                    <br>
                                    <br>
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                            </div>
                            </form>
                                </div>
                            </div>
                        </div>
                     </div>

                     <div class="table-responsive m-t-40">
                          <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Photo</th>
                                            <th>Name</th>
                                            <th>Roll</th>
                                            <th>Email</th>                                          
                                            <th>Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                 
                                         @foreach($student_admissions as $room)
                                    
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td><img src="{{asset('public/backend/images/student/'.$room->image)}}"  width="80"></td>
                                            <td>{{$room->first_name.' '.$room->middle_name.' '.$room->last_name}}</td>
                                            <td>{{$room->rollno}}</td>
                                            <td>{{$room->email}}</td>
                                            <td>
                                        
                               <a href="{{ route('markview',['id'=> $room->id])}}" class="text-info p-r-10" title="View" data-toggle="tooltip" ><i class="ti-eye"></i></a>
                                                  
                                            </td>
                                        </tr>
                                 @endforeach


                                        
                                        </tbody>
                                    </table>
                                </div>
                          
    @endif

 @endsection
