@extends('backend.includes.app')

@section('content')
 
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Mark </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active">Subject Mark Add</li>
                            </ol>
                            
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                              Mark Details
                              <div class="col-2">  
                             <a href="{{ route('markadd')}}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add New Mark</a>
                                 </div>
                            <br><br>
                          <div class="row">
                             <div class="col-3">  
                                <label class="control-label" for="exam_name">Exam:{{ $rooms[0]->exam_id}}</label>
                             </div>
                             <div class="col-3">  
                                <label class="control-label" for="cname">Class:{{ $rooms[0]->cname}}</label>
                             </div>
                             <div class="col-3"> 
                                <label class="control-label" for="sname">Section:{{$rooms[0]->section_id}}</label>
                             </div>
                             <div class="col-3"> 
                                <label class="control-label" for="subject_id">Subject:{{ $rooms[0]->subject_id}}</label>
                             </div>
                            </div>
                      
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Photo</th>
                                            <th>Name</th>
                                            <th>Roll No.</th>
                                            <th>Exam Mark</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                             @foreach($rooms as $room)
                                        <tr> 
                                           <form class="form-horizontal" method="post" action="{{ route('subjectmarksave')}}" enctype="multipart/form-data" >
                                     <input type="hidden" name="_token" value="{{csrf_token()}}">
                                         @csrf
                                         <input type="hidden" id="firstName" name="register_number" value="{{$room->register_number}}">
                                         <input type="hidden" id="firstName" name="section_id" value="{{$room->section_id}}">
                                         <input type="hidden" id="firstName" name="room_id" value="{{$room->id}}">
                                         <input type="hidden" id="firstName" name="exam_id" value="{{$room->exam_id}}">
                                         <input type="hidden"id="firstName" name="subject_id" value="{{$room->subject_id}}">
                                            <td>{{$loop->index+1}}</td>
                                             <td><img src="{{asset('public/backend/images/student/'.$room->image)}}" width="80px;" height="80px;" /></td>
                                            <td>
                                             {{ $room->first_name}} </td>

                                            <td>{{ $room->rollno}}</td>
                                            <td>
                                            <div class="row">
                                                <input name="mark"  min="0" max="100"  id="firstName">
                                                <button id="formbuttonId">Save</button>
                                         </div>
                                       </td>
                                    </form>
                                            
                                        </tr>
                                @endforeach 


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




 @endsection
