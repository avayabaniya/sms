@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Academic</li>
                                <li class="breadcrumb-item active">Lesson Planning edit</li>
                            </ol>
                         </div>
                    </div>
                </div>
              <div class="row">
            <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Lesson Planning Edit</p>
                        <form class="form-horizontal" method="post" action="{{route('lessonupdate', $les->id)}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                        <div class="form-group">
                            <label class="control-label" for="sname">Batch </label>
                              <select  name="batch" class="form-control custom-select">
                                 @foreach($arr['academics'] as $aca)
                                <option  @if($les->batch == $aca->batch) selected @endif >{{($aca->batch)}}</option>
                                @endforeach
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('batch') }}</a>             
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="scode">Subject Code</label>
                            <select  name="scode" class="form-control custom-select">
                               @foreach($arr['subjects'] as $sub)
                                <option  @if($les->scode == $sub->scode) selected @endif >{{($sub->scode)}}</option>
                                @endforeach
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('scode') }}</a>        
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="cname">Class</label>
                            <select  name="cname" class="form-control custom-select">
                               @foreach($arr['rooms'] as $roo)
                                <option  @if($les->cname == $roo->cname) selected @endif >{{($roo->cname)}}</option>
                                @endforeach
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>        
                        </div>
                       </div>
                      </div>
                    </div>
                    
            <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body"> 
                        <div class="form-group">
                            <label class="control-label" for="sname">Subject Name</label>
                            <select  name="sname" class="form-control custom-select">
                               @foreach($arr['subjects'] as $sub)
                                <option @if($les->sname == $sub->sname) selected @endif >{{($sub->sname)}}</option>
                                @endforeach
                                
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('sname') }}</a>        
                        </div>
                       
                           <?php $details = json_decode($les->topic); ?>
                               
                          <div class="form-group ">
                               <div class="control-label" id="parent_ph">
                                   <label for="exampleInputuname">Topic</label>

                                         @for($i=0;$i<sizeof($details[0]);$i++)

                                   <div class="input-group mb-3" >

                                       <div class="input-group-prepend controls">
                                           <span class="input-group-text" id="basic-addon1"></span>
                                       </div>
                                               <input type="text" value="{{ $details[0][$i] }}"
                                                name="topic[]" id="topic" class="form-control" placeholder="Enter topic" aria-label="name" aria-describedby="basic-addon1">
                       <a onclick=javascript:removeElement('ph_desc_div"+itemID+"');" +
   " return false;'><button class='btn btn-danger'> - </button></a>
                                         
                                   </div>
                                    @endfor
                               </div>
                           <button class="btn btn-success" type="button" style="margin-left:10px;"  onclick="addItem_to();"> <big> + </big> </button>
                           <a href="" style="color: red;">{{ $errors -> first('topic') }}</a>
                           </div>
                                <div class="form-actions">
                                 <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                              </form>
                            </div>
                          </div>
                    </div>
                        </div>
                              
                            </div>
                        </div>
                    </div>
                
             
    </div>
    
      
<script>

   var itemID = 0;

   function addElement(parentId, elementTag, elementId, html) { // Adds an element to the document
   var p = document.getElementById(parentId);
   var newElement = document.createElement(elementTag);
   newElement.setAttribute('class', 'controls');
   newElement.setAttribute('id',elementId);
   newElement.innerHTML = html;
   p.appendChild(newElement);
   }

   function removeElement(elementId) {
   // Removes an element from the document
   var element = document.getElementById(elementId);
   element.parentNode.removeChild(element);
   }

   function addItem_to(){

   itemID++; // increment fileId to get a unique ID for the new element

   var html =
   "<div class='input-group mb-3' >"+
   "   <div class='input-group-prepend controls'>"+
   "    <span class='input-group-text' id='basic-addon1'></span>"+
   "    </div>"+
   "    <input type='text' name='topic[]' id='topic' class='form-control' placeholder='Enter topic' aria-label='name' aria-describedby='basic-addon1'>"+
   "</div> <a onclick=javascript:removeElement('ph_desc_div"+itemID+"');" +
   " return false;'><button class='btn btn-danger'> - </button></a> ";

   addElement('parent_ph', 'div', 'ph_desc_div'+itemID, html);


   }
   </script>





@endsection