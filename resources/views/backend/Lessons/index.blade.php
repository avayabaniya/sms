@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Academic</li>
                                <li class="breadcrumb-item active">Lesson Planning</li>

                            </ol>
                           
                        </div>
                    </div>
                </div>
                <div class="row">
            <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Lesson Planning</p>
                        <form class="form-horizontal" method="post" action="{{route('lessonsave')}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                        <div class="form-group">

                            <label class="control-label" for="sname">Batch </label>
                              <select  name="batch" class="form-control custom-select">
                               <option value="null">--Choose Batch--</option>
                                 @foreach($arr['academics'] as $aca)
                                <option  >{{($aca->batch)}}</option>
                                @endforeach
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('batch') }}</a>             
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="sname">Subject Name</label>
                            <select  name="sname" class="form-control custom-select">
                               <option value="null">--Choose Subject--</option>
                               @foreach($arr['subjects'] as $sub)
                                <option >{{($sub->sname)}}</option>
                                @endforeach
                                
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('sname') }}</a>        
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="scode">Subject Code</label>
                            <select  name="scode" class="form-control custom-select">
                               <option value="null">--Choose Subject Code--</option>
                               @foreach($arr['subjects'] as $sub)
                                <option >{{($sub->scode)}}</option>
                                @endforeach
                                
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('scode') }}</a>        
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="cname">Class</label>
                            <select  name="cname" class="form-control custom-select">
                               <option value="null">--Choose Class--</option>
                               @foreach($arr['rooms'] as $roo)
                                <option >{{($roo->cname)}}</option>
                                @endforeach
                                
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>        
                        </div>
                        <div class="form-group ">
                               <div class="control-label" id="parent_ph">
                                   <label for="exampleInputuname">Topic</label>
                                   <div class="input-group mb-3" >
                                       <div class="input-group-prepend controls">
                                           <span class="input-group-text" id="basic-addon1"></span>
                                       </div>
                                       <input type="text" name="topic[]" id="topic" class="form-control" placeholder="Enter topic" aria-label="name" aria-describedby="basic-addon1">
                                   </div>
                               </div>
                           <button class="btn btn-success" type="button" style="margin-left:10px;"  onclick="addItem_to();"> <big> + </big> </button>
                           <a href="" style="color: red;">{{ $errors -> first('topic') }}</a>
                           </div>
                                <div class="form-actions">
                                 <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                              </form>
                            </div>
                            
                        </div>


                    </div>
                <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
            <div class="card-body">
                <div class="row">
                       <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <th>SN</th>
                                                <th>Batch</th>
                                                <th>Subject</th>
                                                <th>Class</th>
                                                <th>Topic</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach($arr['lessons'] as $les)
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                <td> {{ ($les->batch)}}</td>
                                                <td> {{ ($les->sname)}}</td>
                                                <td>{{ ($les->cname)}}</td>
                                                <td> 
                                                    <?php $details = json_decode($les->topic); ?>
                                                    <ul>
                                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                                        <li>{{ $details[0][$i] }}</li>
                                                        @endfor
                                                    </ul>
                                                </td>
                                          <td>

               <a href="{{ route('lessonedit',$les->id)}}" class="ti-pencil ti-pencil-info"></a>&nbsp;

               <a href="#" class="ti-eye ti-eye-info" data-toggle="modal" data-target=".bs-example-modal-lg{{$les->id}}" class="model_img img-responsive" ></a> &nbsp;

              

              <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
              <form method="post" id="del" action="{{ route('lessondestroy',$les->id)}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @method('DELETE')
            </form>
               </td>
                  </tr> 
                <div class="modal fade bs-example-modal-lg{{$les->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                                    myLargeModalLabel" aria-hidden="true" style="display: none;">
                              <div class="modal-dialog modal-lg">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Lesson </h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    @method('PUT')
                                                                                                       
                                                     <div class="row">
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Batch</strong>
                                                                <br>
                                                                <p class="text-muted">{{($les->batch)}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Subject</strong>
                                                                <br>
                                                                <p class="text-muted">{{($les->sname)}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Subject Code</strong>
                                                                <br>
                                                                <p class="text-muted">{!!($les->scode) !!}</p>
                                                            </div>
                                                             </div>

                                                             <div class="row">
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Class</strong>
                                                                <br>
                                                                <p class="text-muted">{!!($les->cname) !!}</p>
                                                            </div>
                                                             <div class="col-md-3 col-xs-6 b-r"> <strong>Topic</strong>
                                                                <br>
                                                              
                                                               <?php $details = json_decode($les->topic); ?>
                                                       
                                                              @for($i=0;$i< sizeof($details[0]);$i++)
                                                               <li>
                                                               {{ $details[0][$i] }}
                                                               </li>
                                                                @endfor
                                                       
                                                        
                                                            </div>
                                                            </div>
                                                            </div>
                                                       


                                            
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                             </form>
                                          
                                                </div>
                                                </div>
                                                </div>
                                                </div>

                        @endforeach 
                              
                                        </tbody>
                                    </table>

                                    
                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             
    </div>
    
      
<script>

   var itemID = 0;

   function addElement(parentId, elementTag, elementId, html) { // Adds an element to the document
   var p = document.getElementById(parentId);
   var newElement = document.createElement(elementTag);
   newElement.setAttribute('class', 'controls');
   newElement.setAttribute('id',elementId);
   newElement.innerHTML = html;
   p.appendChild(newElement);
   }

   function removeElement(elementId) {
   // Removes an element from the document
   var element = document.getElementById(elementId);
   element.parentNode.removeChild(element);
   }

   function addItem_to(){

   itemID++; // increment fileId to get a unique ID for the new element

   var html =
   "<div class='input-group mb-3' >"+
   "   <div class='input-group-prepend controls'>"+
   "    <span class='input-group-text' id='basic-addon1'></span>"+
   "    </div>"+
   "    <input type='text' name='topic[]' id='topic' class='form-control' placeholder='Enter topic' aria-label='name' aria-describedby='basic-addon1'>"+
   "</div> <a onclick=javascript:removeElement('ph_desc_div"+itemID+"');" +
   " return false;'><button class='btn btn-danger'> - </button></a> ";

   addElement('parent_ph', 'div', 'ph_desc_div'+itemID, html);


   }
   </script>





@endsection