@extends('backend.includes.app')
@section('content')

    
             <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Guardian</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">View Guardians</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><a href="{{ route('studentadmissions.index')}}" class="btn btn-info"><i class="fa fa-plus-circle"></i>View Student List</a></button>
                            
                        </div>
            </div>
        </div>

        
                
                 <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Guardian Information</h4>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Name</th>
                                                <th>Child's Name</th>
                                                <th>Email</th>
                                                <th>Mobile Number</th>
                                                <th>Address</th>
                                                <th>Manage</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            

                                           @foreach($admission as $g)
                                            <tr>
                                                <td>{{$loop->index +1}}</td>
                                                <td>{{$g->pname}}</td>
                                                <td>{{ $g->first_name.' '.$g->middle_name.' '.$g->last_name }}</td>
                                                <td>{{$g->pemail}}</td>
                                                <td>{{$g->pmobile}}</td>
                                                <td>{{$g->parent_municipality}}</td>
                                                <td>
                                                    
                                                    <a href="{{route('guardians.edit',  $g->id)}}" class="text-success p-r-10" data-toggle="tooltip" title="Edit"><i class="ti-marker-alt"></i></a>

                                                    <a href="#" class="ti-eye" data-toggle="modal" data-target=".bs-example-modal-lg{{$g->id}}" class="model_img img-responsive" ></a>

                                                    </td>
                                               
                                            </tr>
                                                <div class="modal fade bs-example-modal-lg{{$g->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myLargeModalLabel">Profile Details: {{ $g->pname}}</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        
                                                        <div class="row">
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->pname}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Education</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->education}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Occupation</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->occupation}}</p>
                                                            </div>

                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Income</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->income}}</p>
                                                            </div>

                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Relation</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->relation}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Country</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->pcountry}}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            
                                                            <div class="col-md-3 col-xs-6"> <strong>Municipality</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->parent_municipality}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Street</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->parent_street}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Ward Number</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->parent_ward_number}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Academic Year</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->academic_year}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Phone Number</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->pphone}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile Number</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->pmobile}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Email</strong>
                                                                <br>
                                                                <p class="text-muted">{{$g->pemail}}</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="" class="btn btn-info waves-effect">Edit</a>
                                                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        
                                            @endforeach

                                           
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
        </div>
    </div>
                                  
                                
 @endsection