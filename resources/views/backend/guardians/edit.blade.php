@extends('backend.includes.app')
@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row page-titles">
                    <div class="col-md-10 align-self-center">
                        <h4 class="text-themecolor">Guardian List</h4>
                    </div>
                    <div class="col-md-10 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item">Guardian</li>
                                <li class="breadcrumb-item active">List</li>
                            </ol>
                            
                        </div>
                    </div>
                </div><!-- /.row -->
   <!-- /.container-fluid -->
<div class="content">

<form enctype="multipart/form-data" method="POST" action="{{route('updateguardian',['id' => $studentadmission->id])}}">
<input type="hidden" name="_token" value="{{csrf_token()}}">
@method('PUT')
                        <p><b>GUARDIAN'S DETAILS</b></p>  
                    <hr>
                        <div class="row">
                            
                            <div class="form-group col-sm-4">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="pname" value="{{$studentadmission->pname}}">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="relation">Relation</label>
                                <input type="text" class="form-control" name="relation" value="{{$studentadmission->relation}}">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="occupation">Occupation</label>
                                <input type="text" class="form-control" name="occupation" value="{{$studentadmission->occupation}}">
                              </div>

                               <div class="form-group col-sm-4">
                                <label for="education">Education</label>
                                <input type="text" class="form-control" name="education" value="{{$studentadmission->education}}">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="income">Income</label>
                                <input type="text" class="form-control" name="income" value="{{$studentadmission->inome}}">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" name="address" value="{{$studentadmission->address}}">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="pcity">City</label>
                                <input type="text" class="form-control" name="pcity">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="pcountry">Country</label>
                                <input type="text" class="form-control" name="pcountry">
                              </div>
                

                              <div class="form-group col-sm-4">
                                <label for="mobile">Mobile</label>
                                <input type="text" class="form-control" name="mobile">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" name="phone">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="email">email</label>
                                <input type="text" class="form-control" name="email">
                              </div>

                                  <input type="hidden" class="form-control" name="academic_year" value="{{$studentadmission->academic_year}}"> 

                                  
                                  <input type="hidden" class="form-control" name="register_number" value="{{$studentadmission->register_number}}"> 
                                  <input type="hidden" class="form-control" name="joining_date" value="{{$studentadmission->joining_date}}" > 

                                  <input type="hidden" class="form-control" name="course" value="{{$studentadmission->course}}"> 
                        
                        
                                  <input type="hidden" class="form-control" name="batch"  value="{{$studentadmission->batch}}"> 
                              

                                  <input type="hidden" class="form-control" name="rollno"  value="{{$studentadmission->rollno}}"> 

                                  <input type="hidden" class="form-control" name="first_name"  value="{{$studentadmission->first_name}}"> 

                              
                                  <input type="hidden" class="form-control" name="middle_name"  value="{{$studentadmission->middle_name}}"> 
                              

                           
                                  <input type="hidden" class="form-control" name="last_name" value="{{$studentadmission->last_name}}"> 
                              
                                  <input type="hidden" class="form-control" name="dob" value="{{$studentadmission->dob}}"> 
                              
                                  <input type="hidden" class="form-control" name="gender" value="{{$studentadmission->gender}}">

                                  <input type="hidden" class="form-control" name="blood_group" value="{{$studentadmission->blood_group}}">

                                  <input type="hidden" class="form-control" name="birth_place" value="{{$studentadmission->birth_place}}">

                                  <input type="hidden" class="form-control" name="nationality" value="{{$studentadmission->nationality}}">

                                  <input type="hidden" class="form-control" name="mother_tongue" value="{{$studentadmission->mother_tongue}}">
                                  <input type="hidden" class="form-control" name="category" value="{{$studentadmission->category}}">

                                  <input type="hidden" class="form-control" name="religion" value="{{$studentadmission->religion}}">
                                  <input type="hidden" class="form-control" name="present_address" value="{{$studentadmission->present_address}}">

                                  <input type="hidden" class="form-control" name="permanent_address" value="{{$studentadmission->permanent_address}}">

                                  <div class="col-md-12" >
                                    <div class="text-center">
                                      <input type="submit" value="update" class="btn btn-info btn-xs">
                                    </div>
                                  </div>

                            </div>
                            </div>
                        </div>


                        </div>
                     </div>
                </div>
  </form>                           

                                
 @endsection