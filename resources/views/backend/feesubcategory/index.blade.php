@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h4 class="text-themecolor">Fee Sub Category</h4>
   </div>
   <div class="col-md-7 align-self-center text-right">
      <div class="d-flex justify-content-end align-items-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item ">Fee Sub Category</li>
           <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
         </ol>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-lg-6 col-xlg-6 col-md-6">
      <div class="card">
         <div class="card-body">
            <p>Fee Sub Category</p>
            <form class="form-horizontal" method="post" action="{{ route('feesubcategory.store')}}" enctype="multipart/form-data" >
               @csrf
               <div class="form-group">
                  <div class="row">
                     <label name="feecategory" class="col-md-4" for="name">Fee Category</label>
                     <div class="col-sm-6">
                                            <select class="form-control" name="feecategory" id="feecategory" required>
                                                <option value=>--Select feecategory--</option>
                                                @foreach($feesubcategory as $fsc)
                                                    <option value="{{ $fsc->feecategory }}" >{{ $fsc->feecategory }}</option>
                                                @endforeach
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('fsc') }}</a>
                                        </div>

                  </div>
               </div>
                 <div class="form-group">
                  <div class="row">
                     <label name="feesubcategoryname" class="col-md-4" for="name">Fee Sub Category Name</label>
                     <div class="col-md-12">
                        <input type="text" class="form-control" name="feesubcategoryname" id="feesubcategoryname" placeholder="Fee Sub Category Name"required >
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <label name="amount" class="col-md-4" for="name">Amount</label>
                     <div class="col-md-12">
                        <input type="text" class="form-control" name="amount" id="amount" placeholder="Amount"required>
                     </div>
                  </div>
               </div>
              <div class="form-group">
                  <div class="row">
                     <label name="feetype" class="col-md-4" for="name">Fee Type</label>
                    <div class="col-sm-6">
                                            <select class="form-control" name="feetype" id="feetype" required>
                                                <option value=>Select Type</option>
                                                <option value="lab" @if(old('feetype') == 'lab')selected @endif>Lab</option>
                                                <option value="sport" @if(old('feetype') == 'sport')selected @endif>Sport</option>
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('feetype') }}</a>

                                        </div>
                  </div>
               </div>
               <div class="form-actions">
                  <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
               </div>
            </form>
         </div>
      </div>
   </div>
    <div class="col-lg-12 col-md-8">
      <div class="card-center">
         <div class="card-body">
            Show Fee Sub Category    
            <div class="table-responsive m-t-40">
               <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                     <tr>
                        <th>SN</th>
                        <th>Fee Category</th>
                        <th>Fee Sub Category Name</th>
                        <th>Amount</th>
                        <th>FeeType</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($feeDetail as $fsc)
                     <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $fsc->feecategory }}</td>
                        <td>{{ $fsc->feesubcategoryname }}</td>
                        <td>{{ $fsc->amount }}</td>
                        <td>{{ $fsc->feetype }}</td>
                        <td>
                           <a href="{{ url('feesubcategory/update',$fsc->id)}}" class="ti-pencil ti-pencil-info" data-toggle="modal" data-target=".bs-example-modal-lg1{{$fsc->id}}" class="model_img img-responsive"></a>&nbsp;

                           <a href="#" class="ti-eye ti-eye-info" data-toggle="modal" data-target=".bs-example-modal-lg{{$fsc->id}}" class="model_img img-responsive" ></a> &nbsp;

                           <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
                           <form method="get" id="del" action="{{ url('/feesubcategory/destroy',$fsc->id)}}">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              @method('DELETE')
                           </form>

                        </td>
                     </tr>
                     <div class="modal fade bs-example-modal-lg1{{$fsc->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                        myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myLargeModalLabel">Fee Sub Category </h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="{{ url('feesubcategory/update',$fsc->id)}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="row">
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Fee Category</strong>
                                          <br>
                                          <input type="text" name="feecategory" value=" {{($fsc->feecategory)}}">
                                       </div>

                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Fee Sub Category Name</strong>
                                          <br>
                                          <input type="text" name="feesubcategoryname" value=" {{($fsc->feesubcategoryname)}}">
                                       </div>

                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Amount</strong>
                                          <br>
                                          <input type="text" name="amount" value="{{($fsc->amount)}}">
                                       </div>
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Fee Type</strong>
                                          <br>
                                          <input type="text" name="feetype" value="{!!($fsc->feetype) !!}">
                                       </div>
                                    </div>
                                    <div class="modal-footer">
                                       <div class="form-actions">
                                          <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>
                                       </div>
                                       <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal fade bs-example-modal-lg{{$fsc->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                        myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myLargeModalLabel">Fee Sub Category</h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    @method('PUT')
                                    <div class="row">
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Fee Category</strong>
                                          <br>
                                          <p class="text-muted">{{($fsc->feecategory)}}</p>
                                       </div>

                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Fee Sub Category name</strong>
                                          <br>
                                          <p class="text-muted">{{($fsc->feesubcategoryname)}}</p>
                                       </div>

                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Amount</strong>
                                          <br>
                                          <p class="text-muted">{{($fsc->amount)}}</p>
                                       </div>
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Fee Type</strong>
                                          <br>
                                          <p class="text-muted">{!!($fsc->feetype) !!}</p>
                                       </div>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                 </form>
                                 @endforeach
                              </div>
                           </div>
                        </div>
                     </div>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection