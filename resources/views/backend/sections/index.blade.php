@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item ">Academic</li>
                                <li class="breadcrumb-item active">Section</li>

                            </ol>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
            <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Add Section</p>
                        <form class="form-horizontal" method="post" action="{{route('sectionssave')}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                        <div class="form-group">

                            <label class="control-label" for="sname">Section<span style="color: red;">*</span> </label>
                            <input type="text" id="sname" value="{{old('sname')}}" name="sname" placeholder="Enter section" class="form-control" >
                            <a href="" style="color: red;">{{ $errors -> first('sname') }}</a>             
                        </div>
                             <div class="form-group">
                                <label name="room_id" for="room_id">Class<span style="color: red;">*</span></label>
                                <select  name="room_id" class="form-control custom-select">
                                    <option value="" >Choose Class</option>
                                     @foreach($room as $room)
                                     <option value="{{$room->id}}" @if($room->id == old('room_id') ) selected @endif >{{$room->cname}}</option>
                                    @endforeach
                             </select>
                                <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>
                         </div>
                              <div class="form-actions">
                                 <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                              </form>
                            </div>
                            
                        </div>


                    </div>
                <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
            <div class="card-body">
                           Show Section list
                <div class="row">
                       
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Section </th>
                                                <th>Class in Alphabet</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           @foreach($sections as $sec)
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                
                                                <td>{{($sec->sname)}}</td>
                                                <td> {{($sec->room['cname'])}}</td>
                                          <td>
 
                         <a href="{{ route('sectionsupdate',$sec->id)}}" class="ti-pencil ti-pencil-info" data-toggle="modal" data-target=".bs-example-modal-lg1{{$sec->id}}" class="model_img img-responsive"></a>&nbsp;
                     

              <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
              <form method="post" id="del" class="delete" action="{{ route('sectionsdestroy',$sec->id)}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @method('DELETE')
            </form></td>
                        </tr>  


                     <div class="modal fade bs-example-modal-lg1{{$sec->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                                    myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Class Edit </h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" method="post" action="{{ route('sectionsupdate',$sec->id)}}" enctype="multipart/form-data">
                                                 <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                
                                                    <div class="row">
                                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Section</strong>
                                                                <br>
                                                                <input type="text" name="sname" value=" {{ucfirst(trans($sec->sname))}}">
                                                                
                                                            </div>
                                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Class in Alphabet</strong>
                                                                <br>
                                                                <input type="text" name="description" value="{{($sec->room['cname'])}}">
                                                            </div>
                                                        </div>
                                            <div class="modal-footer">
                                                 <div class="form-actions">
                                                <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>
                                        
                                                </div>
                                                
                                                <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                             </form>
                                          
                                                </div>
                                                </div>
                                                </div>
                                                </div>

                                          @endforeach         
                                        </tbody>
                                    </table>

                                    
                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             
    </div>
    
      






@endsection