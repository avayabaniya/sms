@extends('backend.includes.app')

@section('content')

    <?php
    use \App\Http\Controllers\Controller;
    $studentCount = Controller::studentCount();
    $employeeCount = Controller::employeeCount();
    $classCount = Controller::classCount();
    $departmentCount = Controller::departmentCount();
    $designationCount = Controller::designationCount();
    $eventCount = Controller::eventCount();
    ?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">School Dashboard</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Number of Students</h5>
                <div class="text-right"> <span class="text-muted">Student Number</span>
                    <h2>{{$studentCount}}</h2>
                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Number of Employee</h5>
                <div class="text-right"> <span class="text-muted">Employee Number</span>
                    <h2>{{$employeeCount}}</h2>
                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Number of Classes</h5>
                <div class="text-right"> <span class="text-muted">Class Number</span>
                    <h2>{{ $classCount }}</h2>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Number of Departments</h5>
                <div class="text-right"> <span class="text-muted">Department Number</span>
                    <h2>{{ $departmentCount }}</h2>
                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Number of Designation</h5>
                <div class="text-right"> <span class="text-muted">Designation Number</span>
                    <h2>{{ $designationCount }}</h2>
                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Number of Event</h5>
                <div class="text-right"> <span class="text-muted">Event Number</span>
                    <h2>{{$eventCount}}</h2>
                </div>

            </div>
        </div>
    </div>
</div>


@endsection
