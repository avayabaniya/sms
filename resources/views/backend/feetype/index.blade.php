@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Fee Type</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active">Class</li>

                            </ol>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-4 col-xlg-4 col-md-12">
                        <div class="card">
                            <div class="card-body">
                               <p>Fee Type</p>
                        <form class="form-horizontal" method="post" action="{{route('feetype.store')}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                            
                              <div class="form-group">
                              <label class="control-label" name="room_id" for="room_id">Class<span style="color: red;">*</span></label>
                              <select class="form-control" name="room_id" id="room_id" required>
                                    <option value=>Select Class</option>
                                          @foreach($room as $ro)
                                                <option value="{{$ro->id}}" >{{ ucfirst(trans($ro->cname)) }}</option>
                                          @endforeach
                              </select>
                                                 <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>
                                              </div>
                                          

                        <div class="form-group">
                            <label class="control-label" for="feetype">Fee Type<span style="color: red;">*</span></label>
                            <input type="text" id="feetype" name="feetype" class="form-control" value="{{old('feetype')}}" >
                            <a href="" style="color: red;">{{ $errors -> first('feetype') }}</a>             
                        </div>
                            {{-- <label><input type="checkbox" class="control-label" name="onetime_fee" value="1">One Time Fee</label> --}}
                            <div class="form-group">
                              <label class="control-label" for="type"></label>
                              <select class="form-control" name="type" id="type" required>
                                    <option value=>Select category</option>
                                                <option value="Onetime" >Onetime</option>
                                                <option value="Monthly" >Monthly</option>
                              </select>
                            </div>
                          <br>

                        <div class="form-group">
                            <label class="control-label" for="amount">Amount<span style="color: red;">*</span></label>
                            <input type="text" id="amount" name="amount" class="form-control" value="{{old('amount')}}" >
                            <a href="" style="color: red;">{{ $errors -> first('amount') }}</a>             
                        </div>
                              <div class="form-actions">
                                 <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                              </form>
                            </div>
                            
</div>

                    </div>
                <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
            <div class="card-body">
                           Show Fee Type
                            <div class="row">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Class</th>
                                                <th>Fee Type</th>
                                                <th>Amount(Annual)</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($feetype as $ft)
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                <td>{{$ft->room['cname']}}</td>
                                                <td>{{ucfirst($ft->feetype)}}</td>
                                                <td>{{$ft->amount}}</td>
                                                
                                                <td>

                        <a href="{{ route('feetype.update',$ft->id)}}" class="ti-pencil ti-pencil-info" data-toggle="modal" data-target=".bs-example-modal-lg1{{$ft->id}}" class="model_img img-responsive"></a>&nbsp;

                        
              <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
              <form method="post" id="del" class="delete" action="{{ route('feetype.destroy',$ft->id)}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @method('DELETE')
              </form>
          </td>
                        </tr>  
                            <div class="modal fade bs-example-modal-lg1{{$ft->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                                    mySmallModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="mySmallModalLabel">Fee Type</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" method="post" action="{{ route('feetype.update',$ft->id)}}" enctype="multipart/form-data">
                                                 <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                

                                                    <div class="row">
                                                            <div style="text-align: center;">Class</div>
                                                                <br>
                                                                <select class="form-control" name="room_id" id="room_id" required>
                                    <option value=>Select Class</option>
                                          @foreach($room as $ro)
                                                <option value="{{$ro->id}}" @if ($ro->id == $ft->room_id)
                                      selected
                                      @endif>{{ ucfirst(trans($ro->cname)) }}</option>
                                          @endforeach
                              </select>
                                                                
                                                            </div>
                                                            <br>

                                                             <div class="row">
                                                            <div style="text-align: center;">Fee Type </div>
                                                                <br>
                                                                <input type="text" name="feetype" class="form-control col-md-12" value=" {{$ft->feetype}}">
                                                                
                                                            </div>
                                                            <br>

                                                             <div class="row">
                                                            <div style="text-align: center;">Amount</div>
                                                                <br>
                                                                <input type="text" name="amount" class="form-control col-md-12" value=" {{$ft->amount}}">
                                                                
                                                            </div>
                                                            <br>
                                            
                                                {{-- <button type="submit" class="btn btn-info" style="text-align: center;"> <i class="fa fa-check"></i> Update</button> --}}

                                                <div class="col-md-12" >
                                    <div class="text-center">
                                      <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                  </div>
                                        
                                                </div>
                                            </div>
                                             </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>

                            
                                            @endforeach
                                                

                                        </tbody>
                                    </table>
                   </div>
               </div>
           </div>
     

                    </div>
                </div>
             
    </div>
@endsection