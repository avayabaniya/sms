@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h4 class="text-themecolor">Class Fee</h4>
   </div>
   <div class="col-md-7 align-self-center text-right">
      <div class="d-flex justify-content-end align-items-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item ">Class Fee</li>
         </ol>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-lg-4 col-xlg-4 col-md-4">
      <div class="card">
         <div class="card-body">
            <p>Class Fee</p>
            <form class="form-horizontal" method="post" action="{{ route('classfee.store') }}" enctype="multipart/form-data" >
               @csrf
               <div class="form-group">
                  <div class="row">
                     <label name="room" class="col-md-4" for="room">Class<span style="color: red;">*</span></label>
                   {{--   <div class="col-md-12">
                        <input type="text" class="form-control" name="room" id="room" placeholder="Class" required>
                     </div> --}}
                     <select class="form-control" name="room" id="room" required>
                        <div class="col-md-12">
                                                <option value=>Select Class</option>
                                                @foreach($room as $ro)
                                                 <option value="{{$ro->cname}}" >{{ ucfirst(trans($ro->cname)) }}</option>
                                                @endforeach
                                            </select>
                                        <a href="" style="color: red;">{{ $errors -> first('room') }}</a>
                                     </div>
               </div>
               <div class="form-group">
                  @foreach($feetype as $ft)
                  <div class="row">
                     <label name="$ft->feetype" class="col-md-4" for="$ft->feetype">{{$ft->feetype}}<span style="color: red;">*</span></label>
                     <div class="col-md-12">
                        <input type="text" class="form-control" name="$ft->feetype" id="$ft->feetype" placeholder="Fee Amount" required>
                     </div>
                  </div>
                  @endforeach
               </div>
               {{-- <div class="form-group">
                  <div class="row">
                     <label class="col-md-4">Description</label>
                     <div class="col-md-12">
                        <textarea id="summernote" class="form-control" name="description" id="description" class="form-control" maxlength="200" placeholder="Description" ></textarea>
                     </div>
                  </div>
               </div> --}}
               <div class="form-actions">
                  <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
               </div>
            </form>
         </div>
      </div>
   </div>
    <div class="col-lg-8 col-md-8">
      <div class="card-center">
         <div class="card-body">
            Show ClassFee    
            <div class="table-responsive m-t-40">
               <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                     <tr>
                        <th>SN</th>
                        <th>Class</th>
                        <th>Amount</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($classfee as $cf)
                     <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ ucfirst(trans($cf->room)) }}</td>
                        <td>{{ $cf->amount }}</td>
                        <td>
                           <a href="{{ route('classfee.update',$cf->id)}}" class="ti-pencil ti-pencil-info" data-toggle="modal" data-target=".bs-example-modal-lg1{{$cf->id}}" class="model_img img-responsive"></a>&nbsp;

                           {{-- <a href="#" class="ti-eye ti-eye-info" data-toggle="modal" data-target=".bs-example-modal-lg{{$cf->id}}" class="model_img img-responsive" ></a> &nbsp; --}}

                           <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash" ></a>
                            <form method="get" id="del" class="delete" action="{{ route('classfee.destroy',$cf->id)}}">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              @method('DELETE')
                           </form>


                        </td>
                     </tr>
                     <div class="modal fade bs-example-modal-lg1{{$cf->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                        myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myLargeModalLabel">Class Fee</h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="{{ route('classfee.update',$cf->id)}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="row">
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>class</strong>
                                          <br>
                                          <input type="text" name="room" value=" {{($cf->room)}}">
                                       </div>
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Fees</strong>
                                          <br>
                                          <input type="text" name="amount" value="{{($cf->amount)}}">
                                       </div>
                                       
                                    </div>
                                    <div class="modal-footer">
                                       <div class="form-actions">
                                          <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>
                                       </div>
                                       <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal fade bs-example-modal-lg{{$cf->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                        myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myLargeModalLabel">Fee Type</h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    @method('PUT')
                                    <div class="row">
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Class</strong>
                                          <br>
                                          <p class="text-muted">{{($cf->room)}}</p>
                                       </div>
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Amount</strong>
                                          <br>
                                          <p class="text-muted">{{($cf->amount)}}</p>
                                       </div>
                                       
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                 </form>
                                 @endforeach
                              </div>
                           </div>
                        </div>
                     </div>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection