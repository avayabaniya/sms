@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h4 class="text-themecolor">Fee Category</h4>
   </div>
   <div class="col-md-7 align-self-center text-right">
      <div class="d-flex justify-content-end align-items-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item ">Fee Category</li>
         </ol>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-lg-4 col-xlg-4 col-md-4">
      <div class="card">
         <div class="card-body">
            <p>Fee Category</p>
            <form class="form-horizontal" method="post" action="{{ url('/feecategory/store')}}" enctype="multipart/form-data" >
               @csrf
               <div class="form-group">
                  <div class="row">
                     <label name="feecategory" class="col-md-4" for="name">Fee Category</label>
                     <div class="col-md-12">
                        <input type="text" class="form-control" name="feecategory" id="feecategory" placeholder="Fee Category" required>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <label name="receiptno" class="col-md-4" for="name">Receipt No</label>
                     <div class="col-md-12">
                        <input type="text" class="form-control" name="receiptno" id="receiptno" placeholder="Receipt No" required>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <label class="col-md-4">Description</label>
                     <div class="col-md-12">
                        <textarea id="summernote" class="form-control" name="description" id="description" class="form-control" maxlength="200" placeholder="Description" required></textarea>
                     </div>
                  </div>
               </div>
               <div class="form-actions">
                  <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-lg-8 col-md-8">
      <div class="card-center">
         <div class="card-body">
            Show FeeCategory    
            <div class="table-responsive m-t-40">
               <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                     <tr>
                        <th>SN</th>
                        <th>Fee Category</th>
                        <th>Receipt No</th>
                        <th>Description</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($feecategory as $fc)
                     <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $fc->feecategory }}</td>
                        <td>{{ $fc->receiptno }}</td>
                        <td>{{ $fc->description }}</td>
                        <td>
                           <a href="{{ url('feecategory/update',$fc->id)}}" class="ti-pencil ti-pencil-info" data-toggle="modal" data-target=".bs-example-modal-lg1{{$fc->id}}" class="model_img img-responsive"></a>&nbsp;

                           <a href="#" class="ti-eye ti-eye-info" data-toggle="modal" data-target=".bs-example-modal-lg{{$fc->id}}" class="model_img img-responsive" ></a> &nbsp;

                           <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
                            <form method="get" id="del"  action="{{ url('/feecategory/destroy',$fc->id)}}">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              @method('DELETE')
                           </form>


                        </td>
                     </tr>
                     <div class="modal fade bs-example-modal-lg1{{$fc->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                        myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myLargeModalLabel">Fee Category </h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="{{ url('feecategory/update',$fc->id)}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="row">
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Fee Category</strong>
                                          <br>
                                          <input type="text" name="feecategory" value=" {{($fc->feecategory)}}">
                                       </div>
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Receipt No</strong>
                                          <br>
                                          <input type="text" name="receiptno" value="{{($fc->receiptno)}}">
                                       </div>
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Description</strong>
                                          <br>
                                          <input type="text" name="description" value="{!!($fc->description) !!}">
                                       </div>
                                    </div>
                                    <div class="modal-footer">
                                       <div class="form-actions">
                                          <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>
                                       </div>
                                       <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal fade bs-example-modal-lg{{$fc->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                        myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myLargeModalLabel">Fee Category</h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    @method('PUT')
                                    <div class="row">
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Fee Category</strong>
                                          <br>
                                          <p class="text-muted">{{($fc->feecategory)}}</p>
                                       </div>
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong> Receipt No</strong>
                                          <br>
                                          <p class="text-muted">{{($fc->receiptno)}}</p>
                                       </div>
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Description</strong>
                                          <br>
                                          <p class="text-muted">{!!($fc->description) !!}</p>
                                       </div>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                 </form>
                                 @endforeach
                              </div>
                           </div>
                        </div>
                     </div>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection