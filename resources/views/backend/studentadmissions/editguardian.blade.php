@extends('backend.includes.app')
@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit parent's Detail</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item">Parent</li>
                                <li class="breadcrumb-item ">List</li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                            
                        </div>
                    </div>
                </div><!-- /.row -->
   <!-- /.container-fluid -->


<form enctype="multipart/form-data" method="post" action="{{route('studentadmissions.update',$studentadmission->id)}}">
<input type="hidden" name="_token" value="{{csrf_token()}}">
@method('PUT')
                        <p><b>PARENT'S DETAILS</b></p>
                    <hr>
                        <div class="row">
                            <input type="hidden" name="check" value="1">
                            <div class="form-group col-sm-4">
                                <label for="pname">Name</label>
                                <input type="text" class="form-control" name="pname" value="{{$studentadmission->pname}}">
                                  <a href="" style="color:red;">{{ $errors -> first('pname') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="relation">Relation</label>
                                <input type="text" class="form-control" name="relation" value="{{$studentadmission->relation}}">
                                  <a href="" style="color: red;">{{ $errors -> first('relation') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="occupation">Occupation</label>
                                <input type="text" class="form-control" name="occupation" value="{{$studentadmission->occupation}}">
                                  <a href="" style="color: red;">{{ $errors -> first('occupation') }}</a>

                              </div>

                               <div class="form-group col-sm-4">
                                <label for="education">Education</label>
                                <input type="text" class="form-control" name="education" value="{{$studentadmission->education}}">
                                  <a href="" style="color: red;">{{ $errors -> first('education') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="income">Income</label>
                                <input type="text" class="form-control" name="income" value="{{$studentadmission->income}}">
                                  <a href="" style="color: red;">{{ $errors -> first('income') }}</a>

                              </div>
                                
                              <div class="row" >

                              <div class="form-group col-sm-4">
                                <label for="pcountry">Country</label>       
                                <select class="form-control" name="pcountry" id="pcountry ">
                                        @foreach($countries as $country)
                                          <option value="{{ $country->name }}"
                                            @if ($country->name == $studentadmission->pcountry)
                                      selected
                                      @endif >{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('pcountry') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="pprovince">Province</label>       
                                <select class="form-control" name="pprovince" id="pprovince ">
                                        @foreach($provinces as $province)
                                          <option value="{{ $province->name }}"
                                            @if ($province->name == $studentadmission->pprovince)
                                      selected
                                      @endif >{{ $province->name }}</option>
                                                @endforeach
                                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('pprovince') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="parent_municipality">Municipality</label>
                                <input type="text" class="form-control" name="parent_municipality" value="{{$studentadmission->parent_municipality}}">
                                  <a href="" style="color: red;">{{ $errors -> first('parent_municipality') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="parent_street">Street</label>
                                <input type="text" class="form-control" name="parent_street" value="{{$studentadmission->parent_street}}">
                                  <a href="" style="color: red;">{{ $errors -> first('parent_street') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="parent_ward_number">Ward Number </label>
                                <input type="text" class="form-control" name="parent_ward_number" value="{{$studentadmission->parent_ward_number}}">
                                  <a href="" style="color: red;">{{ $errors -> first('parent_ward_number') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="pmobile">Mobile</label>
                                <input type="text" class="form-control" name="pmobile" value="{{$studentadmission->pmobile}}">
                                  <a href="" style="color: red;">{{ $errors -> first('pmobile') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="pphone">Phone</label>
                                <input type="text" class="form-control" name="pphone" value="{{$studentadmission->pphone}}">
                                  <a href="" style="color: red;">{{ $errors -> first('pphone') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="pemail">email</label>
                                <input type="text" class="form-control" name="pemail" value="{{$studentadmission->pemail}}">
                                  <a href="" style="color: red;">{{ $errors -> first('pemail') }}</a>
                              </div>

                                  <input type="hidden" class="form-control" name="academic_year" value="{{$studentadmission->academic_year}}"> 

                                  <input type="hidden" class="form-control" name="admission_number" value="{{$studentadmission->admission_number}}"> 
                                  
                                  <input type="hidden" class="form-control" name="register_number" value="{{$studentadmission->register_number}}"> 
                                  <input type="hidden" class="form-control" name="joining_date" value="{{$studentadmission->joining_date}}" > 

                                  <input type="hidden" class="form-control" name="room_id" value="{{$studentadmission->room_id}}"> 
                        
                        
                                  <input type="hidden" class="form-control" name="section_id"  value="{{$studentadmission->section_id}}"> 
                              

                                  <input type="hidden" class="form-control" name="rollno"  value="{{$studentadmission->rollno}}"> 

                                  <input type="hidden" class="form-control" name="first_name"  value="{{$studentadmission->first_name}}"> 

                              
                                  <input type="hidden" class="form-control" name="middle_name"  value="{{$studentadmission->middle_name}}"> 
    
                                  <input type="hidden" class="form-control" name="last_name" value="{{$studentadmission->last_name}}"> 
                              
                                  <input type="hidden" class="form-control" name="dob" value="{{$studentadmission->dob}}"> 
                              
                                  <input type="hidden" class="form-control" name="gender" value="{{$studentadmission->gender}}">

                                  <input type="hidden" class="form-control" name="blood_group" value="{{$studentadmission->blood_group}}">

                                  <input type="hidden" class="form-control" name="birth_place" value="{{$studentadmission->birth_place}}">

                                  <input type="hidden" class="form-control" name="nationality" value="{{$studentadmission->nationality}}">

                                  <input type="hidden" class="form-control" name="mother_tongue" value="{{$studentadmission->mother_tongue}}">
                                  <input type="hidden" class="form-control" name="student_id" value="{{$studentadmission->student_id}}">

                                  <input type="hidden" class="form-control" name="religion" value="{{$studentadmission->religion}}">
                                  
                                  <input type="hidden" class="form-control" name="province" value="{{$studentadmission->province}}">
                                  <input type="hidden" class="form-control" name="country" value="{{$studentadmission->country}}">

                                  <input type="hidden" class="form-control" name="permanent_municipality" value="{{$studentadmission->permanent_municipality}}">
                                  
                                  <input type="hidden" class="form-control" name="permanent_street" value="{{$studentadmission->permanent_street}}">
                                  
                                  <input type="hidden" class="form-control" name="permanent_ward_number" value="{{$studentadmission->permanent_ward_number}}">

                                  <input type="hidden" class="form-control" name="temporary_ward_number" value="{{$studentadmission->temporary_ward_number}}">

                                  <input type="hidden" class="form-control" name="temporary_street" value="{{$studentadmission->temporary_street}}">

                                  <input type="hidden" class="form-control" name="temporary_municipality" value="{{$studentadmission->temporary_municipality}}">

                                  <input type="hidden" class="form-control" name="temporary_country" value="{{$studentadmission->temporary_country}}">

                                  <input type="hidden" class="form-control" name="temporary_province" value="{{$studentadmission->temporary_province}}">


                                  <input type="hidden" class="form-control" name="phone_no" value="{{$studentadmission->phone_no}}">
                                  <input type="hidden" class="form-control" name="mobile_no" value="{{$studentadmission->mobile_no}}">
                                  <input type="hidden" class="form-control" name="email" value="{{$studentadmission->email}}">

                                  <div class="col-md-12" >
                                    <div class="text-center">
                                      <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>
                                    </div>
                                  </div>

                            </div>
                            </div>
                        </div>


                        </div>
                     </div>
                </div>
  </form>                           

                                
 @endsection