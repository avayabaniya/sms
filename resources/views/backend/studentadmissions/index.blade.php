@extends('backend.includes.app')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Academic</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item ">Student </li>
                    <li class="breadcrumb-item active">Admission </li>
                </ol>
                 <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><a href="{{ route('studentadmissions.index')}}" class="btn btn-info"><i class="fa fa-plus-circle"></i>View Student List</a></button>
            </div>
        </div>
    </div><!-- /.col -->

<div class="content">

<form method="post" action="{{ route('studentadmissions.store')}}" enctype="multipart/form-data" class="from-horizontal">

            <input type="hidden" name="_token" value="{{csrf_token()}}">                
                  <p><b>OFFICIAL DETAILS:-</b> <b style="float: right;">admission number: @foreach($setting as $se) <input type="disabled" class="col-md-5 form-control" name="admission_number" value="{{$se->icode.'-'.rand(1,9999999) }}" readonly="readonly" style=" opacity:1"> @endforeach </b></p>
            
                    <hr>
                        <div class="row">
                            
                            <div class="form-group col-sm-4">
                                <label for="academic_year">Academic Year<span style="color: red;"> * </span></label>
                                <input type="text" id="datepicker" name="academic_year" class="form-control " placeholder="Enter Start Year " value="{{ old('academic_year') }}">
                                <a href="" style="color: red;">{{ $errors -> first('academic_year') }}</a>
                              </div>

                                                           
                                <div class="form-group col-sm-4">
                                  <label for="register_number">Register Number<span style="color: red;"> * </span></label>
                                  <input class="form-control" name="register_number" id="register_number" type="text" maxlength="45" placeholder="Enter Register Number" value="{{ old('register_number') }}"/> 
                                  <a href="" style="color: red;">{{ $errors -> first('register_number') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="joining_date">Joining Date<span style="color: red;"> * </span></label>
                                  <input class="form-control mydatepicker" type="text" name="joining_date" placeholder="Enter Joining Date" value="{{ old('joining_date') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('joining_date') }}</a>
                                </div>
                            </div>
                        </div>

                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label name="room_id" for="room_id">Class <span style="color: red;">*</span></label>
                            <select class="form-control class" name="room_id" id="class">
                                <option value=>Select Class</option>
                                @foreach($room as $room)
                                     <option value="{{$room->id}}" @if(old('room_id')==$room->id)selected @endif>{{$room->cname}}</option>
                                @endforeach
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>
                        </div>

                        <div class="form-group col-sm-4">
                            <label name="section_id" for="section_id">Section <span style="color: red;"></span></label>
                            <select class="form-control section" name="section_id" id="section">
                               
                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('section_id') }}</a> 
                        </div>
                                                  
                              <!-- <div class="form-group col-sm-4">
                                  <label for="rollno">Roll No.</label>
                                  <input class="form-control" name="rollno" id="rollno" type="text" maxlength="60" />
                                  <a href="" style="color: red;">{{ $errors -> first('rollno') }}</a>
                                                      
                              </div> -->
                        </div>
                        <p><b>PERSONAL DETAILS:-</b></p>
                    <hr>
                        <div class="row">
                            
                            <div class="form-group col-sm-4">
                                <label for="first_name">First Name<span style="color: red;"> * </span></label>
                                <input type="text" class="form-control" name="first_name" placeholder="Enter First Name" value="{{ old('first_name') }}">
                                <a href="" style="color: red;">{{ $errors -> first('first_name') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="middle_name">Middle Name</label>
                                <input type="text" class="form-control" name="middle_name" maxlength="20" placeholder="Enter Middle name" value="{{ old('middle_name') }}">
                                <a href="" style="color: red;">{{ $errors -> first('middle_name') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="last_name">Last Name<span style="color: red;"> * </span></label>
                                <input type="text" class="form-control" name="last_name" placeholder="Enter Last Name" value="{{ old('last_name') }}">
                                <a href="" style="color: red;">{{ $errors -> first('last_name') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" name="image" id="file" onchange="return fileValidation()" placeholder="Select" value="{{ old('image') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('image') }}</a>

                              </div>

                                <div class="form-group col-sm-4">
                                  <label for="dob">Date of Birth<span style="color: red;"> * </span></label>
                                  <input class="form-control dob_datepicker" type="text" name="dob"placeholder="Enter dob" value="{{ old('dob') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('dob') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="gender">Gender<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="gender" id="gender">
                                                <option value=>Select Gender</option>
                                                <option value="Male" @if(old('gender') == 'Male')selected @endif>Male</option>
                                                <option value="Female" @if(old('gender') == 'Female')selected @endif>Female</option>
                                                <option value="Other" @if(old('gender') == 'Other')selected @endif>Other</option>
                                            </select>
                                <a href="" style="color: red;">{{ $errors -> first('gender') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="blood_group">Blood Group<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="blood_group" id="blood_group">
                                    <option value=>Select Blood Group</option>
                                  <option value="A+" @if(old('blood_group') == 'A+')selected @endif> A+ </option>
                                  <option value="A-"  @if(old('blood_group') == 'A-')selected @endif> A- </option>
                                  <option value="AB+" @if(old('blood_group') == 'AB+')selected @endif> AB+ </option>
                                  <option value="AB-" @if(old('blood_group') == 'AB-')selected @endif> AB- </option>
                                  <option value="B+" @if(old('blood_group') == 'B+')selected @endif> B+ </option>
                                  <option value="B-" @if(old('blood_group') == 'B-')selected @endif> B- </option>
                                  <option value="O+" @if(old('blood_group') == 'O+')selected @endif> O+ </option>
                                  <option value="O-" @if(old('blood_group') == 'O-')selected @endif> O- </option>
                                </select>
                                <a href="" style="color: red;">{{ $errors -> first('blood_group') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="birth_place">Birth Place<span style="color: red;"> * </span></label>
                                  <input class="form-control" type="text" name="birth_place" placeholder="Enter Birth Place" value="{{ old('birth_place') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('birth_place') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="nationality">Nationality<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="nationality" id="nationality">
                                                <option value=> Select Nationality </option>
                                                @foreach($nationalities as $nationality)
                                                    <option value="{{ $nationality->name }}" @if(old('nationality')==$nationality->name)selected @endif > {{ $nationality->name }}</option>
                                                @endforeach
                                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('nationality') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="mother_tongue">Mother Tongue<span style="color: red;"> * </span></label>
                                  <input class="form-control" type="text" name="mother_tongue" placeholder="Enter Mother Tongue" value="{{ old('mother_tongue') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('mother_tongue') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="student_id">Discount Category</label>
                                  <select class="form-control" name="student_id">
                                <option value=>Select Category</option>
                                @foreach($student as $student)
                                     <option value="{{$student->id}}" @if(old('student_id')==$student->id)selected @endif>{{$student->category}}</option>
                                @endforeach
                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('student_id') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="religion">Religion<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="religion">
                                    <option value=>Select Religion</option>
                                  <option value="Hinduism" @if(old('religion') == 'Hinduism')selected @endif> Hinduism</option>
                                  <option value="Buddhism"  @if(old('religion') == 'Buddhism')selected @endif> Buddhism</option>
                                  <option value="Islam" @if(old('religion') == 'Islam')selected @endif> Islam </option>
                                  <option value="Kiratism" @if(old('religion') == 'Kiratism')selected @endif> Kiratism </option>
                                  <option value="Christianity " @if(old('religion') == 'Christianity ')selected @endif> Christianity </option>
                                  <option value="Prakriti" @if(old('religion') == 'Prakriti')selected @endif> Prakriti </option>
                                  <option value="Bon" @if(old('religion') == 'Bon')selected @endif>Bon </option>
                                  <option value="Jainism" @if(old('religion') == 'Jainism')selected @endif> Jainism</option>
                                  <option value="Other" @if(old('religion') == 'Other')selected @endif> Other</option>
                                </select>
                                  <a href="" style="color: red;">{{ $errors -> first('religion') }}</a>
                                </div>
                            </div>

                        <p><b>CONTACT DETAILS:-</b></p>
                    <hr>
                    <b style="padding-left: 30px;">Permanent Address:-</b>
                    <hr>
                        <div class="row">
                          <div class="form-group col-sm-4">
                                  <label for="country">Country<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="country" id="country">
                                                <option value=> Select Country </option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->name }}" @if(old('country')==$country->name)selected @endif>{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                  <a href="" style="color: red;"> {{ $errors -> first('country') }}</a> 
                          </div>
                            
                            <div class="form-group col-sm-4">
                                  <label for="province">Province<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="province" id="province">
                                      <option value=> Select province </option>
                                        @foreach($provinces as $province)
                                        <option value="{{ $province->name }}" @if(old('province')==$province->name)selected @endif>{{ $province->name }}</option>
                                        @endforeach
                                    </select>
                                  <a href="" style="color: red;">{{ $errors -> first('province') }}</a> 
                              </div>


                              <div class="form-group col-sm-4">
                                  <label for="permanent_municipality">VDC/Municipality<span style="color: red;"> * </span></label>
                                  <input type="text" class="form-control" name="permanent_municipality" maxlength="80" placeholder="Enter VDC/Municipality" value="{{ old('permanent_municipality') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('permanent_municipality') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="permanent_street">Street</label>
                                  <input type="text" class="form-control" name="permanent_street" maxlength="80" placeholder="Enter Street" value="{{ old('permanent_street') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('permanent_street') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="permanent_ward_number">Ward Number</label>
                                  <input type="text" class="form-control" name="permanent_ward_number" maxlength="3" placeholder="Enter Ward Number" value="{{ old('permanent_ward_number') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('permanent_ward_number') }}</a> 
                              </div>
                            </div>
                      <b style="padding-left: 30px;">Temporary Address:-</b>
                        <hr>
                            <div class="row">
                              <div class="form-group col-sm-4">
                                  <label for="temporary_country">Country<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="temporary_country" id="temporary_country">
                                                <option value=> Select Country </option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->name }}" @if(old('temporary_country')==$country->name)selected @endif>{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_country') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="temporary_province">Province<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="temporary_province" id="temporary_province">
                                      <option value=> Select province </option>
                                        @foreach($provinces as $province)
                                        <option value="{{ $province->name }}" @if(old('temporary_province')==$province->name)selected @endif>{{ $province->name }}</option>
                                        @endforeach
                                    </select>
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_province') }}</a> 
                              </div>



                              <div class="form-group col-sm-4">
                                  <label for="temporary_municipality">VDC/Municipality<span style="color: red;"> * </span></label>
                                  <input type="text" class="form-control" name="temporary_municipality" placeholder="Enter VDC/Municipality" value="{{ old('temporary_municipality') }}" maxlength="80">
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_municipality') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="temporary_street">Street</label>
                                  <input type="text" class="form-control" name="temporary_street" placeholder="Enter Street" value="{{ old('temporary_street') }}" maxlength="80">
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_street') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="temporary_ward_number">Ward Number</label>
                                  <input type="text" class="form-control" name="temporary_ward_number" maxlength="3" placeholder="Enter Ward Number" value="{{ old('temporary_ward_number') }}" maxlength="80">
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_ward_number') }}</a> 
                              </div>
                            </div>
                            <b style="padding-left: 30px;">Contact-</b>
                        <hr>
                            <div class="row">
                            
                              <div class="form-group col-sm-4">
                                  <label for="phone_no">Phone Number</label>
                                  <input type="text" class="form-control" name="phone_no" placeholder="Enter Phone Number" value="{{ old('phone_no') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('phone_no') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="mobile_no">Mobile Number</label>
                                  <input type="text" class="form-control" name="mobile_no" placeholder="Enter Mobile Number" value="{{ old('mobile_no') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('mobile_no') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="email">Email</label>
                                  <input type="eamil" class="form-control" name="email" placeholder="Enter Email" value="{{ old('email') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('email') }}</a> 
                              </div>
                          </div>

                         <p><b>PARENT'S DETAILS:-</b></p>
                  
                    <hr>
                        <div class="row">
                            
                              <div class="form-group col-sm-4">
                                  <label for="pname">Name<span style="color: red;"> * </span></label>
                                  <input type="text" class="form-control" name="pname" placeholder="Enter Parent's Name" value="{{ old('pname') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('pname') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="relation">Relation<span style="color: red;"> * </span></label>
                                  <input type="text" class="form-control" name="relation" placeholder="Enter Relation" value="{{ old('relation') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('relation') }}</a> 
                              </div>
                              <div class="form-group col-sm-4">
                                  <label for="education">Education<span style="color: red;"> * </span></label>
                                  <input type="text" class="form-control" name="education" placeholder="Enter Education" value="{{ old('education') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('education') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="occupation">Occupation<span style="color: red;"> * </span></label>
                                  <input type="text" class="form-control" name="occupation" placeholder="Enter Occupation" value="{{ old('occupation') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('occupation') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="income">Income(Monthly)<span style="color: red;"> * </span></label>
				<div class="input-group mb-2">
                                  <div class="input-group-prepend">
                                  	<div class="input-group-text">NRs.</div>
                                  </div>
                                  <input type="text" class="form-control form-control-line" maxlength="12" name="income" placeholder="Enter Income in Numeric" value="{{ old('income') }}"> 
                                  	<a href="" style="color: red;">{{ $errors -> first('income') }}</a>
                              	</div>
                             </div>
                             </div>
                        <div class="row">
                              <div class="form-group col-sm-5">
                                  Check if parent's Address Same as Student's Permanent Address
                                  <input type="radio" id="trigger" name="checkbox" value="1" @if(old('checkbox')==1) checked @endif> 
                              </div>
                              <div class="form-group col-sm-5">
                                  Check if parent's Address Same as Student's Temporary Address
                                  <input type="radio" id="trigger1" name="checkbox" value="2" @if(old('checkbox')==2) checked @endif> 
                              </div>
                              <div class="form-group col-sm-2">
                                  cancel selection
                                  <input type="radio" id="trigger2" name="checkbox" value="2" @if(old('checkbox')==2) checked @endif> 
                              </div>
                        </div>

                    <b style="padding-left: 30px;">Address:-</b>
                    <hr>

                        <div class="row" id="hidden_fields"> 
                                <div class="form-group col-sm-6">
                                  <label for="pcountry">Country</label>
                                    <select class="form-control" name="pcountry" id="pcountry">
                                      <option value=> Select Country </option>
                                        @foreach($countries as $country)
                                      <option value="{{ $country->name }}" @if(old('pcountry')==$country->name)selected @endif>{{ $country->name }}</option>
                                        @endforeach
                                    </select> 
                                  <a href="" style="color: red;">{{ $errors -> first('pcountry') }}</a>

                                </div>

                                <div class="form-group col-sm-6">
                                  <label for="pprovince">Province</label>
                                  <select class="form-control" name="pprovince" id="pprovince">
                                      <option value=> Select province </option>
                                        @foreach($provinces as $province)
                                        <option value="{{ $province->name }}" @if(old('pprovince')==$province->name)selected @endif>{{ $province->name }}</option>
                                        @endforeach
                                    </select>
                                  <a href="" style="color: red;">{{ $errors -> first('pprovince') }}</a>

                              </div>

                              <div class="form-group col-sm-6">
                                  <label for="parent_municipality">VDC/Municipality</label>
                                  <input type="text" class="form-control" name="parent_municipality" id="hidden_field" placeholder="Enter VDC/Municipality" value="{{ old('parent_municipality') }}"> 
                                  <a href="" style="color: red;">{{ $errors -> first('parent_municipality') }}</a>

                              </div>

                                <div class="form-group col-sm-6">
                                  <label for="parent_street">Street</label>
                                  <input type="text" class="form-control" name="parent_street" id="hidden_field" placeholder="Enter Street Name" value="{{ old('parent_street') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('parent_street') }}</a>

                              </div>

                              <div class="form-group col-sm-6">
                                  <label for="parent_ward_number">Ward Number</label>
                                  <input type="text" class="form-control" name="parent_ward_number" id="hidden_field" maxlength="3" placeholder="Enter Ward Number" value="{{ old('parent_ward_number') }}"> 
                                  <a href="" style="color: red;">{{ $errors -> first('parent_ward_number') }}</a>
                              </div>
                            </div>

                            <div class="row">
                              <div class="form-group col-sm-4">
                                  <label for="pphone">Phone Number</label>
                                  <input type="text" class="form-control" name="pphone" placeholder="Enter Parent's Phone Number" value="{{ old('pphone') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('pphone') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="pmobile">Mobile Number<span style="color: red;"> * </span></label>
                                  <input type="text" class="form-control" name="pmobile" placeholder="Enter Parent's Mobile Number" value="{{ old('pmobile') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('pmobile') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="pemail">Email</label>
                                  <input type="email" class="form-control" name="pemail" placeholder="Enter Parent's Email" value="{{ old('pemail') }}">
                                  <a href="" style="color: red;">{{ $errors -> first('pemail') }}</a> 
                              </div>
                              </div>

                              <div class="col-md-12" >
                                    <div class="text-center">
                                      <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        </div>
                     </div>
                </div>

  </form>

                                
 @endsection
 