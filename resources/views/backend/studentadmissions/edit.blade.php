@extends('backend.includes.app')
@section('content')


        <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Student Detail</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item">Student</li>
                                <li class="breadcrumb-item ">List</li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                            
                        </div>
                    </div>
                </div><!-- /.row -->
   <!-- /.container-fluid -->


<form enctype="multipart/form-data" method="post" action="{{route('studentadmissions.update',$studentadmission->id)}}">
<input type="hidden" name="_token" value="{{csrf_token()}}">
@method('PUT')
                        <p><b>PERSONAL DETAILS</b></p>
                    <hr>
                        <div class="row">
                          <div>
                            <input type="hidden" name="admission_number" value="{{ $studentadmission->admission_number }}">
                          </div>
                            <div class="form-group col-sm-4">
                                <label for="academic_year">Academic Year<span style="color: red;"> * </span></label>
                                <input type="text" id="datepicker" name="academic_year" class="form-control "  value="{{ $studentadmission->academic_year }}">
                                <a href="" style="color: red;">{{ $errors -> first('academic_year') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="register_number">Register Number<span style="color: red;"> * </span></label>
                                <input type="text" name="register_number" class="form-control "  value="{{ $studentadmission->register_number }}">
                                <a href="" style="color: red;">{{ $errors -> first('register_number') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="joining_date">Joining Date<span style="color: red;"> * </span></label>
                                  <input class="form-control mydatepicker" type="text" name="joining_date" placeholder="Enter Joining Date" value="{{$studentadmission->joining_date }}">
                                  <a href="" style="color: red;">{{ $errors -> first('joining_date') }}</a>
                                </div>

                        <div class="form-group col-sm-4">
                            <label name="room_id" for="room_id">Class <span style="color: red;">*</span></label>
                            <select class="form-control class" name="room_id">
                                <option value=>Select Class</option>
                                @foreach($room as $room)
                                     <option value="{{$room->id}}"
                                      @if ($room->id == $studentadmission->room_id)
                                      selected
                                      @endif 
                                      >{{$room->cname}}</option>
                                @endforeach
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>
                        </div>

                                                    
                        <div class="form-group col-sm-4">
                            <label name="section_id" for="section_id">Section </label>
                            <select class="form-control section" name="section_id">
                               
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('section_id') }}</a>
                        </div>

                              <div class="form-group col-sm-4">
                                <label for="first_name">First Name<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="first_name" value="{{ $studentadmission->first_name }}">
                                <a href="" style="color: red;">{{ $errors -> first('first_name') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="middle_name">Middle Name</label>
                                <input type="text" class="form-control" name="middle_name" value="{{ $studentadmission->middle_name }}">
                                <a href="" style="color: red;">{{ $errors -> first('last_name') }}</a>

                              </div>

                              <div class="form-group col-sm-4">
                                <label for="last_name">Last Name<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="last_name" value="{{ $studentadmission->last_name }}">
                                <a href="" style="color: red;">{{ $errors -> first('last_name') }}</a>
                              </div>

                              <div class="form-group col-sm-3">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" name="image" id="file" onchange="return fileValidation()" placeholder="Select">
                                  <a href="" style="color: red;">{{ $errors -> first('image') }}</a>

                              </div>

                                <div class="form-group col-sm-1">
                                @if($studentadmission->image)
                            <img src="{{ asset('public/backend/images/student/'.$studentadmission->image) }}" style="width:80px;">
                            @endif
                              </div>
                              

                                <div class="form-group col-sm-4">
                                  <label for="dob">Date of Birth<span style="color: red;">*</span></label>
                                  <input class="form-control mydatepicker" type="text" name="dob" value="{{ $studentadmission->dob }}">
                                  <a href="" style="color: red;">{{ $errors -> first('dob') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="gender">Gender<span style="color: red;">*</span></label>
                                  <select class="form-control" name="gender" id="gender">
                                    <option value="{{$studentadmission->gender}}"
                                    @if($studentadmission->gender == $studentadmission->gender)
                                    selected
                                    @endif

                                    >{{$studentadmission->gender}}</option>
                                  <option value="male">Male</option>
                                  <option value="female">Female</option>
                                  <option value="other">Other</option>

                                </select>
                                <a href="" style="color: red;">{{ $errors -> first('gender') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="blood_group">Blood Group</label>
                                  <select class="form-control" name="blood_group" id="blood_group">
                                    <option value="{{$studentadmission->blood_group}}"
                                    @if($studentadmission->blood_group == $studentadmission->blood_group)
                                    selected
                                    @endif

                                    >{{$studentadmission->blood_group}}</option>
                                  <option value="A+">A+</option>
                                  <option value="A-">A-</option>
                                  <option value="AB+">AB+</option>
                                  <option value="AB-">AB-</option>
                                  <option value="B+">B+</option>
                                  <option value="B-">B-</option>
                                  <option value="O+">O+</option>
                                  <option value="O-">O-</option>
                                </select>
                                <a href="" style="color: red;">{{ $errors -> first('blood_group') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="birth_place">Birth Place<span style="color: red;">*</span></label>
                                  <input class="form-control" type="birth_place" name="birth_place" value="{{ $studentadmission->birth_place }}">
                                  <a href="" style="color: red;">{{ $errors -> first('birth_place') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="nationality">Nationality<span style="color: red;"> * </span></label>
                                  <select class="form-control" name="nationality" id="nationality">
                                        @foreach($nationalities as $nationality)
                                          <option value="{{ $nationality->name }}"
                                            @if ($nationality->name == $studentadmission->nationality)
                                      selected
                                      @endif >{{ $nationality->name }}</option>
                                                @endforeach
                                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('nationality') }}</a>
                                </div>



                                

                                <div class="form-group col-sm-4">
                                  <label for="rollno">Roll Number</label>
                                  <input class="form-control" type="text" name="rollno" value="{{ $studentadmission->rollno }}">
                                  <a href="" style="color: red;">{{ $errors -> first('rollno') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="mother_tongue">Mother Tongue<span style="color: red;">*</span></label>
                                  <input class="form-control" type="text" name="mother_tongue" value="{{ $studentadmission->mother_tongue }}">
                                  <a href="" style="color: red;">{{ $errors -> first('mother_tongue') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="student_id">Discount Category</label>
                                  <select class="form-control" name="student_id">
                                @foreach($student as $student)
                                     <option value="{{$student->id}}"
                                      @if ($student->id == $studentadmission->student_id)
                                       selected
                                       @endif>{{$student->category}}</option>
                                @endforeach
                            </select>

                            
                                  <a href="" style="color: red;">{{ $errors -> first('student_id') }}</a>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="religion">Religion<span style="color: red;">*</span></label>
                                  <input class="form-control" type="text" name="religion" value="{{ $studentadmission->religion }}">
                                  <a href="" style="color: red;">{{ $errors -> first('religion') }}</a>
                                </div>
                            </div>
                                <p><b>CONTACT DETAILS:-</b></p>
                    <hr>
                    <b style="padding-left: 30px;">Permanent Address:-</b>
                    <hr>
                        <div class="row">
                              <div class="form-group col-sm-4">
                                  <label for="country">Country<span style="color: red;">*</span></label>
                                  <select class="form-control" name="country" id="nationality">
                                        @foreach($countries as $country)
                                          <option value="{{ $country->name }}"
                                            @if ($country->name == $studentadmission->country)
                                      selected
                                      @endif >{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('country') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="province">Province<span style="color: red;">*</span></label>
                                  <select class="form-control" name="province" id="nationality">
                                        @foreach($provinces as $province)
                                          <option value="{{ $province->name }}"
                                            @if ($province->name == $studentadmission->province)
                                      selected
                                      @endif >{{ $province->name }}</option>
                                                @endforeach
                                            </select> 
                                  <a href="" style="color: red;">{{ $errors -> first('province') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="permanent_municipality">Permanent Municipality<span style="color: red;">*</span></label>
                                  <input type="text" class="form-control" name="permanent_municipality" value="{{ $studentadmission->permanent_municipality }}">
                                  <a href="" style="color: red;">{{ $errors -> first('permanent_municipality') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="permanent_street">Permanent Street</label>
                                  <input type="text" class="form-control" name="permanent_street" value="{{ $studentadmission->permanent_street }}">
                                  <a href="" style="color: red;">{{ $errors -> first('permanent_street') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="permanent_ward_number">Permanent Ward Number</label>
                                  <input type="text" class="form-control" name="permanent_ward_number" value="{{ $studentadmission->permanent_ward_number }}">
                                  <a href="" style="color: red;">{{ $errors -> first('permanent_ward_number') }}</a> 
                              </div>
                            </div>

                             <b style="padding-left: 30px;">Temporary Address:-</b>
                        <hr>
                            <div class="row">
                              

                              <div class="form-group col-sm-4">
                                  <label for="temporary_country">Country<span style="color: red;">*</span></label>
                                  <select class="form-control" name="temporary_country" id="temporary_country">
                                        @foreach($countries as $country)
                                          <option value="{{ $country->name }}"
                                            @if ($country->name == $studentadmission->temporary_country)
                                      selected
                                      @endif >{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_country') }}</a>
                              </div>

                              

                              <div class="form-group col-sm-4">
                                  <label for="temporary_province">Province<span style="color: red;">*</span></label>
                                  <select class="form-control" name="temporary_province" id="temporary_province">
                                        @foreach($provinces as $province)
                                          <option value="{{ $province->name }}"
                                            @if ($province->name == $studentadmission->temporary_province)
                                      selected
                                      @endif >{{ $province->name }}</option>
                                                @endforeach
                                            </select> 
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_province') }}</a>
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="temporary_municipality">VDC/Municipality<span style="color: red;"> * </span></label>
                                  <input type="text" class="form-control" name="temporary_municipality" placeholder="Enter VDC/Municipality" value="{{ $studentadmission->temporary_municipality }}" maxlength="80">
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_municipality') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="temporary_street">Street</label>
                                  <input type="text" class="form-control" name="temporary_street" placeholder="Enter Street" value="{{ $studentadmission->temporary_street }}" maxlength="80">
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_street') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="temporary_ward_number">Ward Number</label>
                                  <input type="text" class="form-control" name="temporary_ward_number" placeholder="Enter Ward Number" value="{{ $studentadmission->temporary_ward_number }}" maxlength="80">
                                  <a href="" style="color: red;">{{ $errors -> first('temporary_ward_number') }}</a> 
                              </div>
                            </div>
                              
                              <div class="row">                              
                                <div class="form-group col-sm-4">
                                  <label for="phone_no">Phone Number</label>
                                  <input type="text" class="form-control" name="phone_no" value="{{ $studentadmission->phone_no }}">
                                  <a href="" style="color: red;">{{ $errors -> first('phone_no') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="mobile_no">Mobile Number<span style="color: red;">*</span></label>
                                  <input type="text" class="form-control" name="mobile_no" value="{{ $studentadmission->mobile_no }}">
                                  <a href="" style="color: red;">{{ $errors -> first('mobile_no') }}</a> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="email">Email</label>
                                  <input type="email" class="form-control" name="email" value="{{ $studentadmission->email }}"> 
                                  <a href="" style="color: red;">{{ $errors -> first('email') }}</a>
                              </div>
                            </div>

                             
                            
                                  
                                  <input type="hidden" class="form-control" name="pname" value="{{$studentadmission->pname}}"> 

                                  
                                  <input type="hidden" class="form-control" name="relation" value="{{$studentadmission->relation}}"> 
                                  <input type="hidden" class="form-control" name="education" value="{{$studentadmission->education}}" > 
                                  <input type="hidden" class="form-control" name="occupation" value="{{$studentadmission->occupation}}"> 


                                  <input type="hidden" class="form-control" name="income" value="{{$studentadmission->income}}">  

                                  <input type="hidden" class="form-control" name="pprovince" id="hidden_field" value="{{$studentadmission->pprovince}}"> 

                              
                                  <input type="hidden" class="form-control" name="pcountry" id="hidden_field" value="{{$studentadmission->pcountry}}"> 
                              

                                  <input type="hidden" class="form-control" name="parent_street" value="{{$studentadmission->parent_street}}"> 
                                  <input type="hidden" class="form-control" name="parent_municipality" value="{{$studentadmission->parent_municipality}}"> 
                                  <input type="hidden" class="form-control" name="parent_ward_number" value="{{$studentadmission->parent_ward_number}}"> 
                                  
                                  <input type="hidden" class="form-control" name="pphone" value="{{$studentadmission->pphone}}"> 
                              

                              
                                  <input type="hidden" class="form-control" name="pmobile" value="{{$studentadmission->pmobile}}"> 
                              

                              
                                  <input type="hidden" class="form-control" name="pemail" value="{{$studentadmission->pemail}}"> 
                             

                                    
                                  <div class="col-md-12" >
                                    <div class="text-center">
                                      <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>

                                    </div>
                                  </div>

                            


                        </div>
                     </div>
                </div>
            
  </form>                                    
                                
 @endsection