@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h4 class="text-themecolor">Fee Allocation</h4>
   </div>
   <div class="col-md-7 align-self-center text-right">
      <div class="d-flex justify-content-end align-items-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item ">Fee Allocation</li>
         </ol>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-lg-4 col-xlg-4 col-md-4">
      <div class="card">
         <div class="card-body">
            <p>Fee Allocation</p>
            <form class="form-horizontal" method="post" action="{{ route('feeallocate.store')}}" enctype="multipart/form-data" >
               @csrf
               <div class="form-group">
                  <div class="row">
                      <label name="feecategory" class="col-md-4" for="name">Fee Category</label>
                     <div class="col-sm-12   ">
                                            <select class="form-control" name="feecategory" id="feecategory">
                                                <option value=>--Select Feecategory--</option>
                                                @foreach($feecategory as $fsc)
                                                    <option value="{{ $fsc->feecategory }}" >{{ $fsc->feecategory }}</option>
                                                @endforeach
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('fsc') }}</a>
                                        </div>

                  </div>
               </div>
                 <div class="form-group">
                  <div class="row">
                     <label name="feesubcategory" class="col-md-4" for="name">Fee SubCategory</label>
                     <div class="col-md-12">
                       <select class="form-control" name="feesubcategory" id="feesubcategory">
                                                <option value=>--Select Feesubcategory--</option>
                                                @foreach($feesubcategory as $fscf)
                                                    <option value="{{ $fscf->feesubcategoryname }}" >{{ $fscf->feesubcategoryname }}</option>
                                                @endforeach
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('fscf') }}</a>
                     </div>
                  </div>
               </div>
             
             <div class="form-group">
                  <div class="row">
                     <label name="feefor" class="col-md-4" for="name">Fee For</label>
                    <div class="col-sm-12">
                                            <select class="form-control" name="feefor" id="feefor">
                                                <option value=>Please Select</option>
                                                <option value="allbatches" @if(old('feefor') == 'allbatches')selected @endif>All Batches</option>
                                                <option value="selectedbatches" @if(old('feefor') == 'selectedbatches')selected @endif>Selected Batches</option>
                                                <option value="studentinabatch" @if(old('feefor') == 'studentinabatch')selected @endif>Student in a Batches</option>
                                                <option value="categorywise" @if(old('feefor') == 'categorywise')selected @endif>Category Wise</option>
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('feefor') }}</a>

                                        </div>
                  </div>
               </div>
               <div class="form-actions">
                  <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-lg-8 col-md-8">
      <div class="card-center">
         <div class="card-body">
            Fee Allocation 
            <div class="table-responsive m-t-40">
               <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                     <tr>
                        <th>SN</th>
                        <th>Fee Category</th>
                        <th>Fee SubCategory</th>
                        <th>Fee For</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($feeDetail as $fscf)
                     <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $fscf->feecategory }}</td>
                        <td>{{ $fscf->feesubcategory }}</td>
                        <td>{{ $fscf->feefor }}</td>
                        <td>
                           <a href="{{ route('feeallocate.update',$fscf->id)}}" class="ti-pencil ti-pencil-info" data-toggle="modal" data-target=".bs-example-modal-lg1{{$fscf->id}}" class="model_img img-responsive"></a>&nbsp;

                           <a href="#" class="ti-eye ti-eye-info" data-toggle="modal" data-target=".bs-example-modal-lg{{$fscf->id}}" class="model_img img-responsive" ></a> &nbsp;

                           <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
                           <form method="get" id="del" action="{{ route('feeallocate.destroy',$fscf->id)}}">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              @method('DELETE')
                           </form>

                        </td>
                     </tr>
                     <div class="modal fade bs-example-modal-lg1{{$fscf->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                        myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myLargeModalLabel">Fee Allocation</h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="{{ route('feeallocate.update',$fscf->id)}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="row">
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Fee Category</strong>
                                          <br>
                                          <input type="text" name="feecategory" value=" {{($fscf->feecategory)}}">
                                       </div>

                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Fee SubCategory</strong>
                                          <br>
                                          <input type="text" name="feesubcategory" value=" {{($fscf->feesubcategory)}}">
                                       </div>

                                       {{-- <div class="col-md-3 col-xs-6 b-r"> <strong>Type</strong>
                                          <br>
                                          <input type="text" name="type" value="{{($fscf->type)}}">
                                       </div> --}}
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Fee For</strong>
                                          <br>
                                          <input type="text" name="feefor" value="{!!($fscf->feefor) !!}">
                                       </div>
                                    </div>
                                    <div class="modal-footer">
                                       <div class="form-actions">
                                          <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>
                                       </div>
                                       <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal fade bs-example-modal-lg{{$fscf->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                        myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myLargeModalLabel">Fee Allocation</h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    @method('PUT')
                                    <div class="row">
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Fee Category</strong>
                                          <br>
                                          <p class="text-muted">{{($fscf->feecategory)}}</p>
                                       </div>

                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Fee SubCategory</strong>
                                          <br>
                                          <p class="text-muted">{{($fscf->feesubcategory)}}</p>
                                       </div>

                                       {{-- <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Type</strong>
                                          <br>
                                          <p class="text-muted">{{($fscf->type)}}</p>
                                       </div> --}}
                                       <div class="col-md-3 col-xs-6 b-r">
                                          <strong>Fee For</strong>
                                          <br>
                                          <p class="text-muted">{!!($fscf->feefor) !!}</p>
                                       </div>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                    </div>
                                 </form>
                                 @endforeach
                              </div>
                           </div>
                        </div>
                     </div>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div> 
</div>
</div>
@endsection