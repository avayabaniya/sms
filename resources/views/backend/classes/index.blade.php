@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item">Academic</li>
                                <li class="breadcrumb-item active">Class</li>

                            </ol>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
            <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Add Class</p>
                        <form class="form-horizontal" method="post" action="{{route('classessave')}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                        <div class="form-group">

                            <label class="control-label" for="cname">Class in Alphabet<span style="color: red;">*</span></label>
                            <input type="text" id="cname" name="cname" class="form-control" placeholder="Enter class in Alphabet" value="{{old('cname')}}" >
                            <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>             
                        </div>

                       <!--  <div class="form-group">
                            <label class="control-label" for="room_code">Class in Numeric<span style="color: red;">*</span></label>
                            <input type="text" id="room_code" name="room_code" placeholder="Enter class in Numeric" class="form-control" value="{{old('room_code')}}" >
                            <a href="" style="color: red;">{{ $errors -> first('room_code') }}</a>             
                        </div> -->
                      {{--  <div class="form-group">
                            <label class="control-label" for="room_code">Class in Numeric<span style="color: red;">*</span></label>
                            <input type="text" id="room_code" name="room_code" placeholder="Enter class in Numeric" class="form-control" value="{{old('room_code')}}" >
                            <a href="" style="color: red;">{{ $errors -> first('room_code') }}</a>             
                        </div>--}}

                             
                              <div class="form-actions">
                                 <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                              </form>
                            </div>
                            
                        </div>


                    </div>
                <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
            <div class="card-body">
                           Show Class list
                <div class="row">
                       
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Class in  Alphabet </th>
                                                <th>Class in Numeric</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($rooms as $ro)
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                <td>{{ucfirst(trans($ro->cname))}}</td>
                                                <td> {{($ro->room_code)}}</td>
                                                <td>


                        <a href="{{ route('classupdate',$ro->id)}}" class="ti-pencil ti-pencil-info" data-toggle="modal" data-target=".bs-example-modal-lg1{{$ro->id}}" class="model_img img-responsive"></a>&nbsp;

                        
              <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
              <form method="post" id="del" class="delete" action="{{ route('classdestroy',$ro->id)}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @method('DELETE')
              </form>
          </td>
                        </tr>  
                            <div class="modal fade bs-example-modal-lg1{{$ro->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                                    myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Class Edit  </h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" method="post" action="{{ route('classupdate',$ro->id)}}" enctype="multipart/form-data">
                                                 <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                

                                                    <div class="row">
                                                            <div class=" col-md-6 col-xs-6 b-r"> <strong>Class in Alphabet</strong>
                                                                <br>
                                                                <input type="text" name="cname" value=" {{ucfirst(trans($ro->cname))}}">
                                                                
                                                            </div>
                                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Class in Numeric</strong>
                                                                <br>
                                                                <input type="text" name="room_code" value="{{($ro->room_code)}}">
                                                                
                                                            </div>
                                                            
                                                        </div>
                                            <div class="modal-footer">
                                                 <div class="form-actions">
                                                <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>
                                        
                                                </div>
                                            </div>
                                             </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>

                            
                                            @endforeach
                                                

                                        </tbody>
                                    </table>
                   </div>
               </div>
           </div>
     

                    </div>
                </div>
             
    </div>
    
      @endsection