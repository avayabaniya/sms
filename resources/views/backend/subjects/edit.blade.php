@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item ">Academic</li>
                                <li class="breadcrumb-item active">Subject edit</li>
                            </ol>
                         </div>
                    </div>
                </div>
              <div class="row">
            <div class="col-lg-6">         
                          <div class="card">               
                            <div class="card-body"> 
                               <p>Subject Edit</p>
                        <form class="form-horizontal" method="post" action="{{route('subjectupdate', $sub->id)}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                        
                        <div class="form-group">
                            <label class="control-label" for="room_id">Class</label>
                            <select  name="room_id" class="form-control custom-select">
                               @foreach($arr['rooms'] as $roo)
                                <option  @if($sub->room["cname"] == $roo->cname) selected @endif value="{{$roo->id}}">{{ucfirst($roo->cname)}}</option>
                                @endforeach
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>        
                        </div>
                     
                      </div>
                    </div>
             
                </div>
                    
            <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body"> 
                                               
                           <?php $details = json_decode($sub->topic); ?>
                               
                              <div class="form-group ">
                               <div class="control-label" id="parent_ph">
                                   <label for="exampleInputuname">Subject Name</label>

                                   @for($i=0;$i<sizeof($details[0]);$i++)
                                   <div class="input-group mb-3" >

                                       <div class="input-group-prepend controls">
                                           <span class="input-group-text" id="basic-addon1"></span>
                                       </div>
                                       
                                               <input type="text" value="{{ ucfirst($details[0][$i]) }}"
                                                name="topic[]" id="topic" placeholder="Enter Subject" class="form-control" aria-label="name" aria-describedby="basic-addon1">
                                       <a onclick=javascript:removeElement('ph_desc_div"+itemID+"');" +
                                        " return false;'>
                                        <button class='btn btn-danger'> - </button></a>                                   
                                   </div>
                                    @endfor
                               </div>
                           <button class="btn btn-success" type="button" style="margin-left:10px;"  onclick="addItem_to();"> <big> + </big> </button>
                           <a href="" style="color: red;">{{ $errors -> first('topic') }}</a>
                           </div>
                                <div class="form-actions">
                                 <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                               </div>
                          </div>
                              </form>
                           
                    </div>
                        </div>
                              
                            </div>
                        </div>
                    </div>
                
             
    </div>
    
      






@endsection