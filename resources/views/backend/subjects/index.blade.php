@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item ">Academic</li>
                                <li class="breadcrumb-item active">Subjects</li>

                            </ol>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
            <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Add Subjects</p>
                        <form class="form-horizontal" method="post" action="{{route('subjectsave')}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                        
                        <div class="form-group">
                                <label name="room_id" for="room_id">Class <span style="color: red;">*</span></label>
                                <select  name="room_id" class="form-control custom-select">
                                    <option >Choose Class</option>
                                     @foreach($arr['rooms'] as $room)
                                     <option value="{{$room->id}}">{{ucfirst($room->cname)}}</option>
                                    @endforeach
                             </select>
                                <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>
                        </div>
                          
                         <div class="form-group ">
                               <div class="control-label" id="parent_ph">
                                   <label for="exampleInputuname">Subject</label>
                                   <div class="input-group mb-3" >
                                       <div class="input-group-prepend controls">
                                           <span class="input-group-text" id="basic-addon1"></span>
                                       </div>
                                       <input type="text" name="topic[]" id="topic" class="form-control" placeholder="Enter Subject" aria-label="name" aria-describedby="basic-addon1">
                                        <button class="btn btn-success" type="button" style="margin-left:10px;"  onclick="addItem_to();"> <big> + </big> </button>
                                   </div>
                               </div>
                          
                           <a href="" style="color: red;">{{ $errors -> first('topic[]') }}</a>
                           </div>
                           
                              <div class="form-actions">
                                 <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                              </form>
                            </div>
                            
                        </div>


                    </div>
            <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
            <div class="card-body">
                           Show Subject list
                <div class="row">
                       
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Class</th>
                                                <th>Subject Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                       
                                        <tbody>
                                            @foreach($arr['subjects'] as $sub)
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                <td>{{ucfirst($sub->room['cname'])}}</td>
                                                <td>
                                                  <?php $details = json_decode($sub->topic); ?>
                                                    <ul>
                                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                                        <li>{{ ucfirst($details[0][$i] )}}</li>
                                                        @endfor
                                                    </ul>
                                                </td>
                                                <td>
 
               <a href="{{ route('subjectedit',$sub->id)}}" class="ti-pencil ti-pencil-info"></a>&nbsp;
                     

              <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
              <form method="post" id="del" class="delete" action="{{ route('subjectdestroy',$sub->id)}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @method('DELETE')
            </form></td>
                  </tr>  
                

                      @endforeach
                                            
                                        </div>
                                       
                                    </div>
                                   
                                </div>
                                         
                                        </tbody>
                                    </table>

                                    
                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             
    </div>
    
      







@endsection