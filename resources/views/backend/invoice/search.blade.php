@extends('backend.includes.app')
@section('content')

<div class="col-lg-12 col-md-8">
   <div class="card-center">
         <div class="card-body">
            @if(isset($details))
            Invoice   
            <div class="table-responsive m-t-40">
               <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                     <tr>
                        <th>SN</th>
                        <th>Class</th>
                        <th>Section</th>
                        <th>Student</th>
                        <th>Date</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($feeDetails as $fd)
                     <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $fd->room['cname'] }}</td>
                        <td>{{ $fd->Section['sname'] }}</td>
                        <td>
                          @foreach($ganga as $d)
                           @if($fd->student_admission_id == $d->id)
                            {{$d->first_name.' '.$d->middle_name.' '.$d->last_name}}
                           @endif
                           @endforeach
                        </td>
                        <td>{{ $fd->date }}</td>
                        <td> 
                           <a href="{{ route('invoice.edit', $fd->id)}}" class="ti-pencil ti-pencil-info"></a> &nbsp;
                          <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>

                           <a href="{{ route('invoice.editinvoice', $fd->id)}}" class="fa fa-print"></a>&nbsp;
                            

                           <form method="post" id="del" class="delete" action="{{ route('invoice.destroy',$fd->id)}}">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              @method('DELETE')
                           </form>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
            @endif
         </div>
      </div>
   </div>
   @endsection