@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h4 class="text-themecolor">Invoice</h4>
   </div>
   <div class="col-md-7 align-self-center text-right">
      <div class="d-flex justify-content-end align-items-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item ">Invoice</li>
         </ol>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-lg-12 col-xlg-4 col-md-4">
      <div class="card">
         <div class="card-body">
            <form class="form-material m-t-40" method="post" action="{{route('storeinvoice')}}" enctype="multipart/form-data" >
               @csrf
                <div class="card card-body printableArea">
                <h3><b>INVOICE</b> <span class="pull-right">#{{rand(1,999999)}}</span></h3>  
                    <hr>

                     <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                       <address>
                                          @foreach($setting as $setting)
                                            <h3> &nbsp;<b class="text-danger">{{$setting->name}}</b></h3>
                                              <div style="padding-left: 10px;">
                                              {{$setting->address}}
                                              <br>
                                              {{$setting->number}}
                                              <br>
                                              {{$setting->country}}
                                            </div>
                                            @endforeach
                                            <p class="text-muted m-l-5">
                                       </address>
                                    </div>
                                    <div class="pull-right text-right">
                                        <address>
                                            <h3>To,</h3>
                                            <h7 class="font-bold">
                                            {{$studentadmission->first_name.' '.$studentadmission->middle_name.' '.$studentadmission->last_name}}
                         </h7>
                         <br>
                             <label>Class:-  {{ $studentadmission->room['cname'] }}, {{ $studentadmission->section[('sname')] }} <br>
                              Roll Number: {{$studentadmission->rollno}}
                         </label>
                           <br>
                            {{$studentadmission->temporary_municipality}},
                            {{$studentadmission->temporary_street}},
                            {{$studentadmission->country}}
                              <br>
                              <br>
                            <b>Invoice Date :</b> <i class="fa fa-calendar"></i>{{ date('Y-m-d') }} 
                         </address>
                       </div>
                     </div>
                   </div>

                                  <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">SN</th>
                                                    <th>Fee Type</th>
                                                    <th class="text-right">Amount</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                             <?php $k=0 ?>
                                             <?php $sum=0 ?>

                                             @foreach($feetype as $fee =>$value)
                                              
                                                   @if($value->room_id == $studentadmission->room_id)

                                                   <?php $k++;?>
                                                  
                                              
                                                <tr>

                                                    <td class="text-center">{{ $k }}</td>
                                                    <td>
                                                      {{$value->feetype}}
                                                      </td>
                                                    <td class="text-right">
                                                      {{$value->amount}} 
                                                      <?php $sum=$sum+$value->amount;?>
                                 
                                                    </td>
                                                   </tr>

                                                @endif 

                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="pull-right m-t-30 text-right">
                                      <div class="pull-right m-t-30 text-right">
                                        
                                        <p>Sub - Total Amount: <?php  echo $sum; ?></p>
                                        @if($studentadmission->student_id == '')
                                       <p> Discount : 0%; </p>
                                      
                                       <hr>
                                       <h3> <b> Total :</b> <?php echo $sum; ?> </h3>
                                      
                                      @else
                                        <p>Discount: {{ $studentadmission->student->percent}}%
                                        
                                          <?php $finalSum =  ($studentadmission->student->percent/100)*$sum?> 
                                        <hr>
                                                
                                         <h3><b>Total :</b> <?php  echo $monthly = $sum - $finalSum; ?> </h3>
                                            @if(!empty($invoice))

                                         <hr>
                                          @if($invoice->status == 1)
                                        <h3><b>Due Amount :</b> <?php  echo $monthly = $sum - $finalSum; ?> </h3>
                                        @elseif($invoice->status == 2)
                                        <h3><b>Due Amount :</b> <?php  echo $monthly = $invoice->partial; ?> </h3>
                                         @elseif($invoice->status == 3)
                                        <h3><b>Due Amount :</b> <?php  echo $monthly = 0 ?> </h3>
                                        @endif
                                        @endif

                                        <input type="hidden" id="total" class="total" name="total_amount" value="{{$monthly}}">
                                       
                                        @endif
                                         <input type="hidden" name="student_id" value="{{$studentadmission->student_id}}">
                                        <input type="hidden" name="student_admission_id" value="{{$studentadmission->id}}">
                                        <input type="hidden" name="room_id" value="{{$studentadmission->room_id}}">
                                        <input type="hidden" name="section_id" value="{{$studentadmission->section_id}}">

                                        <div class="col-md-6">
                                          @if(empty($invoice->status))
                                            <input type="number"  name="paid_amount" class="paid_amount" placeholder="Input Amount">
                                          @elseif($invoice->status == 3)
                                          <input type="disabled" name="paid_amount" class="paid_amount" readonly="readonly">
                                          @else
                                        <input type="number"  name="paid_amount" class="paid_amount" placeholder="Input Amount">
                                        @endif
                                        </div>

                                        <br>

                                        <div class="col-md-6">
                                        <input type="disabled" readonly="readonly" class="partial" name="partial" value="{{$monthly - request()->paid_amount}}" placeholder="Complete Payment">
                                        </div>
                                        
                                    </div>
                                </div>  


            </form>
          </table>
          <br>
       <div class="text-right" >
        <input type="submit"  value="Payment" class="btn btn-success btn-success-lg">
                  <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
            </div>
          <br>
      </div>
   </div>
</div>
</p>
</div>
</div>
        
</form>

@endsection