<?php
   use \App\Http\Controllers\Controller;
   $rooms = Controller::rooms();
   $invoices = Controller::invoices();
   ?>
@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h4 class="text-themecolor">Invoice</h4>
   </div>
   <div class="col-md-7 align-self-center text-right">
      <div class="d-flex justify-content-end align-items-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item ">Invoice</li>
         </ol>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-lg-12 col-xlg-4 col-md-4">
      <div class="card">
         <div class="card-body">
            <p>Invoice</p>
            <form class="form-horizontal" method="post" action="{{URL::to('/search/invoice')}}" enctype="multipart/form-data" id="myform" role="search">
               @csrf
               <div class="form-group">
                  <div class="row">
                     <label name="room_id" class="col-md-4" for="room_id">Class</label>
                     <select id="class" class="form-control class" name="cname"  required>
                        <option value=>Select Class</option>
                        @foreach($rooms as $ro)
                        <option value="{{$ro->id}}" >{{ ucfirst($ro->cname) }}</option>
                        @endforeach
                     </select>
                     <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <label name="section_id" class="col-md-4" for="section_id">Section</label>
                     <select id="section" class="form-control section" name="sname" style="width: 100%; height:36px;">
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <label name="student_admission_id" class="col-md-4" for="student_admission_id">Student</label>
                     <select class="form-control student_admission" name="student" id="student_admission" required>
                     </select>
                     <a href="" style="color: red;">{{ $errors -> first('student_admission_id') }}</a>
                  </div>
               </div>
               <div class="form-actions">
                  <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>   
               </div>
            </form>
         </div>
      </div>
   </div>
   @if(isset($details))
   <div class="col-lg-12 col-md-8">
      <div class="card-center">
         <div class="card-body">
            Invoice   
            <div class="table-responsive m-t-40">
               <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                     <tr>
                        <th>SN</th>
                        <th>Class</th>
                        <th>Section</th>
                        <th>Status</th>
                        <th>Student</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($details as $d)
                     <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $d->room['cname'] }}</td>
                        <td>{{ $d->section['sname'] }}</td>
                        @if(empty($invoice1->status))
                        <td><span class="label label-danger">Unpaid</span></td>
                        @elseif($invoice1->status == 1)<td><span class="label label-danger">Unpaid</span></td>
                        
                        @elseif($invoice1->status == 2)<td><span class="label label-warning">Partial</span></td>
                        
                        @elseif($invoice1->status ==3)<td><span class="label label-success">Paid</span></td>
                        
                        @endif

                        <td>{{$d->first_name.' '.$d->middle_name.' '.$d->last_name}}</td>
                        <td> 
                           {{--  <a href="{{ route('invoice.edit', $d->id)}}" class="ti-pencil ti-pencil-info"></a> &nbsp;
                           <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a> --}}
                           <a href="{{ route('invoice.editinvoice', $d->id)}}" class="fa fa-money"></a>&nbsp;
                           {{-- <a href="{{route('invoice.payment')}}" class="fa fa-plus" data-toggle="modal" data-target=".bs-example-modal-md{{$d->id}}" class="model_img img-responsive"></a>&nbsp; --}}
                        </td>
                     </tr>
                     <div class="modal fade bs-example-modal-md{{$d->id}}"tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-md">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title" id="myModalLabel">Payment Invoice</h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                 <form class="form-horizontal" method="post" action="{{ route('invoice.payment',$d->id)}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="form-group">Date:-
                                       <input type="text" name="date" class="form-control" id="myModalLabel" value=" {{date('Y-m-d')}}">
                                    </div>
                                    <div class="form-group"> Amount:-
                                       <input type="text" name="monthly" class="form-control"  value=" {{$d->monthly}}">
                                    </div>
                                    <div class="modal-footer">
                                       <div class="form-actions">
                                          <button type="submit" class="btn btn-info"> Process To Payment</button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   @endif
</div>
</div>
@endsection