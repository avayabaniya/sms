@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
 <div class="col-md-5 align-self-center">
  <h4 class="text-themecolor">Edit Invoice</h4>
</div>
<div class="col-md-7 align-self-center text-right">
  <div class="d-flex justify-content-end align-items-center">
   <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
    <li class="breadcrumb-item ">Invoice</li>
  </ol>
</div>
</div>
</div>
<div class="row">
 <div class="col-lg-12 col-xlg-4 col-md-4">
  <div class="card">
   <div class="card-body">
    <h5> Edit Invoice</h5>
    <form class="form-horizontal" method="post" action="{{ route('invoice.update', $invoice->id)}}" enctype="multipart/form-data" >
     @csrf
     <div class="form-group">
      <div class="row">
       <label name="room_id" class="col-md-4" for="room_id">Class</label>
       <select  name="room_id" class="form-control">
         @foreach($room as $ro)
         <option  @if($ro->id == $invoice->room_id) selected @endif value="{{$ro->id}}">{{ucfirst(trans($ro->cname))}}</option>
         @endforeach
       </select>
       <a href="" style="color: red;">{{ $errors -> first('room-id') }}</a>
     </div>
   </div>

   <div class="form-group">
    <div class="row">
     <label name="section_id" class="col-md-4" for="section_id">Section</label>
     <select class="form-control section" name="section_id" id="section" required>
      @foreach($sections as $fd)
      <option  @if($fd->id == $invoice->section_id) selected @endif value="{{$fd->id}}">{{ucfirst(trans($fd->sname))}}</option>
      @endforeach
    </select>

    <a href="" style="color: red;">{{ $errors -> first('section_id') }}</a>
  </div>
</div>

<div class="form-group">
    <div class="row">
  <label for="student_id">Student Discount Category</label>
  <select class="form-control" name="student_id">
    @foreach($studentdiscountcategory as $studentdc)
    <option value="{{$studentdc->id}}"
      @if ($studentdc->id == $invoice->student_id)
      selected
      @endif>{{$studentdc->category}}</option>
      @endforeach
    </select>
    <a href="" style="color: red;">{{ $errors -> first('student_id') }}</a>
  </div>
</div>

  <div class="form-group">
    <div class="row">
    <label for="student_admission_id">Student</label>
    <select class="form-control student_admission" name="student_admission_id" id="student_admission">
      @foreach($studentadmission as $st)
      <option value="{{$st->id}}"
       @if($st->id == $invoice->student_admission_id)
       selected
       @endif>{{$st->first_name.' '.$st->middle_name.' '.$st->last_name}}
     </option>
       @endforeach
     </select>



     <a href="" style="color: red;">{{ $errors -> first('student_admission_id') }}</a>
   </div>
 </div>


   <div class="form-group">
    <div class="row">
     <label name="date" class="col-md-4" for="date">Date</label>

     <input type="text" id="date" name="date" class="form-control mydatepicker" placeholder="click Here" value="{{ $invoice->date }}">
     <a href="" style="color: red;">{{ $errors -> first('date') }}</a>

   </div>
 </div>

 <div class="form-actions">
  <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    </div>
</form>
</div>
</div>
</div>
</div>
@endsection


