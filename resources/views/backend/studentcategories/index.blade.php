@extends('backend.includes.app')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Academic</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Student Category</li>
                </ol>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body">
                    <div class="col-12">
                        <div class="card">
                            <h5 class="card-title">Student Discount Category</h5>
                            <div class="card-body">
                               <form method="post" action="{{ route('studentcategories.store')}}" enctype="multipart/form-data" class="from-horizontal">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="form-group">
                                        <label for="category">Category Title</label>
                                        <input type="text" id="category" name="category" class="form-control" placeholder="Enter category" value="{{old('category')}}">
                                        <a href="" style="color: red;">{{ $errors -> first('category') }}</a>
                                    </div>
                                    <div class="form-group">
                                        <label for="percent">Discount in Percentage (%)</label>
                                        <input type="text" id="percent" name="percent" class="form-control" placeholder="Enter discount in percent" value="{{old('percent')}}">
                                        <a href="" style="color: red;">{{ $errors -> first('percent') }}</a>
                                    </div>
                                    <div class="text-center">
                                    <button type="submit" class="btn btn-info btn-md waves-effect waves-light m-t-30">Add</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Nav tabs -->
                <!-- Tab panes -->
                <div class="card-body">
                    <h5 class="card-title">Category List</h5>
                    <div class="row">

                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Category</th>
                                    <th>Discount in Percent</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($student as $student)
                                    <tr>
                                        <td>{{$loop->index +1}}</td>
                                        <td> {{ $student->category }}</td>
                                        <td> {{ $student->percent }} %</td>

                                        <td>
                                           <a href="#" class="text-info p-r-10" title="Edit" data-toggle="modal" data-target=".bs-example-modal-lg{{$student->id}}"><i class="ti-marker-alt"></i></a>

                                             <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();"><i class="ti-trash"></i></a>
                                                      <form method="post" id="del" class="delete" action="{{ route('studentcategories.destroy',$student->id)}}">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        @method('DELETE')
                                                      </form>
                                        </td>
                                    </tr>       


                                    <div class="modal fade bs-example-modal-lg{{ $student->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myLargeModalLabel">Edit Student Category</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            
                                                            <form class="form-material" method="post" enctype="multipart/form-data" action="{{ route('studentcategories.edit', ['id' => $student->id]) }}">
                                                                @csrf
                                                                <p><strong>Category</strong></p>
                                                                <input type="text" id="category" name="category" class="form-control" value="{{$student->category}}">
                                                                <a href="" style="color: red;">{{ $errors -> first('title') }}</a>

                                                                <p><strong>Percentage</strong></p>
                                                                <input type="text" id="percent" name="percent" class="form-control" value="{{$student->percent}}">
                                                                <a href="" style="color: red;">{{ $errors -> first('title') }}</a>
                                                                    <div class="col-md-12 text-center">
                                                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-30 " style="text-align: center;">update</button>
</div>
                                                            </form>

                                                        </div>

                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection