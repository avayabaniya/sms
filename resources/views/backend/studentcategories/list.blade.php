
@extends('backend.includes.app')
@section('content')

    
             <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Student List</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active">View Students</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><a href="{{ route('studentadmissions.create')}}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Create New</a></button>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><a href="{{ route('guardian_index')}}" class="btn btn-info"><i class="fa fa-eye"> &nbsp</i>View Parent's List</a></button>
                        </div>
            </div>
        </div>
                 <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Student Information</h4>
                                <div class="table-responsive m-t-40">
                                    
                                <!-- <p><a href="{{ route('studentlists.create') }}" class="btn btn-primary">Add New Student</a></p> -->
                        
                          <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                             <tr>
                               <th>SN</th>
                               <th>Image</th>
                               <th>Student Name</th>
                               <th>Roll No</th>
                               <th>Registration Number</th>
                               <th>Temporary Address</th>
                               <th style="display:none;">Present Address</th>
                               <th>Class</th>
                               <th>Section</th>
                               <th style="display:none;">Parent's Name</th>
                               <th>Manage</th>             
                             </tr>
                            </thead>
                                        
                            <tbody>
                              @foreach($admission as $ad)
                              <tr>
                                  <td>{{ $loop->index + 1 }}</td>
                                  <td><img src="{{asset('public/backend/images/student/'.$ad->image)}}" alt="{{$ad->first_name}}" width="80"></td>
                                  <td>{{ $ad->first_name.' '.$ad->middle_name.' '.$ad->last_name }}</td>
                                  <td><?php if ($ad->rollno=='') {
                                     echo "Not Assigned";
                                  }else ?>{{ $ad->rollno }}</td>
                                  <td>{{ $ad->register_number}}</td>
                                  <td>{{ $ad->temporary_municipality }}</td>
                                  <td style="display:none;">{{ $ad->permanent_street }}</td>
                                  <td style="display:none;">{{ $ad->permanent_municipality }}</td>
                                  <td>{{ $ad->room['cname'] }}</td>
                                  <td>{{ $ad->section['sname'] }}</td>
                                  <td style="display:none;">{{ $ad->pname}}</td>
                                  <td>
                                    <a href="{{ route('studentadmissions.edit',$ad->id)}}" class="ti-pencil"></a>

                                    <a href="{{ route('studentadmissions.show',$ad->id)}}" class="ti-eye ti-eye-info" data-toggle="modal" data-target=".bs-example-modal-lg{{$ad->id}}" class="model_img img-responsive" ></a>

                                    <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
                                    <form method="post" id="del" class="delete" action="{{ route('studentadmissions.destroy',$ad->id)}}">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      @method('DELETE')
                                    </form>
                                  </td>
            
                              </tr>

                                        <div class="modal fade bs-example-modal-lg{{$ad->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myLargeModalLabel">Profile Details: {{ $ad->first_name.' '.$ad->middle_name.' '.$ad->last_name }}</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        
                                                        <div class="row text-center">
                                                            <div class="col-md-12 col-xs-12 b-r">
                                                                <center> <img src="{{asset('public/backend/images/student/'.$ad->image)}}" class="img-circle" width="180" height="180" /> </center>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Academic Year</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->academic_year}} </p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Admission Number</strong>
                                                                <p class="text-muted">
                                                                {{ $ad->admission_number }}
                                                                </p> 
                                                            </div>

                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>DOB</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->dob}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Gender</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->gender}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Phone Number</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->phone_no}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Blood Group</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->blood_group}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Admission Date</strong>
                                                                <br>
                                                                <p class="text-muted">{{ $ad->joining_date }}</p>
                                                            </div>
                                                            
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mother Tongue</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->mother_tongue}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Religion</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->religion}}</p>
                                                            </div>

                                                            <div class="col-md-3 col-xs-6"> <strong>Parent Name</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->pname}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Birth Place</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->birth_place}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Nationality</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->nationality}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Category</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->student['category']}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->mobile_no}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->email}}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row text-center">

                                                            <div class="col-md-3 col-xs-6"> <strong>Country</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->country}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Province</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->province}}</p>
                                                            </div>
                                                            
                                                            <div class="col-md-3 col-xs-6"> <strong>Municipality</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->temporary_municipality}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Street</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->temporary_street}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6"> <strong>Ward Number</strong>
                                                                <br>
                                                                <p class="text-muted">{{$ad->temporary_ward_number}}</p>
                                                            </div>
                                                            
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="{{ route('studentadmissions.edit',$ad->id)}}" class="btn btn-primary"> Edit</a>
                                                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                              @endforeach      
                            </tbody>
                          </table>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
        </div>
    </div>
                                  
                                
 @endsection
