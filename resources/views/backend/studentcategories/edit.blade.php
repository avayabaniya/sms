@extends('backend.includes.app')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Academic</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Student Category</li>
                </ol>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body">
                    <div class="col-12">
                        <div class="card">
                            <h5 class="card-title">Student Discount Category</h5>
                            <div class="card-body">
                                <form enctype="multipart/form-data" method="post" action="{{route('studentcategories.update',['id' => $student->id])}}">
                                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                                  @method('PUT')
                                    <div class="form-group">
                                        <label for="category">Category Title</label>
                                        <input type="text" id="category" name="category" class="form-control" placeholder="Enter category" value="{{$student->category}}">
                                        <a href="" style="color: red;">{{ $errors -> first('category') }}</a>
                                    </div>
                                    <div class="form-group">
                                        <label for="percent">Discount in Percentage (%)</label>
                                        <input type="text" id="percent" name="percent" class="form-control" placeholder="Enter discount in percent" value="{{ $student->percent}}">
                                        <a href="" style="color: red;">{{ $errors -> first('percent') }}</a>
                                    </div>

                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-30">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Nav tabs -->
                <!-- Tab panes -->
                <div class="card-body">
                    <h5 class="card-title">Category List</h5>
                    <div class="row">

                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Category</th>
                                    <th>Discount in Percent</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(<1students></1students> as $student)
                                    <tr>
                                        <td>{{$loop->index +1}}</td>
                                        <td> {{ $student->category }}</td>
                                        <td> {{ $student->percent }} %</td>

                                       
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection