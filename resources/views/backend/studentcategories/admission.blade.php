@extends('backend.includes.app')
@section('content')

<br>
<br>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-3">
          <div class="col-sm-4">
            <h1 class="m-0 text-dark"> Student Admission</h1>
          </div><!-- /.col -->
          <div class="col-sm-4">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
              <li class="breadcrumb-item">Student</li>
              <li class="breadcrumb-item active">Admission</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
   <!-- /.container-fluid -->
<div class="content">

<form method="post" action="{{ route('studentadmissions.store')}}" enctype="multipart/form-data" class="from-horizontal">
            <input type="hidden" name="_token" value="{{csrf_token()}}">                
                    <p><b>OFFICIAL DETAILS:-</b></p>
                    <hr>
                        <div class="row">
                            
                            <div class="form-group col-sm-4">
                                <label for="academic_year">Academic Year</label>
                                <select class="form-control" name="academic_year" id="academic_year">
                                  <option value="">Select Academic Year</option>
                                  <option value="1">2018 - 2019</option>
                                </select>
                              </div>
                                                           
                                <div class="form-group col-sm-4">
                                  <label for="register_number">Register Number</label>
                                  <input class="form-control" name="register_number" id="register_number" type="text" maxlength="45" /> 
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="joining_date">Joining Date</label>
                                  <input class="form-control" type="date" name="joining_date">
                                </div>
                            </div>
                        </div>

                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="course" class="req">Course</label>
                            <select class="form-control" name="course" id="Student_courseid">
                                <option value="">Select Course</option>
                                <option value="15">Schudele One1</option>
                            </select>
                        </div>

                                                    
                        <div class="form-group col-sm-4">
                            <label for="batch">Batch</label>
                              <select class="form-control" name="batch" id="batch">
                                  <option value="">Select Batch</option>
                              </select>
                              <div class="roll_no" id="roll_no" style="display:none"></div>
                        </div> 
                                                  
                              <div class="form-group col-sm-4">
                                  <label for="rollno">Roll No.</label>
                                  <input class="form-control" name="rollno" id="rollno" type="text" maxlength="60" />
                                                      
                              </div>
                        </div>
                        <p><b>PERSONAL DETAILS:-</b></p>
                    <hr>
                        <div class="row">
                            
                            <div class="form-group col-sm-4">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control" name="first_name">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="middle_name">Middle Name</label>
                                <input type="text" class="form-control" name="middle_name">
                              </div>

                              <div class="form-group col-sm-4">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" name="last_name">
                              </div>

                                <div class="form-group col-sm-4">
                                  <label for="dob">Date of Birth</label>
                                  <input class="form-control" type="date" name="dob">
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="gender">Gender</label>
                                  <select class="form-control" name="gender" id="gender">
                                    <option value="">Select Gender</option>
                                  <option value="male">male</option>
                                  <option value="female">female</option>
                                </select>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="blood_group">Blood Group</label>
                                  <select class="form-control" name="blood_group" id="blood_group">
                                    <option value="">Select Blood Group</option>
                                  <option value="A+">A Positive</option>
                                  <option value="A-">A negative</option>
                                  <option value="AB+">AB Positive</option>
                                  <option value="AB-">AB negative</option>
                                  <option value="B+">B Positive</option>
                                  <option value="B-">B negative</option>
                                  <option value="O+">O Positive</option>
                                  <option value="O-">O negative</option>
                                </select>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="birth_place">Birth Place</label>
                                  <input class="form-control" type="birth_place" name="birth_place">
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="nationality">Nationality</label>
                                  <select class="form-control" name="nationality" id="nationality">
                                    <option value="">Select Country</option>
                                    <option value="A">a</option>
                                   
                                  </select>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="mother_tongue">Mother Tongue</label>
                                  <input class="form-control" type="text" name="mother_tongue">
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="category">Category</label>
                                  <select class="form-control" name="category" id="category">
                                    <option value="">Select Category</option>
                                    <option value="A">a</option>
                                  </select>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="religion">Religion</label>
                                  <input class="form-control" type="text" name="religion">
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="caste">Caste</label>
                                  <input class="form-control" type="text" name="caste">
                                </div>
                            </div>

                        <p><b>CONTACT DETAILS:-</b></p>
                    <hr>
                        <div class="row">
                            
                              <div class="form-group col-sm-4">
                                  <label for="permanent_address">Permanent Address</label>
                                  <input type="text" class="form-control" name="permanent_address"> 
                              </div>
                                                           
                                <div class="form-group col-sm-4">
                                  <label for="present_address">Present Address</label>
                                  <input type="text" class="form-control" name="present_address"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="city">City</label>
                                  <input type="text" class="form-control" name="city"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="province">Province</label>
                                  <input type="text" class="form-control" name="province"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="country">Country</label>
                                  <input type="text" class="form-control" name="country"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="phone_no">Phone Number</label>
                                  <input type="text" class="form-control" name="phone_no"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="mobile_no">Mobile Number</label>
                                  <input type="text" class="form-control" name="mobile_no"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="email">Email</label>
                                  <input type="text" class="form-control" name="email"> 
                              </div>
                            </div>
                        

                         <p><b>PARENT'S DETAILS:-</b></p>
                    <hr>
                        <div class="row">
                            
                              <div class="form-group col-sm-4">
                                  <label for="pname">Name</label>
                                  <input type="text" class="form-control" name="pname"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="relation">Relation</label>
                                  <input type="text" class="form-control" name="relation"> 
                              </div>
                              <div class="form-group col-sm-4">
                                  <label for="education">Education</label>
                                  <input type="text" class="form-control" name="education"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="occupation">Occupation</label>
                                  <input type="text" class="form-control" name="occupation"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="income">Income</label>
                                  <input type="text" class="form-control" name="income"> 
                              </div>
                        </div>
                        
                              <div class="form-group col-sm-6">
                                  <label for="check">Check if parent Address Same as Student Address</label>
                                  <input type="checkbox" value=""> 
                              </div>


                        <div class="row">                           
                                <div class="form-group col-sm-6">
                                  <label for="address">Address</label>
                                  <input type="text" class="form-control" name="address"> 
                              </div>

                              <div class="form-group col-sm-6">
                                  <label for="pcity">City</label>
                                  <input type="text" class="form-control" name="city"> 
                              </div>

                              <div class="form-group col-sm-6">
                                  <label for="pprovince">Province</label>
                                  <input type="text" class="form-control" name="province"> 
                              </div>

                              <div class="form-group col-sm-6">
                                  <label for="pcountry">Country</label>
                                  <input type="text" class="form-control" name="country"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="pphone_no">Phone Number</label>
                                  <input type="text" class="form-control" name="phone_no"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="pmobile_no">Mobile Number</label>
                                  <input type="text" class="form-control" name="mobile_no"> 
                              </div>

                              <div class="form-group col-sm-4">
                                  <label for="pemail">Email</label>
                                  <input type="text" class="form-control" name="email"> 
                              </div>

                              <div class="col-md-12" >
                                    <div class="text-center">
                                      <input type="submit" value="save" class="btn btn-info btn-xs">
                                    </div>
                                  </div>

                            </div>
                        </div>


                        </div>
                     </div>
                </div>
  </form>                                    
                                
 @endsection