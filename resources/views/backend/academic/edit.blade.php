@extends('backend.includes.app')
@section('content')
<div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item ">Academic</li>
                                <li class="breadcrumb-item active">Class Teacher Allocation Update</li>

                            </ol>
                            
                        </div>
                    </div>
                </div>
 <div class="col-lg-12 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Class Teacher Allocation</p>

                        <form class="form-horizontal" method="post" action="{{ route('academicupdate',$academic->id)}}" enctype="multipart/form-data" >

                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                             @csrf
                             @method('PUT')

                            <div class="form-group">
                                <label name="room_id" for="room">Class</label>
                                <select  name="room_id" id="room_id" class="form-control custom-select class">
                                     @foreach($arr['rooms'] as $room)
                                     <option value="{{$room->id}}" @if($room->id == old('room_id') ) selected @endif >{{$room->cname}}</option>
                                    @endforeach
                             </select>
                                <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>
                             </div>
                             <div class="form-group">
                                <label name="Batch"  for="datepicker">Batch</label>
                                <input type="text"  id="datepickeracademic"  name="batch" class="form-control " placeholder="enter Batch">
                                <a href="" style="color: red;">{{ $errors -> first('batch') }}</a>
                             </div>
                             <div class="form-group">
                                <label name="section_id" for="section_id">Section</label>
                                <select  name="section_id" class="form-control custom-select section">
                                
                                    
                                </select>
                                <a href="" style="color: red;">{{ $errors -> first('section_id') }}</a>                                    

                             </div>
                             <div class="form-group">
                                <label name="employee_id" for="employee_id">Class Teacher</label>
                                <select  name="employee_id" class="form-control custom-select">
                                    @foreach($arr['employees'] as $emp)
                                    <option value="{{$emp->id}}"  @if($emp->id == old('employee_id') ) selected @endif >{{($emp->name)}} </option>
                                   @endforeach
                                </select>
                                <a href="" style="color: red;">{{ $errors -> first('employee_id') }}</a>                                   

                             </div>
                              <div class="form-actions">
                                <center>
                                 <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button> 
                                 </center>   
                              </div>
                              </form>

                            </div>
                            
                        </div>


                    </div>
@endsection
