@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item ">Academic</li>
                                <li class="breadcrumb-item active">Class Teacher Allocation</li>

                            </ol>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
            <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Class Teacher Allocation</p>
                        <form class="form-horizontal" method="post" action="{{route('academicsave')}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                             @csrf

                            <div class="form-group">
                                <label name="room_id" for="room">Class</label>
                                <select  name="room_id" id="room_id" class="form-control custom-select class">
                                    <option  value="">Choose Class</option>
                                     @foreach($arr['rooms'] as $room)
                                     <option value="{{$room->id}}"  @if($room->id == old('room_id') ) selected @endif >{{$room->cname}}</option>
                                    @endforeach
                             </select>
                                <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>
                             </div>
                             <div class="form-group">
                                <label name="Batch"  for="datepicker">Batch</label>
                                <input type="text" id="datepickeracademic" value="{{old('batch')}}" name="batch" class="form-control " placeholder="enter Batch">
                                <a href="" style="color: red;">{{ $errors -> first('batch') }}</a>
                             </div>
                             <div class="form-group">
                                <label name="section_id" for="section_id">Section</label>
                                <select  name="section_id" class="form-control custom-select section">
                                
                                    
                                </select>
                                <a href="" style="color: red;">{{ $errors -> first('section_id') }}</a>                                    

                             </div>
                             <div class="form-group">
                                <label name="employee_id" for="employee_id">Class Teacher</label>
                                <select  name="employee_id" class="form-control custom-select">
                                    <option  value=""> Chhose Class Teacher</option>
                                    @foreach($arr['employees'] as $emp)
                                    <option value="{{$emp->id}}"  @if($emp->id == old('employee_id') ) selected @endif >{{($emp->name)}} </option>
                                   @endforeach
                                </select>
                                <a href="" style="color: red;">{{ $errors -> first('employee_id') }}</a>                                   

                             </div>
                              <div class="form-actions">
                                 <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                              </form>
                            </div>
                            
                        </div>


                    </div>
                <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            
                            <div class="card-body">
                      
                           Class  Section and Class Teacher Display
                                     <div class="row">
                    <div class="col-12">
                       
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SN</th>
                                                <th>Class Teacher</th>
                                                <th>Class</th>
                                                <th>Section</th>
                                                <th>Batch</th>
                                                 <th>Action</th>
                                                
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($arr['academics'] as $academic)
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                <td>{{($academic->employee->name)}} </td>
                                                <td> {{ucfirst($academic->room->cname)}}</td>
                                                <td>{{ucfirst($academic->section->sname)}}</td>
                                                <td>{{($academic->batch)}}</td>
                                                <td>

      <!-- 'academicedit',['id'=>$academic->id]           -->
                        <!-- <a href="#" class="ti-pencil ti-pencil-info"></a>&nbsp; -->

                            <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
                              <form method="post" id="del" class="delete" action="{{ route('academicdelete',$academic->id)}}">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              </form>
            </td>
                             </tr>
            @endforeach
                                            
                                           
                                            
                                        </tbody>
                                    </table>
                         
                        </div>
                       
                        
                    </div>
                </div>


                                    </div>
                                </div>
                                
                    </div>
</div>

      






@endsection