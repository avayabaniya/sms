@extends('backend.includes.app')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Academic</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Designation</li>
                </ol>
                <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i>Add Designation</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">

                <div class="card-body">

                        <div class="col-12">
                            <div class="card">
                                <h5 class="card-title text-uppercase">Add Designation</h5>
                                <div class="card-body">
                                    <form class="form-material" method="post" enctype="multipart/form-data" action="{{ route('add.designation') }}">
                                        @csrf
                                        <input type="text" id="designation" name="designation" class="form-control" placeholder="enter designation" value="{{old('designation')}}">
                                        <a href="" style="color: red;">{{ $errors -> first('designation') }}</a>
                                        <br><br><br>
                                        <input type="text" id="salary" name="salary" class="form-control" placeholder="enter base salary" value="{{old('salary')}}">
                                        <a href="" style="color: red;">{{ $errors -> first('salary') }}</a>

                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-30">Add</button>

                                    </form>
                                </div>
                            </div>

                        </div>

                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Nav tabs -->

                <!-- Tab panes -->
                <div class="card-body">

                    <h5 class="card-title text-uppercase">Designation List</h5>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive m-t-40">
                                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Designation Title</th>
                                                <th>Basic Salary</th>
                                                <th>Total Salary</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($designations as $designation)
                                            <tr>
                                                <td>{{$loop->index +1}}</td>
                                                <td> <a style="color: black;" href="{{ route('view.selected.designation', ['id' => $designation->id]) }}">{{ $designation->name }}</a></td>
                                                <td>
                                                    {{ $designation->salary }}
                                                </td>
                                                <td>
                                                    {{ $designation->total }}
                                                </td>
                                                <td>
                                                    <a href="{{route('delete.designation', ['id' => $designation->id])}}" class="text-inverse p-r-10" title="Delete" data-toggle="tooltip" onclick="return confirm('Are u sure?')"><i class="ti-trash"></i></a>
                                                    <a href="#" class="text-info p-r-10" title="Edit Salaries" data-toggle="modal" data-target=".bs-example-modal-lg{{$designation->id}}"><i class="ti-marker-alt"></i></a>
                                                </td>
                                            </tr>
                                            <div class="modal fade bs-example-modal-lg{{ $designation->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myLargeModalLabel">Edit Designation</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <form class="form-material" method="post" enctype="multipart/form-data" action="{{ route('edit.designation', ['id' => $designation->id]) }}">
                                                                    @csrf
                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Base Salary</strong></p>
                                                                        <input type="text" id="salary" name="salary" class="form-control" value="{{$designation->salary}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('salary') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Grade</strong></p>
                                                                        <input type="text" id="grade" name="grade" class="form-control" value="{{$designation->grade}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('grade') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Allowance</strong></p>
                                                                        <input type="text" id="allowance" name="allowance" class="form-control" value="{{$designation->allowance}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('allowance') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Micro Finance</strong></p>
                                                                        <input type="text" id="micro_finance" name="micro_finance" class="form-control" value="{{$designation->micro_finance}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('micro_finance') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Local Allowance</strong></p>
                                                                        <input type="text" id="local_allowance" name="local_allowance" class="form-control" value="{{$designation->local_allowance}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('local_allowance') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Festival Allowance</strong></p>
                                                                        <input type="text" id="festival_allowance" name="festival_allowance" class="form-control" value="{{$designation->festival_allowance}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('festival_allowance') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Home Rent</strong></p>
                                                                        <input type="text" id="home_rent" name="home_rent" class="form-control" value="{{$designation->home_rent}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('home_rent') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Mahangi Allowance</strong></p>
                                                                        <input type="text" id="mahangi_allowance" name="mahangi_allowance" class="form-control" value="{{$designation->mahangi_allowance}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('mahangi_allowance') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Responsibility Allowance</strong></p>
                                                                        <input type="text" id="responsibility_allowance" name="responsibility_allowance" class="form-control" value="{{$designation->responsibility_allowance}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('responsibility_allowance') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <p><strong>Manager Allowance</strong></p>
                                                                        <input type="text" id="manager_allowance" name="manager_allowance" class="form-control" value="{{$designation->manager_allowance}}">
                                                                        <a href="" style="color: red;">{{ $errors -> first('manager_allowance') }}</a>
                                                                    </div>

                                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-30">Edit</button>
                                                                    </div>


                                                                </form>


                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection