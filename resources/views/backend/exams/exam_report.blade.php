@extends('backend.includes.app')

@section('content')
 
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Exam Schedule</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item ">Exam</li>
                                <li class="breadcrumb-item active">Exam Report</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>


     <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-material m-t-40" method="post" enctype="multipart/form-data" action="">
                            @csrf
                            <div class="row">
                             <div class="col-4 form-group">
                            <label class="control-label" for="exam_name">Academic Year</label>
                            <select  name="exam_name" class="form-control custom-select">
                               <option value="null">--Choose Academic Year--</option>
                               @foreach($arr['student_admissions'] as $student)
                                <option value="{{$student->id}}"  @if(old('student') == $student->id) selected @endif >{{($student->academic_year)}}</option>
                                @endforeach
                                
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('exam_name') }}</a>  
                       </div>
                            <div class="col-4 form-group">
                            <label class="control-label" for="exam_name">Exam Name</label>
                            <select  name="exam_name" class="form-control custom-select">
                               <option value="null">--Choose Exam Name--</option>
                               @foreach($arr['creates'] as $create)
                                <option value="{{$create->id}}"  @if(old('create') == $create->id) selected @endif >{{ucfirst(trans($create->exam_name))}}</option>
                                @endforeach
                                
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('exam_name') }}</a>  
                        </div>
                          
                                <div class="col-4 form-group">
                                <label class="control-label" for="cname">Class</label>
                            <select  name="cname" class="form-control custom-select">
                                 <option value="null">--Choose Class--</option>
                                 @foreach($arr['rooms'] as $room)
                                 <option value="{{$room->id}}"  @if(old('room') == $room->id) selected @endif  >{{ucfirst(trans($room->cname))}}</option>
                                 @endforeach
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>  
                        </div>
                                <br>
                                <br>
                                <div class="col-sm-12 " >
                                    <div class="text-center">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                                </div>
                            </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>




               



@endsection

