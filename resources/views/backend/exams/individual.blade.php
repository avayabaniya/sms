@extends('backend.includes.app')


@section('content')
 
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Mark </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active">Mark</li>
                            </ol>
                            
                            
                        </div>
                    </div>
                </div>
    @if($search == 0)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        <form class="form-material m-t-10" method="post" enctype="multipart/form-data" action="{{ route('individual')}}">
                            @csrf

                            <div class="row">
                        <!-- <div class="col-md-3">  
                            <label class="control-label" for="academic_year">Academic Year</label>
                        
                                <select  name="academic_year" class="form-control custom-select">
                                 <option value="">--Choose Year--</option>
                                 @foreach($arr['student_admissions'] as $student)                                 
                                 <option value="{{$student->academic_year}}"  @if(old('student') == $student->id) selected @endif  >{{($student->academic_year)}}</option>
                                 @endforeach
                             </select>
                                  <a href="" style="color: red;">{{ $errors -> first('academic_year') }}</a>  
                            
                                 </div> -->

                                 <div class="col-md-5">  
                            <label class="control-label" for="exam_name">Exam Name</label>

                                <select  name="exam_name" class="form-control custom-select">
                                 <option value="">--Choose Exam--</option>
                                 @foreach($arr['creates'] as $create)
                                 <option value="{{$create->id}}"  @if(old('create') == $create->id) selected @endif  >{{ucfirst($create->exam_name)}}</option>
                                 @endforeach
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('exam_name') }}</a>  
                                 </div>
                                 <div class="col-md-5">  
                            <label class="control-label" for="exam_name">Class</label>

                                <select  name="cname" class="form-control custom-select">
                                 <option value="">--Choose Class--</option>
                                 @foreach($arr['rooms'] as $room)
                                 <option value="{{$room->id}}"  @if(old('room') == $room->id) selected @endif  >{{ucfirst($room->cname)}}</option>
                                 @endforeach
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>  
                                 </div>
                                
                                  <div class="col-md-2">
                                    <br>
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                            </div>
                            </form>
                                </div>
                            </div>
                        </div>
                     </div>
    @endif
    @if($search == 1)
        <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                 ok xa ta 
                                   <form class="form-material m-t-10" method="post" enctype="multipart/form-data" action="{{ route('individual')}}">
                            @csrf

                            <div class="row">
                                
                                <!--  <div class="col-3">  
                                <select  name="academic_year" class="form-control custom-select">
                                <option value="">--Choose Year--</option>
                                @foreach($arr['student_admissions'] as $student)                                 
                                <option value="{{$student->academic_year}}" @if($chooseyear == $student->id) selected @endif  >{{($student->academic_year)}}</option>
                                @endforeach
                                                             </select>
                                 <a href="" style="color: red;">{{ $errors -> first('academic_year') }}</a>  
                                </div> -->

                                 <div class="col-md-5">  
                                <select  name="exam_name" class="form-control custom-select ">
                                 <option value="">--Choose Exam--</option>
                                 @foreach($arr['creates'] as $create)
                                 <option value="{{$create->id}}"  @if($choosecreate == $create->id) selected @endif  >{{ucfirst($create->exam_name)}}</option>
                                 @endforeach
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('exam_name') }}</a>  
                                 </div>
                                 <div class="col-md-5">  
                                <select  name="cname" class="form-control custom-select" >
                                 <option value="">--Choose Class--</option>
                                 @foreach($arr['rooms'] as $room)
                                 <option value="{{$room->id}}"  @if($chooseclass == $room->id) selected @endif  >{{ucfirst($room->cname)}}</option>
                                 @endforeach
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('cname') }}</a>  
                                 </div>
                                
                                  <div class="col-sm-2">
                                    <br>
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                            </div>
                            </form>

                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Date</th>
                                                <th>Exam</th>
                                                <th>Class</th>
                                                <th>Subject</th>
                                                <th>Time From</th>
                                                <th>Time To</th>
                                                <th>Full Mark</th>
                                                <th>Pass Mark</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($exams as $room)
                                         
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                <td>{{$room->date}}</td>
                                                <td>{{$room->create->exam_name}}</td>
                                                <td>{{$room->room->cname}}</td>
                                                <td>{{$room->subject_id}}</td>
                                                <td>{{$room->time_from}}</td>
                                                <td>{{$room->time_to}}</td>
                                                <td>{{$room->full_mark}}</td>
                                                <td>{{$room->pass_mark}}</td>
                                            </tr>
                                          @endforeach
                                        </tbody>
                                    </table>
                      		  </div>
                        </div>
                    </div>
                </div>
             </div>             
    @endif

 @endsection

