@extends('backend.includes.app')

@section('content')


  <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Exam Schedule Edit</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Exam Schedule Edit</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
 <div class="col-lg-12 col-xlg-9 col-md-7">
    <div class="card">
           
                <div class="row">
                        <div class="card-body"> 
                        <form class="form-horizontal" method="post" action="{{ route('examupdate',$exam->id)}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                        <div class="form-group">
                            <label class="control-label" for="exam_id">Exam Name</label>
                            <select  name="exam_id" class="select-exam-date form-control custom-select">
                               @foreach($arr['creates'] as $create)
                                <option  value="{{$create->id}}" @if($exam->exam_id == $create->id) selected @endif >{{ucfirst($create->exam_name)}}</option>
                                @endforeach
                                
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('exam_id') }}</a>  
                        </div>
                            
                             <div class="form-group">
                                <label class="control-label" for="room_id">Class</label>
                                 <select  name="room_id" class="form-control custom-select select-class-subject" id="select_subject">   
                                 @foreach($arr['rooms'] as $room)
                                 <option value="{{$room->id}}" @if($exam->room_id == $room->id) selected @endif >{{ucfirst($room->cname)}}</option>
                                 @endforeach
                             </select>
                            <a href="" style="color: red;">{{ $errors -> first('room_id') }}</a>  
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="subject_id">Subject</label>
                            <select  name="subject_id" class="form-control custom-select" id="select_class_subject">
                               <option value="null">--Choose Subject--</option>
                            </select>
                            <a href="" style="color: red;">{{ $errors -> first('subject_id') }}</a>  
                        </div>
                            <div class="form-group">
                            <label class="control-label" for="full_mark">Full Mark</label>
                             <input type="number" name="full_mark"  min="0" max="100"  class="form-control" id="full_mark" value="{{$exam->full_mark}}" ><br>
                            <a href="" style="color: red;">{{ $errors -> first('full_mark') }}</a>
                        </div>
                       
                        <div class="form-group">
                            <label class="control-label" for="pass_mark">Pass Mark</label>
                              <input type="number" name="pass_mark" min="0" max="100" class="form-control " id="pass_mark" value="{{$exam->pass_mark}}" ><br>
                            <a href="" style="color: red;">{{ $errors -> first('pass_mark') }}</a>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="date">Date</label>
                            <input type="text" name="date"  class="form-control mydatepicker1" id="examDate"  value="{{$exam->date}}"><br>
                            <a href="" style="color: red;">{{ $errors -> first('date') }}</a>  
                        </div>
                            <div class="form-group">
                            <label class="control-label" for="time_from">Time From</label>
                                 <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                     <input type="text" class="form-control" name="time_from" value="{{$exam->time_from}}"><br>
                                      <a href="" style="color: red;">{{ $errors -> first('time_from') }}</a>
                                      <div class="input-group-append">
                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                            </div>
                                        </div>
                                </div>

                              <div class="form-group">
                            <label class="control-label" for="time_to">Time To</label>
                                 <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                     <input type="text" class="form-control" name="time_to" value="{{$exam->time_to}}"><br>
                                      <a href="" style="color: red;">{{ $errors -> first('time_to') }}</a>
                                      <div class="input-group-append">
                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                            </div>
                                        </div>
                                </div>  
                        
                         <div class="form-actions">
                        <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update Exam Schedule</button>    
                        </div>
                           
                    </form>
                    </div>                    
             </div>
</div>
</div>
@endsection