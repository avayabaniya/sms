@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Academic</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item ">Exam</li>
                                <li class="breadcrumb-item active">Create Exam</li>
                            </ol>
                           
                        </div>
                    </div>
                </div>
                <div class="row">
            <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body"> 
                               <p>Create Exam</p>
                        <form class="form-horizontal" method="post" action="{{ route('createsave')}}" enctype="multipart/form-data" >
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf
                         
                        <div class="form-group">
                            <label class="control-label" for="exam_name">Exam Name</label>
                          <input type="text" id="exam_name" name="exam_name" value="{{old('exam_name')}}" class="form-control"  placeholder="enter exam name">
                            <a href="" style="color: red;">{{ $errors -> first('exam_name') }}</a>             
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="exam_name">Exam Date</label>
                            <div class="input-daterange input-group" id="date-range">
                                <input type="text" class="form-control" value="{{old('start')}}" name="start" placeholder="start date" />

                                <div class="input-group-append">
                                    <span class="input-group-text bg-info b-0 text-white">TO</span>
                                </div>
                                <input type="text" class="form-control" value="{{old('end')}}" name="end" placeholder="end date" />
                                 <a href="" style="color: red;">{{ $errors -> first('start') }}</a>  
                                 <a href="" style="color: red;">{{ $errors -> first('end') }}</a>  
                            </div>
                         </div>
                            <div class="form-actions">
                                 <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Save</button>    
                              </div>
                              </form>
                            </div>
                            
                        </div>


                    </div>
                <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
            <div class="card-body">
                      Create Exam Details
                <div class="row">
                       
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Exam Name</th>
                                                <th>Start Date </th>
                                                <th>End Date </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           @foreach($arr['create'] as $cre)
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                <td>{{ucfirst($cre->exam_name)}}</td>
                                                <td>{{($cre->start)}}</td>
                                                <td>{{($cre->end)}}</td>

                                                <td>

                        <a href="{{ route('createupdate',$cre->id)}}" class="ti-pencil ti-pencil-info" data-toggle="modal" data-target=".bs-example-modal-lg1{{$cre->id}}" class="model_img img-responsive"></a>&nbsp;

                        

              <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" class="ti-trash"></a>
              <form method="post" id="del" class="delete" action="{{route('createdelete',$cre->id)}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @method('DELETE')
              </form>
          </td>
                        </tr>  
                            <div class="modal fade bs-example-modal-lg1{{$cre->id}}"tabindex="-1" role="dialog" aria-labelledby="       
                                    myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Exam Edit </h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" method="post" action="{{ route('createupdate',$cre->id)}}" enctype="multipart/form-data">
                                                 <input type="hidden" name="_token" value="{{csrf_token()}}">
                        						            @csrf

                                                    <div class="row">
                                                        <div class="col-4 "> 
                                                     <label class="control-label"for="term_name">Exam Name</label>
                                                            <br>
                                                            <input type="text" name="exam_name" class="form-control" value="{{ucfirst($cre->exam_name)}}">
                                                        </div>
                                                      <div class="form-group">
                                                  <label class="control-label" for="exam_name">Exam Date</label>
                                                  <div class="input-daterange input-group" id="date-range1">
                                                      <input type="text" class="form-control" value="{{$cre->start}}" name="start" placeholder="start date" />
                                                      <div class="input-group-append">
                                                          <span class="input-group-text bg-info b-0 text-white">TO</span>
                                                      </div>
                                                      <input type="text" class="form-control" value="{{$cre->end}}" name="end" placeholder="end date" />
                                                  </div>
                                                  </div>
                                                    </div>


                                                <div class="modal-footer">
                                                 <div class="form-actions">
                                                <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Update</button>
                                                </div>
                                                <button type="button" class="btn btn-danger btn-sm waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                             </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>

                           
                                            @endforeach
                                          
                                               
                                        </tbody>
                                    </table>
                   </div>
               </div>
           </div>
     

                    </div>
                </div>
             
    </div>
    
      






@endsection