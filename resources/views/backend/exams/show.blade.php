@extends('backend.includes.app')

@section('content')
 
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Exam Schedule</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                                <li class="breadcrumb-item ">Exam</li>
                                <li class="breadcrumb-item active">Exam Schedule</li>
                            </ol>
                           
                            
                        </div>
                    </div>
                </div>
<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                  <div class="row">  
                            <div class="col-md-8 align-self-center">
                              
                            </div>
                             <div class="col-md-2 align-self-center">
                                <a href="{{ route('exam')}}">
                                 <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i>  Exam Schedule</button></a>
                            </div>
                            <div class="col-md-2 align-self-center">
                                <a href="{{ route('create')}}">
                                 <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i>  Exam Create</button></a>
                            </div>
                             </div>
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Date</th>
                                            <th>Exam</th>
                                            <th>Class</th>
                                            <th>Subject</th>
                                            <th>Time From</th>
                                            <th>Time To</th>
                                             <th>Full Mark</th>
                                            <th>Pass Mark</th>
                                            <th>Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($arr['exams'] as $exam)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{($exam->date)}}</td>
                                            <td>{{ucfirst($exam->create->exam_name)}}</td>
                                            <td>{{ucfirst($exam->room->cname)}}</td>
                                            <td>{{ucfirst($exam->subject_id)}}</td>
                                            <td>{{($exam->time_from)}}</td>
                                            <td>{{($exam->time_to)}}</td>
                                            <td>{{($exam->full_mark)}}</td>
                                            <td>{{($exam->pass_mark)}}</td>
                                            <td>
                                                <a href="{{ route('examedit',$exam->id)}}" class="text-success p-r-10" data-toggle="tooltip" title="Edit"><i class="ti-marker-alt"></i></a>
                                             
                                               <a href="javascript:void(0)" onclick="$(this).parent().find('#del').submit();" data-toggle="tooltip" title="Delete" class="ti-trash"></a>
                                                  <form method="post" id="del" class="delete" action="{{route('examdelete',$exam->id)}}">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    @method('DELETE')
                                                  </form>
                                            </td>
                                        </tr>
                                     @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




 @endsection
