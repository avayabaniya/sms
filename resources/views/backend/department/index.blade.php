@extends('backend.includes.app')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Academic</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Depatment</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">

                <div class="card-body">

                        <div class="col-12">
                            <div class="card">
                                <h5 class="card-title text-uppercase">Add Department</h5>
                                <div class="card-body">
                                    <form class="form-material" method="post" enctype="multipart/form-data" action="{{ route('add.department') }}">
                                        @csrf
                                        <input type="text" id="department" name="department" class="form-control" placeholder="enter department" value="{{old('department')}}">
                                        <a href="" style="color: red;">{{ $errors -> first('department') }}</a>

                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-30">Add</button>

                                    </form>
                                </div>
                            </div>


                        </div>

                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Nav tabs -->

                <!-- Tab panes -->
                <div class="card-body">

                    <h5 class="card-title text-uppercase">Department List</h5>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive m-t-40">
                                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Department Title</th>
                                                <th>Department Head</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($departments as $department)
                                            <tr>
                                                <td>{{$loop->index +1}}</td>
                                                <td><a style="color: black;" href="{{ route('view.selected.department', ['id' => $department->id]) }}"> {{ $department->name }}</a></td>
                                                <td>
                                                    <div class="col-sm-12">
                                                        <form id="store" name="store" method="post" action="{{ route('editHead.department', ['id'=>$department->id]) }}">
                                                            {{csrf_field()}}
                                                            <select class="form-control" name="department_head" id="department_head" onchange="this.form.submit()">
                                                                <option value=>--Select Head--</option>
                                                                @foreach($departmentHeads as $head)
                                                                    {{--@if($head->department_id == $department->id)--}}
                                                                    <option value="{{ $head->id }}"  @if($department->department_head == $head->id) selected @endif>{{ $head->name }}</option>
                                                                    {{--@endif--}}
                                                                @endforeach
                                                            </select>
                                                        </form>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="{{route('delete.department', ['id' => $department->id])}}" class="text-inverse p-r-10" title="Delete" data-toggle="tooltip" onclick="return confirm('Are u sure?')"><i class="ti-trash"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection