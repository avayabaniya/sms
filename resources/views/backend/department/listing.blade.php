@extends('backend.includes.app')
@section('content')

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">{{ $departmentDetail -> name }}  Employees</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">{{ $departmentDetail -> name }} Department</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        Department Head:
                        <?php
                        foreach ($departmentDetail->employee as $employee){
                            if ($departmentDetail->department_head == $employee->id){
                                echo $employee->name;
                            }
                        }
                        ?>
                    </h4>
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>DOB</th>
                                <th>Start date</th>
                                <th>Mobile</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($departmentDetail->employee as $employee)
                                <tr>
                                    <td>{{$loop->index +1}}</td>
                                    <td><img src="{{asset('public/backend/images/employee/'.$employee->employeeDetails->image)}}" alt="{{$employee->name}}" width="80"></td>
                                    <td>{{$employee->name}}</td>
                                    <td>{{$employee->employeeDetails->dob}}</td>
                                    <td>{{$employee->employeeDetails->join_date}}</td>
                                    <td>{{$employee->employeeDetails->mobile}}</td>
                                    <td>
                                        <a href="{{route('edit.employee', ['id' => $employee->id])}}" class="text-success p-r-10" data-toggle="tooltip" title="Edit"><i class="ti-marker-alt"></i></a><a href="#" class="text-info p-r-10" title="View" data-toggle="modal" data-target=".bs-example-modal-lg{{$employee->id}}"><i class="ti-eye"></i></a>
                                        <a href="{{route('payroll.employee', ['code' => $employee->employee_code])}}" class="text-success p-r-10" data-toggle="tooltip" title="Salary"><i class="fa fa-money"></i></a>
                                        @if(!empty($employee->employeeDetails->resume))
                                            <a href="{{asset('public/backend/documents/'.$employee->employeeDetails->resume)}}" target="_blank" class="text-inverse p-r-10" data-toggle="tooltip" title="View Resume"><i class="icon-doc"></i></a>
                                        @endif
                                        <a href="{{route('delete.employee', ['id' => $employee->id])}}" class="text-inverse p-r-10" title="Delete" data-toggle="tooltip"><i class="ti-trash" onclick="return confirm('Are u sure?')"></i></a>

                                    </td>
                                </tr>

                                <div class="modal fade bs-example-modal-lg{{$employee->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Profile Details: {{$employee->name}}</h4>br
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <center class="m-t-30"> <img src="{{asset('public/backend/images/employee/'.$employee->employeeDetails->image)}}" class="img-circle" width="270" height="250" />
                                                            <h4 class="card-title m-t-10">{{$employee->name}}</h4>
                                                            @if(!empty($employee->designation['name']))
                                                                <h6 class="card-subtitle">{{$employee->designation['name']}} | Rs. {{ $employee->employeeSalary['total'] }}</h6>
                                                            @endif
                                                            @if($employee->employee_type == 1)
                                                                <h6 class="card-subtitle">Employee Type: Teacher</h6>
                                                            @else
                                                                <h6 class="card-subtitle">Employee Type: Non-Teacher</h6>
                                                            @endif
                                                        </center>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p class="m-t-30">{{$employee->employeeDetails->description}}</p>
                                                        <hr>
                                                        @if(empty($employee->employeeDetails->instagram) && empty($employee->employeeDetails->facebook)
                                                            && empty($employee->employeeDetails->linkedin) && empty($employee->employeeDetails->twitter) )
                                                            <small class="text-muted p-t-30 db">Social Profile not added</small>
                                                        @else
                                                            <small class="text-muted p-t-30 db">Social Profile</small>
                                                        @endif
                                                        <br/>
                                                        @if(!empty($employee->employeeDetails->facebook))
                                                            <a class="btn btn-circle btn-secondary" href="{{$employee->employeeDetails->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
                                                        @endif
                                                        @if(!empty($employee->employeeDetails->linkedin))
                                                            <a class="btn btn-circle btn-secondary" href="{{$employee->employeeDetails->linkedin}}" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                        @endif
                                                        @if(!empty($employee->employeeDetails->twitter))
                                                            <a class="btn btn-circle btn-secondary" href="{{$employee->employeeDetails->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
                                                        @endif
                                                        @if(!empty($employee->employeeDetails->instagram))
                                                            <a class="btn btn-circle btn-secondary" href="{{$employee->employeeDetails->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a>
                                                        @endif



                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                                        <br>
                                                        <p class="text-muted">{{$employee->name}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                                        <br>
                                                        <p class="text-muted">{{$employee->employeeDetails->mobile}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                                        <br>
                                                        <p class="text-muted">{{$employee->employeeDetails->email}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                                                        <br>
                                                        <p class="text-muted">{{$employee->employeeDetails->country}}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>DOB</strong>
                                                        <br>
                                                        <p class="text-muted">{{$employee->employeeDetails->dob}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Gender</strong>
                                                        <br>
                                                        <p class="text-muted">{{$employee->gender}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Blood Group</strong>
                                                        <br>
                                                        <p class="text-muted">{{$employee->employeeDetails->blood_group}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6"> <strong>Address</strong>
                                                        <br>
                                                        <p class="text-muted">{{$employee->employeeDetails->address}}</p>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <a href="{{route('edit.employee', ['id' => $employee->id])}}" class="btn btn-info waves-effect">Edit</a>
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->



@endsection