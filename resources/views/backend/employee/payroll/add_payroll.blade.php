@extends('backend.includes.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">{{ $employee->name }}'s Payroll</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Add Payroll</li>
                </ol>
               <a style="margin-left: 5px;" href="{{ route('same.payroll', $employee->id )}}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add Payroll same as previous Month</a>

            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <form class="m-t-40" method="post" enctype="multipart/form-data" action="{{ route('add.payroll', ['id' => $employee->id]) }}">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Salary and Allowance (in Nepali Rupees)</h5>

                        <div class="form-group">
                            <div class="row">
                                <input type="hidden" name="e_id" value="{{ $employee->id }}" id="emp_payroll_id">
                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Base Salary</strong></p>
                                    <input type="text" id="salary" name="salary" class="form-control" value="{{$employee->EmployeeSalary->salary}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('salary') }}</a>
                                </div>

                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Grade</strong></p>
                                    <input type="text" id="grade" name="grade" class="form-control" value="{{$employee->EmployeeSalary->grade}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('grade') }}</a>
                                </div>

                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Allowance</strong></p>
                                    <input type="text" id="allowance" name="allowance" class="form-control" value="{{$employee->EmployeeSalary->allowance}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('allowance') }}</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Micro Finance</strong></p>
                                    <input type="text" id="micro_finance" name="micro_finance" class="form-control" value="{{$employee->EmployeeSalary->micro_finance}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('micro_finance') }}</a>
                                </div>

                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Local Allowance</strong></p>
                                    <input type="text" id="local_allowance" name="local_allowance" class="form-control" value="{{$employee->EmployeeSalary->local_allowance}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('local_allowance') }}</a>
                                </div>

                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Festival Allowance</strong></p>
                                    <input type="text" id="festival_allowance" name="festival_allowance" class="form-control" value="{{$employee->EmployeeSalary->festival_allowance}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('festival_allowance') }}</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Home Rent</strong></p>
                                    <input type="text" id="home_rent" name="home_rent" class="form-control" value="{{$employee->EmployeeSalary->home_rent}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('home_rent') }}</a>
                                </div>

                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Mahangi Allowance</strong></p>
                                    <input type="text" id="mahangi_allowance" name="mahangi_allowance" class="form-control" value="{{$employee->EmployeeSalary->mahangi_allowance}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('mahangi_allowance') }}</a>
                                </div>

                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Responsibility Allowance</strong></p>
                                    <input type="text" id="responsibility_allowance" name="responsibility_allowance" class="form-control" value="{{$employee->EmployeeSalary->responsibility_allowance}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('responsibility_allowance') }}</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Manager Allowance</strong></p>
                                    <input type="text" id="manager_allowance" name="manager_allowance" class="form-control" value="{{$employee->EmployeeSalary->manager_allowance}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('manager_allowance') }}</a>
                                </div>

                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Total</strong></p>
                                    <input type="text" id="total" name="total" class="form-control" value="{{$employee->EmployeeSalary->total}}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('total') }}</a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Deductions (in Nepali Rupees)</h5>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 10px;">
                                    <p><strong>P/F Fund</strong></p>
                                    <input type="text" id="pf_fund" name="pf_fund" class="form-control payrollCalculation">
                                    <a href="" style="color: red;">{{ $errors -> first('pf_fund') }}</a>
                                </div>

                                <div class="col-md-6" style="margin-bottom: 10px;">
                                    <p><strong>Retirement Fund</strong></p>
                                    <input type="text" id="retirement_fund" name="retirement_fund" class="form-control payrollCalculation">
                                    <a href="" style="color: red;">{{ $errors -> first('retirement_fund') }}</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 10px;">
                                    <p><strong>Security Tax</strong></p>
                                    <input type="text" id="security_tax" name="security_tax" class="form-control payrollCalculation">
                                    <a href="" style="color: red;">{{ $errors -> first('security_tax') }}</a>
                                </div>

                                <div class="col-md-6" style="margin-bottom: 10px;">
                                    <p><strong>Salary Tax</strong></p>
                                    <input type="text" id="salary_tax" name="salary_tax" class="form-control payrollCalculation" value={{ $salaryTax }}>
                                    <a href="" style="color: red;">{{ $errors -> first('salary_tax') }}</a>
                                </div>

                                <input type="hidden" id="total_deduction" name="total_deduction">


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Additions (in Nepali Rupees)</h5>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Over Time per hour pay</strong></p>
                                    <input type="text" id="ot_money_hr" name="ot_money_hr" class="form-control payrollCalculation">
                                    <a href="" style="color: red;">{{ $errors -> first('ot_money_hr') }}</a>
                                </div>

                                <div class="col-md-4 payrollCalculation" style="margin-bottom: 10px;">
                                    <p><strong>Hours Worked</strong></p>
                                    <input type="text" id="ot_hr" name="ot_hr" class="form-control payrollCalculation">
                                    <a href="" style="color: red;">{{ $errors -> first('ot_hr') }}</a>
                                </div>

                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <p><strong>Total OT</strong></p>
                                    <input type="text" id="ot" name="ot" class="form-control" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('ot') }}</a>
                                </div>

                                <input type="hidden" id="total_addition" name="total_addition">

                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Total</h5>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 10px;">
                                    <p><strong>Total Payroll</strong></p>
                                    <?php
                                        $first_total_payroll = $employee->designation->total - $salaryTax;
                                    ?>
                                    <input type="text" id="total_payroll" name="total_payroll" class="form-control" value="{{ $first_total_payroll }}" readonly>
                                    <a href="" style="color: red;">{{ $errors -> first('total_payroll') }}</a>
                                </div>

                                <div class="col-md-6" style="margin-bottom: 10px;">
                                    <p><strong>Select Month</strong></p>
                                    <input type="text" id="monthpicker" name="payroll_month" class="form-control" placeholder="Enter Payroll Month" value="{{ old('payroll_month') }}">
                                    <a href="" style="color: red;">{{ $errors -> first('payroll_month') }}</a>
                                </div>

                                <div class="col-md-6" style="margin-bottom: 10px;">
                                    <button type="submit" id="update_payroll" class="btn btn-info waves-effect waves-light m-r-10">Update</button>
                                    <strong><p id="empty_payroll" style="color: red;"></p></strong>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>



@endsection
