@extends('backend.includes.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Salary Details of: {{ $employee->name }}</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Employee Payroll</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="form-material" id="salary_form" method="post" enctype="multipart/form-data" action="{{ route('add.salary', ['code' => $employee->employee_code]) }}">
                        @csrf
                        <div class="row">
                        <div class="col-md-1" >
                            <a  href="{{route('add.payroll', ['id' => $employee->id])}}"><button style="display: inline-block;" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add Payroll</button></a>

                        </div>
                            <div class="col-md-1" >
                                <a style="margin-left: 35px;" href="{{ route('same.payroll', $employee->id )}}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add Payroll same as previous Month</a>
                            </div>


                        </div>
                    <div class="table-responsive m-t-20">

                        <table class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Salary/Allowance Heading</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{1}}</td>
                                    <td>Basic Salary</td>
                                    <td>
                                        <input type="text" id="salary"  name="salary" class="form-control" value="{{$employee->employeeSalary->salary}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('salary') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{2}}</td>
                                    <td>Grade</td>
                                    <td>
                                        <input type="text" id="grade" name="grade" class="form-control" value="{{$employee->employeeSalary->grade}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('grade') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{3}}</td>
                                    <td>Allowance</td>
                                    <td>
                                        <input type="text" id="allowance" name="allowance" class="form-control" value="{{$employee->employeeSalary->allowance}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('allowance') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{4}}</td>
                                    <td>Micro Finance</td>
                                    <td>
                                        <input type="text" id="micro_finance" name="micro_finance" class="form-control" value="{{$employee->employeeSalary->micro_finance}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('micro_finance') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{5}}</td>
                                    <td>Local Allowance</td>
                                    <td>
                                        <input type="text" id="local_allowance" name="local_allowance" class="form-control" value="{{$employee->employeeSalary->local_allowance}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('local_allowance') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{6}}</td>
                                    <td>Festival Allowance</td>
                                    <td>
                                        <input type="text" id="festival_allowance" name="festival_allowance" class="form-control" value="{{$employee->employeeSalary->festival_allowance}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('festival_allowance') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{7}}</td>
                                    <td>Home Rent</td>
                                    <td>
                                        <input type="text" id="home_rent" name="home_rent" class="form-control" value="{{$employee->employeeSalary->home_rent}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('home_rent') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{8}}</td>
                                    <td>Mahangi Allowance</td>
                                    <td>
                                        <input type="text" id="mahangi_allowance" name="mahangi_allowance" class="form-control" value="{{$employee->employeeSalary->mahangi_allowance}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('festival_allowance') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{9}}</td>
                                    <td>Responsibility Allowance</td>
                                    <td>
                                        <input type="text" id="responsibility_allowance" name="responsibility_allowance" class="form-control" value="{{$employee->employeeSalary->responsibility_allowance}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('responsibility_allowance') }}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{10}}</td>
                                    <td>Manager Allowance</td>
                                    <td>
                                        <input type="text" id="manager_allowance" name="manager_allowance" class="form-control" value="{{$employee->employeeSalary->manager_allowance}}" onkeyup="changeSalaryTotal()" style="display: block;" autocomplete="off">
                                        <a href="" style="color: red;">{{ $errors -> first('manager_allowance') }}</a>
                                    </td>
                                </tr>

                                <tfoot>
                                    <tr>
                                        <th>Total</th>
                                        <th></th>
                                        <th><p id="salary_total">{{$employee->employeeSalary->total}}</p></th>
                                    </tr>
                                </tfoot>
                            </tbody>
                        </table>

                        <div class="col-md-2" style="float: right;">
                            <button style="display: inline-block;" type="submit" class="btn btn-primary d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Change Salary</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
