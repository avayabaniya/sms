@extends('backend.includes.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Department Annual Payroll</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Department Payroll</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if($search == 0)
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-material m-t-40" method="post" enctype="multipart/form-data" action="{{ route('view.annual.payroll')}}">
                            @csrf
                            <div class="row">
                                <label class="col-sm-2" for="department">Department</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="department" id="department">
                                        <option value=>--Select Department--</option>
                                        @foreach($departments as $department)
                                            <option value="{{ $department->id }}" @if(old('department') == $department->id) selected @endif>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                    <a href="" style="color: red;">{{ $errors -> first('department') }}</a>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <label class="col-md-2" for="designation">Year</span>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text" id="datepicker" name="payroll_year" class="form-control" placeholder="Enter Payroll Year" value="{{ old('payroll_year') }}" >
                                    <a href="" style="color: red;">{{ $errors -> first('payroll_year') }}</a>
                                </div>

                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($search == 1)

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-material m-t-40" method="post" enctype="multipart/form-data" action="{{ route('view.annual.payroll')}}">
                            @csrf
                            <div class="row">
                                <label class="col-sm-2" for="department">Department</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="department" id="department">
                                        <option value=>--Select Department--</option>
                                        @foreach($departments as $department)
                                            <option value="{{ $department->id }}" @if($selectedDepartment == $department->id) selected @endif>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                    <a href="" style="color: red;">{{ $errors -> first('department') }}</a>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <label class="col-md-2" for="designation">Year</span>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text" id="datepicker" name="payroll_year" class="form-control" placeholder="Enter Payroll Year" value="{{ $selectedYear }}" >
                                    <a href="" style="color: red;">{{ $errors -> first('payroll_year') }}</a>
                                </div>

                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-20">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Image</th>
                                    <th>Employee</th>
                                    <th>Month</th>
                                    <th>Basic Salary</th>
                                    <th>Total Allowance</th>
                                    <th>Total Deduction</th>
                                    <th>Total Overtime</th>
                                    <th>Final Salary</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($finalPayroll as $key => $payroll)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td><img src="{{asset('public/backend/images/employee/'.$payroll->employee->employeeDetails->image)}}" alt="{{$payroll->employee->name}}" width="80"></td>
                                        <td>{{$payroll->employee->name}}</td>
                                        <td>
                                            <?php
                                            $date = explode("-",$payroll->month_id);
                                            ?>
                                            @foreach($months as $month)
                                                @if($date[0] == $month->id)
                                                    {{ $month->english_month }}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>Rs. {{$payroll->employee->designation->salary}}</td>
                                        <td>
                                            Rs. {{$payroll->employee->designation->grade +
                                            $payroll->employee->designation->allowance +
                                            $payroll->employee->designation->micro_finance +
                                            $payroll->employee->designation->local_allowance +
                                            $payroll->employee->designation->festival_allowance+
                                            $payroll->employee->designation->home_rent+
                                            $payroll->employee->designation->mahangi_allowance+
                                            $payroll->employee->designation->responsibility_allowance+
                                            $payroll->employee->designation->manager_allowance}}
                                        </td>
                                        <td>
                                            Rs. {{$payroll->salaryDeduction->pf_fund +
                                            $payroll->salaryDeduction->retirement_fund +
                                            $payroll->salaryDeduction->salary_tax +
                                            $payroll->salaryDeduction->security_tax}}

                                        </td>
                                        <td>Rs. {{$payroll->salaryAddition->over_time_rate * $payroll->salaryAddition->over_time_hour_worked}}</td>
                                        <td>Rs. {{$payroll->salary_per_month}}</td>
                                        <td>
                                            <a href="{{route('payroll.employee', ['id' => $payroll->employee->id])}}" class="text-success p-r-10" data-toggle="tooltip" title="Salary"><i class="fa fa-money"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Annual Total</th>
                                        <th></th>
                                        <th></th>
                                        <th>Year: {{ $selectedYear }}</th>
                                        <th>Rs. {{ $totalBasicSalary }}</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Rs. {{ $totalFinalSalary }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection
