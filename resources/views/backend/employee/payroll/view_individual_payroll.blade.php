@extends('backend.includes.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Individual Payroll</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Individual Payroll</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    @if($search == 0)
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-material m-t-10" method="post" enctype="multipart/form-data" action="{{ route('view.individual.payroll')}}">
                            @csrf
                            <div class="row">
                                <label class="col-sm-2" for="employee">Employee</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="employee" id="employee">
                                        <option value=>--Select Employee--</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}" @if(old('employee') == $employee->id) selected @endif>{{ $employee->name }}</option>
                                        @endforeach
                                    </select>
                                    <a href="" style="color: red;">{{ $errors -> first('employee') }}</a>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($search == 1)

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-material m-t-10" method="post" enctype="multipart/form-data" action="{{ route('view.individual.payroll')}}">
                            @csrf
                            <div class="row">
                                <label class="col-sm-2" for="employee">Employee</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="employee" id="employee">
                                        <option value=>--Select Employee--</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}" @if($selectedEmployee == $employee->id) selected @endif>{{ $employee->name }}</option>
                                        @endforeach
                                    </select>
                                    <a href="" style="color: red;">{{ $errors -> first('employee') }}</a>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive m-t-20">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Month</th>
                                <th>Year</th>
                                <th>Employee</th>
                                <th>Basic Salary</th>
                                <th>Total Allowance</th>
                                <th>Total Deduction</th>
                                <th>Total Overtime</th>
                                <th>Total Salary</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payrolls as $payroll)
                                <tr>
                                    <?php
                                        $date = explode("-",$payroll->month_id);
                                    ?>
                                    <td>{{ $loop->index + 1 }}</td>
                                        <td>
                                            @foreach($months as $month)
                                                @if($date[0] == $month->id)
                                                    {{ $month->english_month }}
                                                @endif
                                            @endforeach
                                        </td>
                                    <td>{{ $date[1] }}</td>
                                    <td>{{$payroll->employee->name}}</td>
                                    <td>Rs. {{$payroll->employee->designation->salary}}</td>
                                    <td>
                                        Rs. {{$payroll->employee->designation->grade +
                                            $payroll->employee->designation->allowance +
                                            $payroll->employee->designation->micro_finance +
                                            $payroll->employee->designation->local_allowance +
                                            $payroll->employee->designation->festival_allowance+
                                            $payroll->employee->designation->home_rent+
                                            $payroll->employee->designation->mahangi_allowance+
                                            $payroll->employee->designation->responsibility_allowance+
                                            $payroll->employee->designation->manager_allowance}}
                                    </td>
                                    <td>
                                        Rs. {{$payroll->salaryDeduction->pf_fund +
                                            $payroll->salaryDeduction->retirement_fund +
                                            $payroll->salaryDeduction->salary_tax +
                                            $payroll->salaryDeduction->security_tax}}

                                    </td>
                                    <td>Rs. {{$payroll->salaryAddition->over_time_rate * $payroll->salaryAddition->over_time_hour_worked}}</td>
                                    <td>Rs. {{$payroll->salary_per_month}}</td>
                                    <td>
                                        <a href="{{route('edit.employee', ['id' => $employee->id])}}" class="text-success p-r-10" data-toggle="tooltip" title="Edit"><i class="ti-marker-alt"></i></a><a href="#" class="text-info p-r-10" title="View" data-toggle="modal" data-target=".bs-example-modal-lg{{$employee->id}}"><i class="ti-eye"></i></a>
                                        <a href="{{route('payroll.employee', ['code' => $employee->employee_code])}}" class="text-success p-r-10" data-toggle="tooltip" title="Salary"><i class="fa fa-money"></i></a>
                                        @if(!empty($employee->employeeDetails->resume))
                                            <a href="{{asset('public/backend/documents/'.$employee->employeeDetails->resume)}}" target="_blank" class="text-inverse p-r-10" data-toggle="tooltip" title="View Resume"><i class="icon-doc"></i></a>
                                        @endif
                                        <a href="{{route('delete.employee', ['id' => $employee->id])}}" class="text-inverse p-r-10" title="Delete" data-toggle="tooltip"><i class="ti-trash" onclick="return confirm('Are u sure?')"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@endsection
