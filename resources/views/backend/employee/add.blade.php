@extends('backend.includes.app')

@section('content')
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Add Employee</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Add Employee</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title text-uppercase">Basic Information</h5>
                            <form class="form-material m-t-40" method="post" enctype="multipart/form-data" action="{{ route('add.employee') }}">
                                @csrf
                                {{--first name & last name--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-6" for="first_name"> First Name</span>
                                        </label>
                                        <label class="col-md-6" for="last_name"> Last Name</span>
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="enter first name" value="{{ old('first_name') }}">
                                            <a href="" style="color: red;">{{ $errors -> first('first_name') }}</a>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="last_name" name="last_name" class="form-control" placeholder="enter last name" value="{{ old('last_name') }}">
                                            <a href="" style="color: red;">{{ $errors -> first('last_name') }}</a>
                                        </div>
                                    </div>
                                </div>
                                {{--Date of birth and gender--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-6" for="bdate">Date of Birth</span>
                                        </label>

                                        <label class="col-sm-6">Gender</label>
                                        <div class="col-md-6">
                                            <input type="text" id="bdate" name="bdate" class="form-control dob_datepicker" placeholder="enter birth date" value="{{ old('bdate') }}">
                                            <a href="" style="color: red;">{{ $errors -> first('bdate') }}</a>
                                        </div>

                                        <div class="col-sm-6">
                                            <select class="form-control" name="gender" id="gender">
                                                <option value=>Select Gender</option>
                                                <option value="male" @if(old('gender') == 'male')selected @endif>Male</option>
                                                <option value="female" @if(old('gender') == 'female')selected @endif>Female</option>
                                                <option value="other" @if(old('gender') == 'other')selected @endif>Other</option>
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('gender') }}</a>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-sm-4" for="department">Department</label>
                                        <label class="col-md-4" for="designation">Designation</label>
                                        <label class="col-md-4" for="employee_type">Employee Type</span>
                                        </label>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="department" id="department">
                                                <option value=>--Select Department--</option>
                                                @foreach($departmentDetails as $department)
                                                    <option value="{{ $department->id }}" @if(old('department') == $department->id)selected @endif>{{ $department->name }}</option>
                                                @endforeach
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('department') }}</a>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control" name="designation" id="designation">
                                                <option value=>--Select Designation--</option>
                                                @foreach($designationDetails as $designation)
                                                    <option value="{{ $designation->id }}" @if(old('designation') == $designation->id)selected @endif>{{ $designation->name }}</option>
                                                @endforeach
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('designation') }}</a>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control" name="employee_type" id="employee_type">
                                                <option value=>--Select Employee Type--</option>
                                                <option value="1" @if(old('employee_type') == 1 )selected @endif>Teaching Staff</option>
                                                <option value="2" @if(old('employee_type') == 2 )selected @endif>Non-Teaching Staff</option>
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('employee_type') }}</a>
                                        </div>
                                    </div>
                                </div>

                                {{--profile image and country--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-sm-6">Profile Image</label>
                                        <label class="col-md-6" for="position">Country</span>
                                        </label>
                                        <div class="col-sm-6">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" id="file" onchange="return fileValidation()"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="country" id="country">
                                                <option value=>--Select Country--</option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->name }}" @if(old('country') == $country->name)selected @endif>{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('country') }}</a>

                                        </div>

                                    </div>
                                </div>

                                {{--CV and Qualificcation--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-sm-6">Resume</label>
                                        <label class="col-md-6" for="position">Qualification</span>
                                        </label>
                                        <div class="col-sm-6">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="resume" id="resume_file" onchange="return fileResumeValidation()"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="qualification" name="qualification" class="form-control" placeholder="enter qualification" value="{{ old('qualification') }}">
                                            <a href="" style="color: red;">{{ $errors -> first('qualification') }}</a>
                                        </div>

                                    </div>
                                </div>

                                {{--phone & mobile--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-6" for="phone"> Phone</span>
                                        </label>
                                        <label class="col-md-6" for="Mobile"> Mobile</span>
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" id="phone" name="phone" class="form-control" placeholder="enter phone number" value="{{old('phone')}}">
                                            <a href="" style="color: red;">{{ $errors -> first('phone') }}</a>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="mobile" name="mobile" class="form-control" placeholder="enter mobile number" value="{{old('mobile')}}">
                                            <a href="" style="color: red;">{{ $errors -> first('mobile') }}</a>

                                        </div>
                                    </div>
                                </div>

                                {{--address and email--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-6" for="address">Address</span>
                                        </label>
                                        <label class="col-md-6" for="email">Email</span>
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" id="address" name="address" class="form-control" placeholder="enter address" value="{{old('address')}}">
                                            <a href="" style="color: red;">{{ $errors -> first('address') }}</a>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="email" id="email" name="email" class="form-control" placeholder="enter email" value="{{old('email')}}">
                                            <a href="" style="color: red;">{{ $errors -> first('email') }}</a>
                                        </div>
                                    </div>
                                </div>

                                {{--join date and blood group--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-6" for="jdate">Join Date</span>
                                        </label>

                                        <label class="col-sm-6">Blood Group</label>
                                        <div class="col-md-6">
                                            <input type="text" id="jdate" name="jdate" class="form-control mydatepicker" placeholder="enter join date" value="{{old('jdate')}}">
                                            <a href="" style="color: red;">{{ $errors -> first('jdate') }}</a>
                                        </div>

                                        <div class="col-sm-6">
                                            <select class="form-control" name="blood_group" id="blood_group">
                                                <option value=>Select Blood Group</option>
                                                <option value="A+" @if(old('blood_group') == 'A+')selected @endif>A+</option>
                                                <option value="A-" @if(old('blood_group') == 'A-')selected @endif>A-</option>
                                                <option value="AB+" @if(old('blood_group') == 'AB+')selected @endif>AB+</option>
                                                <option value="AB-" @if(old('blood_group') == 'AB-')selected @endif>AB-</option>
                                                <option value="B+" @if(old('blood_group') == 'B+')selected @endif>B+</option>
                                                <option value="B-" @if(old('blood_group') == 'B-')selected @endif>B-</option>
                                                <option value="O+" @if(old('blood_group') == 'O+')selected @endif>O+</option>
                                                <option value="O-" @if(old('blood_group') == 'O-')selected @endif>O-</option>
                                            </select>
                                            <a href="" style="color: red;">{{ $errors -> first('blood_group') }}</a>
                                        </div>
                                    </div>
                                </div>

                                {{--Religion and mother tongue--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-6" for="religion">Religion</span>
                                        </label>
                                        <label class="col-md-6" for="mother_tongue">Mother Tongue</span>
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" id="religion" name="religion" class="form-control" placeholder="enter religion" value="{{ old('religion') }}">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="mother_tongue" name="mother_tongue" class="form-control" placeholder="enter language" value="{{ old('mother_tongue') }}">
                                        </div>
                                    </div>
                                </div>

                                {{--description--}}
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12">Description</label>
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" name="description" id="description" value="{{ old('description') }}"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <h5 class="card-title text-uppercase">Social Media</h5>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12" for="furl">Facebook URL</span>
                                        </label>
                                        <div class="col-md-12">
                                            <input type="text" id="furl" name="furl" class="form-control" value="{{ old('furl') }}">
                                            <a href="" style="color: red;">@if($errors -> first('furl')){{ "Invalid Facebook URL" }}@endif</a>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12" for="turl">Twitter URL</span>
                                        </label>
                                        <div class="col-md-12">
                                            <input type="text" id="turl" name="turl" class="form-control" value="{{ old('turl') }}">
                                            <a href="" style="color: red;">@if($errors -> first('turl')){{ "Invalid Twitter URL" }}@endif</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12" for="gurl">Instagram URL</span>
                                        </label>
                                        <div class="col-md-12">
                                            <input type="text" id="insurl" name="insurl" class="form-control" value="{{ old('insurl') }}">
                                            <a href="" style="color: red;">@if($errors -> first('insurl')){{ "Invalid Instagram URL" }}@endif</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12" for="inurl">LinkedIN URL</span>
                                        </label>
                                        <div class="col-md-12">
                                            <input type="text" id="inurl" name="inurl" class="form-control" value="{{ old('inurl') }}" >
                                            <a href="" style="color: red;">@if($errors -> first('inurl')){{ "Invalid LinkedIN URL" }}@endif</a>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

@endsection
