@extends('backend.includes.app')

@section('content')

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Profile</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Profile</li>
                            </ol>
                        </div>
                    </div>
                </div>


     <div class="row">
                    
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> 
                                     <img  src="{{ asset('public/backend/images/setting/'.$setting->image)}}" class="img-circle" width="150" height="150">
                                    <h4 class="card-title m-t-10">{{($setting->name)}}</h4>
                                   
                                </center>
                            </div>
                           
                            <div class="card-body"> <small class="text-muted">Email address </small>
                                <h6>{{($setting->email)}}</h6> <small class="text-muted p-t-30 db">Phone</small>
                                <h6>{{($setting->number)}}</h6> <small class="text-muted p-t-30 db">Address</small>
                                <h6>{{($setting->province)}}</h6>
                              <small class="text-muted p-t-30 db">Social Profile</small>
                                <br/>
                               <a href="{{($setting->facebook)}}" target="_blank"><button class="btn btn-circle btn-secondary"><i class="fa fa-facebook"></i></button></a>
                              
                                 <a href="{{($setting->twitter)}}" target="_blank"><button class="btn btn-circle btn-secondary"><i class="fa fa-twitter"></i></button></a>

                                 <a href="{{($setting->youtube)}}" target="_blank"><button class="btn btn-circle btn-secondary"><i class="fa fa-youtube"></i></button></a>

                               <a href="{{($setting->instagram)}}" target="_blank"><button class="btn btn-circle btn-secondary"><i class="fa fa-instagram"></i></button></a>

                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#setting" role="tab">Settings</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Name</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->name)}}</p>
                                            </div>
                                            
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->email)}}</p>
                                            </div>
                                              <div class="col-md-3 col-xs-6 b-r"> <strong>Code</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->icode)}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Post Code</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->pcode)}}</p>
                                            </div>
                                            
                                        </div>
                                        <hr>

                                         <div class="row">
                                            <div class="col-md-3 col-xs-6"> <strong>Country</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->country)}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Province</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->province)}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>VDC/Municipality</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->city)}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Street</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->state)}}</p>
                                            </div>
                                            
                                        </div>
                                        <hr>
                                         <div class="row">
                                              <div class="col-md-3 col-xs-6 b-r"> <strong>Ward Number</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->cnumber)}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6"> <strong>Phone Number</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->number)}}</p>
                                            </div>
                                          
                                            
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Principal Phone Number</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->pnumber)}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fax Number</strong>
                                                <br>
                                                <p class="text-muted">{{($setting->fax)}}</p>
                                            </div>
                                          
                                           
                                        </div>
                                    </div>
                                </div>
                                <!--second tab-->
                                <div class="tab-pane" id="setting" role="tabpanel">
                                    <div class="card-body">
                                   <div class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                     <div class="card-body">
                               <form  action="{{route('settingssave')}}" class="form-horizontal" method="post"  enctype="multipart/form-data" id="institutionForm">
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                             @csrf
                            <div class="row p-t-20">
                                    <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">Choose Profile Picture</span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" id="file" onchange="return fileValidation()"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                           <input type="hidden" name="current_image" value="{{ $setting->image }}">
                                           <img style="width:40px; height: 40px;" src="{{ asset('public//backend/images/setting/'.$setting->image)}}">
                                                </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">Choose Banner Picture</span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image1" id="file" onchange="return fileValidation()"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                           <input type="hidden" name="current_image" value="{{ $setting->image }}">
                                           <img style="width:40px; height: 40px;" src="{{ asset('public//backend/images/setting/'.$setting->image1)}}">
                                                </div>
                                        </div>
                                    </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="name">Institution  Name</label>
                                                    <input type="text" id="name" value="{{($setting->name)}}"  name="name" class="form-control" maxlength="70" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('name') }}</a>
                                                 </div>
                                            </div>
                                              <div class="col-md-6">
                                                 <div class="form-group">
                                                    <label class="control-label" for="email">Institution  Email</label>
                                                    <input type="email" name="email" value="{{($setting->email)}}" id="email" class="form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('email') }}</a>

                                                 </div>
                                            </div>
                                        </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">


                                                <div class="form-group">
                                                    <label class="control-label" for="icode">Institution Code</label>
                                                    <input type="text" id="icode" maxlength="10" name="icode" value="{{($setting->icode)}}"  class="form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('icode') }}</a>

                                                    <small class="form-control-feedback">Institution code(This code will be used as the prefix for student admission number)</small>
                                                 </div>

                                                

                                              
                                                 </div>
                                           
                                            <div class="col-md-6">
                                                    
                                                 <div class="form-group">
                                                    <label name="country" for="country">Country</label>
                                                    <select  name="country" class="form-control custom-select" id="country">
                                                        @foreach($countries as $country)
                                                        <option value="{{$country->name}}" @if($setting->country == $country->name) selected @endif>{{$country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <a href="" style="color: red;">{{ $errors -> first('country') }}</a>

                                                     </div>

                                               
                                            </div>
                                        </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-6 col-sm-4">
                                              <label for="province">Province<span style="color: red;"> * </span></label>
                                              <select class="form-control  custom-select" name="province" id="province">
                                        @foreach($provinces as $province)
                                        <option value="{{ $province->name }}" @if( $setting->province ==$province->name)selected @endif>{{ $province->name }}</option>
                                        @endforeach
                                              </select>
                                               <a href="" style="color: red;">{{ $errors -> first('province') }}</a> 
                                          </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="city"> VDC/Municipality</label>
                                                    <input type="text" id="city" name="city" maxlength="30" value="{{($setting->city)}}" class="form-control form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('city') }}</a>

                                                     </div>
                                            </div>
                                               </div>
                                        </div>
                                           <div class="row p-t-20">
                                            <div class="col-md-6">
                                                 <div class="form-group">
                                                    <label class="control-label" for="state">Street</label>
                                                    <input type="text" id="state" name="state" maxlength="30" value="{{($setting->state)}}" class="form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('state') }}</a>

                                                 </div>


                                              
                                                 </div>
                                           
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="cnumber"> Ward Number</label>
                                                    <input type="text" id="cnumber" maxlength="10" name="cnumber" value="{{($setting->cnumber)}}" class="form-control form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('cnumber') }}</a>

                                                     </div> 
                                            </div>
                                            
                                        </div>
                                        <div class="row p-t-20">
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                    <label class="control-label" for="pcode">Post Code</label>
                                                    <input type="text" id="pcode" name="pcode" maxlength="10" value="{{($setting->pcode)}}" class="form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('pcode') }}</a>

                                                 </div>
                                               
                                                 </div>
                                           
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label class="control-label" for="number">Phone Number</label>
                                                    <input type="text" id="number" name="number" maxlength="15" value="{{($setting->number)}}" class="form-control form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('number') }}</a>

                                                     </div>
                                                  
                                               
                                            </div>
                                            
                                        </div>
                                           <div class="row p-t-20">
                                            <div class="col-md-6">
                                                 <div class="form-group">
                                                    <label class="control-label" for="pnumber">Principal Phone Number</label>
                                                    <input type="text" id="pnumber" maxlength="15" name="pnumber" value="{{($setting->pnumber)}}" class="form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('pnumber') }}</a>

                                                    
                                                 </div>
                                                 </div>
                                           
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="fax"> Institution Fax</label>
                                                    <input type="text" id="fax" name="fax" value="{{($setting->fax)}}"  class="form-control form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('fax') }}</a>

                                                     </div>
                                            </div>
                                        </div>
                                         <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="facebook">Facebook</label>
                                                    <input type="text" id="facebook" name="facebook" value="{{($setting->facebook)}}"  class="form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('facebook') }}</a>

                                                    
                                                 </div>
                                                 </div>
                                           
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="twitter"> Twitter</label>
                                                    <input type="text" id="twitter" name="twitter" value="{{($setting->twitter)}}"  class="form-control form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('twitter') }}</a>

                                                     </div>
                                            </div>
                                            
                                        </div>
                                         <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="youtube">Youtube</label>
                                                    <input type="text" id="youtube"  name="youtube" value="{{($setting->youtube)}}" class="form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('youtube') }}</a>

                                                    
                                                 </div>
                                                 </div>
                                           
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="instagram">Instagram</label>
                                                    <input type="text" id="instagram" name="instagram" value="{{($setting->instagram)}}"  class="form-control form-control" onkeyup="institutionDetailValidation()">
                                                    <a href="" style="color: red;">{{ $errors -> first('instagram') }}</a>
                                                    
                                                     </div>
                                            </div>
                                            
                                        </div>
                                     <div class="form-actions text-center">
                                 <button type="submit" class="btn btn-info btn-lg" id="update_institution_details"> <i class="fa fa-check"></i> Update</button>
                                         <strong><p id="empty_field" style="color: red;"></p></strong>

                                     </div>
                                    </form>
                                    </div>
                                </div>
                                
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
      



@endsection