<?php
use \App\Http\Controllers\Controller;
$settings = Controller::settings();
$studentadmissions = Controller::studentadmissions();
?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Report</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="public/onlinezeals/css/style.css">

        <!-- Styles -->
        
    </head>
    <body>
    

@extends('backend.includes.app')
@section('content')


<div class="container">
  <div class="form-group">
    <h1 class="text-center">Select Following Fields To See Your ID Card.</h1>    
      <form method="post" action="{{URL::to('/search')}}" id="myform" role="search">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">
              <div class="form-group col-sm-4">
                  <label for="y">Academic Year<span style="color: red;"> * </span></label>
                  <select class="form-control" name="y">
                      <option value=>Select Academic Year</option>
                          @foreach($studentadmissions as $student)
                            <option value="{{$student->academic_year}}">{{$student->academic_year}}</option>
                          @endforeach
                    </select>
                  </div>

                  <div class="form-group col-sm-4">
                  <label for="c">Class<span style="color: red;"> * </span></label>
                    <select class="form-control" name="c">
                      <option value=>Select Class</option>
                          @foreach($studentadmissions as $student)
                            <option value="{{$student->room_id}}">{{$student->room['cname']}}</option>
                          @endforeach
                    </select>
                  </div>

                  <div class="form-group col-sm-4">
                  <label for="q">Roll Number<span style="color: red;"> * </span></label>
                    <select class="form-control" name="q">
                      <option value=>Select Roll Number</option>
                          @foreach($studentadmissions as $student)
                            <option value="{{$student->rollno}}">{{$student->rollno}}</option>
                          @endforeach
                    </select>
                  </div>   
              </div>
              </div>
            </div>

               <div class="col-md-12" >
                    <div class="text-center">
                        <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Search</button>
                      </div>
                </div>
            </div>
            <br>
            <br>
          
              
                @if(isset($details))
                
   <div class="row">   
  <div class="id-card-holder">
    <div class="id-card">
      @foreach($settings as $setting)
      <div class="header">
        <img src="{{asset('public/backend/images/setting/'.$setting->image)}}">
      </div>
      <!-- <svg height="210" width="500">
        <line x1="0" y1="0" x2="50" y2="50" style="stroke:rgb(255,0,0);stroke-width:2" />
      </svg> -->


      @endforeach
      @foreach($details as $student)
      <div class="photo" style="border-radius: 30">
        <img  src="{{asset('public/backend/images/student/'.$student->image)}}"> 
      </div>
      
      <h6 style="float: left;">{{ $student->first_name.' '.$student->middle_name.' '.$student->last_name }}</h6><br>
      <h6 style="float: right;">Class {{$student->room['room_code']}}</h6>
      <hr>
      <h6>Roll Number: {{$student->rollno}}</h6>
      <h6>Phone Number: {{$student->phone_no}}</h6>
      <h6>Address: {{$student->present_address}}</h6>     
      <h6> Validity:13/04/{{$student->academic_year +1 }} </h6>
      
      @endforeach
        @endif
    </div> 
  </div>
</div>

  @endsection
  </body>
<style type="text/css">
  body {
      background-color: #d7d6d3;
      font-family:'verdana';
    }
    .id-card-holder {
      width: 225px;
        padding: 4px;
        margin: 0 auto;
        background-color: #1f1f1f;
        border-radius: 5px;
        position: relative;
    }
    .id-card-holder:after {
        content: '';
        width: 7px;
        display: block;
        background-color: #0a0a0a;
        height: 100px;
        position: absolute;
        top: 110px;
        border-radius: 0 5px 5px 0;
    }
    .id-card-holder:before {
        content: '';
        width: 7px;
        display: block;
        background-color: #0a0a0a;
        height: 100px;
        position: absolute;
        top: 110px;
        left: 222px;
        border-radius: 5px 0 0 5px;
    }
    .id-card {
      
      background-color: #fff;
      padding: 10px;
      border-radius: 10px;
      text-align: center;
      box-shadow: 0 0 1.5px 0px #b9b9b9;
    }
    .id-card img {
      margin: 0 auto;
    }
    .header img {
      width: 100px;
      margin-top:0;
    }
    .photo img {
      width: 80px;
      margin: 0;
    }
    h6 {
      font-size: 1px;
      margin: 1px 0;
      font-weight: 200;
    }
    .qr-code img {
      width: 50px;
    }
    p {
      font-size: 5px;
      margin: 2px;
    }
  </style>
  </html>