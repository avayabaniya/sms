<?php
use Illuminate\Support\Facades\Input;
use App\StudentAdmission;
use App\Setting;
use App\Invoice;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/
Route::get('/', function(){
   if(Auth::guest())
   {
     return Redirect::to('login');
   }
   if(Auth::check()){
     return Redirect::to('/dashboard');
   }
});
Auth::routes();
Route::get('/register', function(){
    return Redirect::to('login');
});
Route::get('/userprofile/edit/{id}', 'UserProfileController@edit')->name('userprofile.edit');
Route::any('/userprofile/update/{id}', 'UserProfileController@update')->name('userprofile.update');
//Employee
Route::match(['get', 'post'],'/employee/add', 'EmployeesController@addEmployee')->name('add.employee');
Route::match(['get', 'post'],'/employee/edit/{id}', 'EmployeesController@editEmployee')->name('edit.employee');
Route::get('/employee', 'EmployeesController@viewEmployee')->name('view.employee');
Route::get('/employee/delete/{id}', 'EmployeesController@deleteEmployee')->name('delete.employee');
//Payroll
Route::match(['get','post'], '/employee/payroll/add/{id}', 'PayrollsController@addPayroll')->name('add.payroll');
Route::get('/employee/payroll/{code}', 'PayrollsController@payrollEmployee')->name('payroll.employee');
Route::match(['get', 'post'], '/employee/depatment-payroll/view','PayrollsController@viewDepartmentPayroll')->name('view.department.payroll');
Route::match(['get', 'post'], '/employee/individual-payroll/view','PayrollsController@viewIndividualPayroll')->name('view.individual.payroll');
Route::match(['get', 'post'], '/employee/annual-payroll/view','PayrollsController@viewAnnualPayroll')->name('view.annual.payroll');
Route::match(['get', 'post'], '/employee/payroll/same-as-previous/{id}','PayrollsController@sameAsPreviousPayroll')->name('same.payroll');
//Salary
Route::match(['get','post'], '/employee/salary/add/{code}', 'SalaryController@addSalary')->name('add.salary');
//Departments
Route::get('/department' , 'DepartmentsController@viewDepartment')->name('view.department');
Route::post('/department', 'DepartmentsController@addDepartment')->name('add.department');
Route::get('/department/delete/{id}', 'DepartmentsController@deleteDepartment')->name('delete.department');
Route::get('/department/view/{id}', 'DepartmentsController@viewSelectedDepartment')->name('view.selected.department');
Route::post('/department/edit/{id}', 'DepartmentsController@editHeadDepartment')->name('editHead.department');
//Designation
Route::get('/designation' , 'DesignationsController@viewDesignation')->name('view.designation');
Route::post('/designation', 'DesignationsController@addDesignation')->name('add.designation');
Route::get('/designation/delete/{id}', 'DesignationsController@deleteDesignation')->name('delete.designation');
Route::get('/designation/view/{id}', 'DesignationsController@viewSelectedDesignation')->name('view.selected.designation');
Route::post('/designation/edit/{id}', 'DesignationsController@editDesignation')->name('edit.designation');
Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
//Settings
Route::prefix('settings')->group(function(){
Route::get('/', 'SettingsController@index')->name('settings');
Route::post('/', 'SettingsController@update')->name('settingssave');
});
//Academic
Route::prefix('academic')->group(function(){
Route::get('/', 'AcademicsController@index')->name('academic');
 Route::post('/', 'AcademicsController@store')->name('academicsave');
 Route::match(['get','post'],'/{id}', 'AcademicsController@edit')->name('academicedit');
 Route::match(['get','post'],'/edit/{id}', 'AcademicsController@update')->name('academicupdate');
 Route::match(['get','post'],'/{id}', 'AcademicsController@destroy')->name('academicdelete');
});
// class
Route::prefix('classes')->group(function(){
Route::get('/', 'RoomsController@index')->name('classes');
Route::post('/', 'RoomsController@store')->name('classessave');
Route::post('/{id}', 'RoomsController@update')->name('classupdate');
Route::any('/delete/{id}', 'RoomsController@destroy')->name('classdestroy');
});
//Subject
Route::prefix('subjects')->group(function(){
Route::get('/', 'SubjectsController@index')->name('subject');
Route::post('/', 'SubjectsController@store')->name('subjectsave');
Route::match(['get', 'post'],'/edit/{id}', 'SubjectsController@edit')->name('subjectedit');
Route::post('/{id}', 'SubjectsController@update')->name('subjectupdate');
Route::any('/delete/{id}', 'SubjectsController@destroy')->name('subjectdestroy');
 });
// Section
Route::prefix('sections')->group(function(){
Route::get('/', 'SectionsController@index')->name('sections');
Route::post('/', 'SectionsController@store')->name('sectionssave');
Route::post('/{id}', 'SectionsController@update')->name('sectionsupdate');
Route::any('/delete/{id}', 'SectionsController@destroy')->name('sectionsdestroy');
});
//lesson
Route::prefix('lessons')->group(function(){
Route::get('/', 'LessonsController@index')->name('lesson');
Route::post('/', 'LessonsController@store')->name('lessonsave');
Route::match(['get', 'post'],'/edit/{id}', 'LessonsController@edit')->name('lessonedit');
Route::post('/update/{id}','LessonsController@update')->name('lessonupdate');
Route::any('/delete/{id}', 'LessonsController@destroy')->name('lessondestroy');
 });
//Create
Route::prefix('exam')->group(function(){
Route::get('/create/', 'CreateExamsController@index')->name('create');
Route::post('/create/', 'CreateExamsController@store')->name('createsave');
Route::post('/create/update/{id}','CreateExamsController@update')->name('createupdate');
Route::any('/create/delete/{id}', 'CreateExamsController@destroy')->name('createdelete');
});
//Exam
Route::prefix('exam/schedule')->group(function(){
Route::get('/', 'ExamsController@index')->name('exam');
Route::post('/', 'ExamsController@store')->name('examsave');
Route::match(['get', 'post'],'/edit/{id}', 'ExamsController@edit')->name('examedit');
Route::post('/update/{id}','ExamsController@update')->name('examupdate');
Route::any('/delete/{id}', 'ExamsController@destroy')->name('examdelete');
Route::match(['get', 'post'], '/view','ExamsController@viewexam')->name('viewexam');
Route::match(['get', 'post'], '/view/individual','ExamsController@individual')->name('individual');
Route::post('/get-selected-date', 'ExamsController@getSelectedDate');
Route::post('/get-selected-subject', 'ExamsController@getSelectedSubject');
 });
//Mark
Route::prefix('mark')->group(function(){
Route::get('/add/', 'MarksController@markadd')->name('markadd');
Route::get('/view/', 'MarksController@markview')->name('markview');
Route::match(['get', 'post'],'/subject/add', 'MarksController@subjectmarkadd')->name('subjectmarkadd');
Route::match(['get', 'post'],'/subject/add1', 'MarksController@subjectmarksave')->name('subjectmarksave');
Route::match(['get','post'],'/view/all/', 'MarksController@classmarkview')->name('classmarkview');
Route::post('/schedule/get-selected-subject', 'ExamsController@getSelectedSubject');

 });
//search academic year and clss
    Route::post('/search/mark',function(){
    $academic_year = Input::get ('academic_year');
    $cname = Input::get ('cname');
    $student = StudentAdmission::where('cname',$cname)->where('academic_year', $academic_year)->get();
    if(count($student) >0)
        return view('classmarkview')->withDetails($student)->withQuery($q);
        
    else return view('classmarkview')->withMessage('Sorry. wrong input!');
});
//Students
Route::match(['get', 'post'], '/students', 'StudentsController@index')->name('studentcategory');
Route::get('/student/admission', 'StudentsController@admission')->name('studentadmission');
Route::resource('/studentlists', 'StudentListsController');
Route::resource('/studentadmissions', 'StudentAdmissionsController');
//Guardians
Route::resource('/guardians', 'GuardiansController');
//Events
Route::resource('/events', 'EventsController');
Route::post('/eventtypes/{id}/edit', 'EventTypesController@editEvent')->name('eventtypes.edit');
Route::resource('/eventtypes', 'EventTypesController');
Route::match(['get', 'post'],'/event/add', 'EventsController@index')->name('add.event');
Route::get('/event/store', 'EventsController@store')->name('event.store');
Route::post('/event/store', 'EventsController@store')->name('event.store');
Route::match(['get', 'post'],'/event/edit/{id}', 'EventsController@editEvent')->name('edit.event');
Route::any('/event/update/{id}', 'EventsController@update')->name('updateevent');
//studentcategory
Route::post('/studentcategories/{id}/edit', 'StudentcategoriesController@edit')->name('studentcategories.edit');
Route::resource('/studentcategories', 'StudentcategoriesController');
//Route::get('/studentcategories/delete/{id}', 'StudentcategoriesController@destroy')->name('delete.studentcategories');
//studentadmission
Route::resource('/studentadmissions', 'StudentAdmissionsController');
Route::resource('/guardians', 'GuardiansController');
Route::get('/student/create','StudentAdmissionsController@guardian_index')->name('guardian_index');
Route::get('/student/idcard','StudentAdmissionsController@idcard_index')->name('idcard_index');
Route::match(['get','post'],'/guardians/edit/{id}', 'StudentAdmissionsController@editGuardian')->name('guardians.edit');
Route::any('/guardians/update', 'StudentAdmissionsController@update')->name('updateguardian');
//Finances
 Route::resource('/finances', 'FinancesController');
 //Route::resource('/feecategory', 'FeecategoryController');
//rollnumber
Route::resource('/rollnumbers', 'RollnumbersController');
 //Finances
 Route::resource('/finances', 'FinancesController');
 //Route::resource('/feecategory', 'FeecategoryController');
 Route::get('/feecategory', 'FeecategoryController@index')->name('feecategory.index');
 Route::post('/feecategory/store', 'FeecategoryController@store')->name('feecategory.store');
 Route::get('/feecategory/{id}', 'FeecategoryController@edit')->name('feecategory.edit');
 Route::post('/feecategory/update/{id}', 'FeecategoryController@update')->name('feecategory.update');
 Route::get('/feecategory/destroy/{id}', 'FeecategoryController@destroy')->name('feecategory.destroy');
 //Route::resource('/feesubcategory', 'FeesubcategoryController');
 Route::get('/feesubcategory', 'FeesubcategoryController@index')->name('feesubcategory.index');
 Route::post('/feesubcategory/store', 'FeesubcategoryController@store')->name('feesubcategory.store');
 Route::get('/feesubcategory/{id}', 'FeesubcategoryController@edit')->name('feesubcategory.edit');
 Route::post('/feesubcategory/update/{id}', 'FeesubcategoryController@update')->name('feesubcategory.update');
 Route::get('/feesubcategory/destroy/{id}', 'FeesubcategoryController@destroy')->name('feesubcategory.destroy');
 //Route::resource('/feesubcategoryfine', 'FeesubcategoryfineController');
 Route::get('/feesubcategoryfine', 'FeesubcategoryfineController@index')->name('feesubcategoryfine.index');
 Route::post('/feesubcategoryfine/store', 'FeesubcategoryfineController@store')->name('feesubcategoryfine.store');
 Route::get('/feesubcategoryfine/{id}', 'FeesubcategoryfineController@edit')->name('feesubcategoryfine.edit');
 Route::post('/feesubcategoryfine/update/{id}', 'FeesubcategoryfineController@update')->name('feesubcategoryfine.update');
 Route::get('/feesubcategoryfine/destroy/{id}', 'FeesubcategoryfineController@destroy')->name('feesubcategoryfine.destroy');
 //Route::FEEWAIVER
 Route::get('/feewaiver', 'FeewaiverController@index')->name('feewaiver.index');
 Route::post('/feewaiver/store', 'FeewaiverController@store')->name('feewaiver.store');
 Route::get('/feewaiver/{id}', 'FeewaiverController@edit')->name('feewaiver.edit');
 Route::post('/feewaiver/update/{id}', 'FeewaiverController@update')->name('feewaiver.update');
 Route::get('/feewaiver/destroy/{id}', 'FeewaiverController@destroy')->name('feewaiver.destroy');
//ROUTE::FEE ALLOCATION
 Route::get('/feeallocate', 'FeeallocateController@index')->name('feeallocate.index');
 Route::post('/feeallocate/store', 'FeeallocateController@store')->name('feeallocate.store');
 Route::get('/feeallocate/{id}', 'FeeallocateController@edit')->name('feeallocate.edit');
 Route::post('/feeallocate/update/{id}', 'FeeallocateController@update')->name('feeallocate.update');
 Route::get('/feeallocate/destroy/{id}', 'FeeallocateController@destroy')->name('feeallocate.destroy');
//ROUTE::FEE COLLECTION
 Route::get('/feecollection', 'FeecollectionController@index')->name('feecollection.index');
 Route::post('/feecollection/store', 'FeecollectionController@store')->name('feecollection.store');
 Route::get('/feecollection/{id}', 'FeecollectionController@edit')->name('feecollection.edit');
 Route::post('/feecollection/update/{id}', 'FeecollectionController@update')->name('feecollection.update');
 Route::get('/feecollection/destroy/{id}', 'FeecollectionController@destroy')->name('feecollection.destroy');
//ROURTE:: FEE TYPE
Route::get('/feetype', 'FeetypeController@index')->name('feetype.index');
Route::post('/feetype/store', 'FeetypeController@store')->name('feetype.store');
Route::post('/feetype/{id}', 'FeetypeController@edit')->name('feetype.edit');
Route::post('/feetype/update/{id}', 'FeetypeController@update')->name('feetype.update');
Route::delete('/feetype/destroy/{id}', 'FeetypeController@destroy')->name('feetype.destroy');
Route::get('/feetype/destroy/{id}', 'FeetypeController@destroy')->name('feetype.destroy');
//ROUTE::INVOICE
//search
    Route::post('/search',function(){
    $q = Input::get ('q');
    $y = Input::get ('y');
    $c = Input::get ('c');
    //if($q!=="") {
    //$student = StudentAdmission::where('rollno','LIKE',$q)->orWhere('academic_year','LIKE',$q)->get();
    $student = StudentAdmission::where('rollno',$q)->where('academic_year', $y)->where('room_id', $c)->get();
    if(count($student) >0)
    {
        return view('welcome')->withDetails($student)->withQuery($q);
    }
                
    else return view('welcome')->withMessage('Sorry. wrong input!');
});

//ROUTE::INVOICE
Route::get('/invoice', 'InvoiceController@index')->name('invoice.index');
Route::post('/invoice/store', 'InvoiceController@store')->name('invoice.store');
Route::get('/invoice/edit/{id}', 'InvoiceController@edit')->name('invoice.edit');
Route::get('/invoice/editinvoice/{id}', 'InvoiceController@editinvoice')->name('invoice.editinvoice');
Route::post('/invoice/update/{id}', 'InvoiceController@update')->name('invoice.update');
Route::delete('/invoice/destroy/{id}', 'InvoiceController@destroy')->name('invoice.destroy');
Route::post('/invoice/get-selected-section', 'InvoiceController@getSelectedSection');
Route::post('/invoice/get-selected-studentadmission', 'InvoiceController@getSelectedStudentadmission');

Route::post('/invoice/get-selected-feetype', 'InvoiceController@getSelectedfeetype');

Route::get('/invoice/payment/', 'InvoiceController@payment')->name('invoice.payment');

Route::post('/addinvoice', 'InvoiceController@addinvoice')->name('addinvoice');



// Route::get('/invoicedate', 'InvoiceController@invoicedate');
Route::get('/invoicedate', 'InvoiceController@invoicedate');
Route::post('/storeinvoice', 'InvoiceController@storeinvoice')->name('storeinvoice');


  //search Invoice
    Route::post('/search/invoice',function(){
    $cname = Input::get ('cname');
    $sname = Input::get ('sname');
    $student = Input::get ('student');
    $students = StudentAdmission::where('room_id',$cname)->where('section_id',$sname)->where('id',$student)->get();
    $invoice1 = Invoice::with('student')->where('student_admission_id', $student)->latest()->first();
//dd($invoice1);
    if(count($students) >0)
        return view('backend/invoice/index')->withDetails($students, $invoice1)->withQuery($cname)->with(compact('invoice1'));
    else return view('backend/invoice/index')->withMessage('Sorry. wrong input!');
});

//ROUTE::CLASSFEE

Route::get('/classfee', 'ClassfeeController@index')->name('classfee.index');
Route::post('/classfee/store', 'ClassfeeController@store')->name('classfee.store');
Route::get('/classfee/edit/{id}', 'ClassfeeController@edit')->name('classfee.edit');
Route::post('/classfee/update/{id}', 'ClassfeeController@update')->name('classfee.update');
Route::delete('/classfee/destroy/{id}', 'ClassfeeController@destroy')->name('classfee.destroy');